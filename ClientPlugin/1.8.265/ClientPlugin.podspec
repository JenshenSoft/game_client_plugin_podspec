Pod::Spec.new do |spec|
    spec.name                     = 'ClientPlugin'
    spec.version                  = '1.8.265'
    spec.homepage                 = 'https://gitlab.com/raspberryapps/game_client_plugin_podspec'
    spec.source                   = { :git => 'https://gitlab.com/raspberryapps/game_client_plugin_podspec.git', :tag => spec.version }
    spec.authors                  = 'Raspberry games'
    spec.license                  = { :type => 'MIT', :text => 'License text'}
    spec.summary                  = 'Client plugin for teh game engine logic'
    spec.vendored_frameworks      = 'ClientPlugin.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '12'
    spec.osx.deployment_target = '10.15'

end
