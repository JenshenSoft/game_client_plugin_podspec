#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class ClientPluginMethodResult, ClientPluginEventSubscriptionRequestDto, ClientPluginGameEngineServices, ClientPluginEnginesControllerCompanion, ClientPluginEngineLoggers, ClientPluginKotlinThrowable, ClientPluginKotlinArray<T>, ClientPluginKotlinException, ClientPluginKotlinRuntimeException, ClientPluginDeveloperConfig, ClientPluginGame_clientClientConfig, ClientPluginCoreCoreConfig, ClientPluginEngineEngineLogicConfig, ClientPluginEngineEngineBotConfig, ClientPluginEngineDealerConfig, ClientPluginGame_clientGameClientConfig, ClientPluginEngineConfig, ClientPluginKotlinEnumCompanion, ClientPluginKotlinEnum<E>, ClientPluginEqualsStrategy, ClientPluginEventSubscribe, ClientPluginEngineConfig_, ClientPluginCoreGameUserInfo, ClientPluginGameStartArguments, ClientPluginResponseTypeDtoCompanion, ClientPluginResponseTypeDto, ClientPluginBooleanResponseTypeDtoCompanion, ClientPluginBooleanResponseTypeDto, ClientPluginKotlinByteArray, ClientPluginByteArrayResponseTypeDtoCompanion, ClientPluginByteArrayResponseTypeDto, ClientPluginDeveloperConfigDtoCompanion, ClientPluginDeveloperConfigDto, ClientPluginEventSubscriptionResponseDtoCompanion, ClientPluginEventSubscriptionResponseDto, ClientPluginSubscriptionErrorDto, ClientPluginErrorSubscriptionResponseDtoCompanion, ClientPluginErrorSubscriptionResponseDto, ClientPluginEventSubscriptionRequestDtoCompanion, ClientPluginIntResponseTypeDtoCompanion, ClientPluginIntResponseTypeDto, ClientPluginStringResponseTypeDtoCompanion, ClientPluginStringResponseTypeDto, ClientPluginSubscriptionErrorDtoCompanion, ClientPluginSuccessSubscriptionResponseDtoCompanion, ClientPluginSuccessSubscriptionResponseDto, ClientPluginKotlinx_serialization_jsonJson, ClientPluginKodein_diDIModule, ClientPluginEngineGameHistory, ClientPluginGame_clientGameType, ClientPluginEngineJassTable, ClientPluginGame_clientClientStatePayload, ClientPluginMechanicService, ClientPluginCalcPointsSceneService, ClientPluginChatSceneService, ClientPluginEarnPointsSceneService, ClientPluginControlsContractService, ClientPluginExpectantContractService, ClientPluginPlayersSceneService, ClientPluginSceneActionsService, ClientPluginTradeSceneService, ClientPluginTableSceneService, ClientPluginChoosePartnerSceneService, ClientPluginEngineUserRatingInteractor, ClientPluginMethodResultCompanion, ClientPluginCoreProcessingCard, ClientPluginEngineLoggersCompanion, ClientPluginLoggerCombinedRaspberryLogger, ClientPluginPluginError, ClientPluginGameEngineFactoryService, ClientPluginGameEngineUtilsService, ClientPluginKotlinPair<__covariant A, __covariant B>, ClientPluginKotlinIllegalStateException, ClientPluginCoreLogType, ClientPluginCoreSuit, ClientPluginCoreCoreConfigCompanion, ClientPluginEngineEngineLogicConfigCompanion, ClientPluginEngineDealerStrategy, ClientPluginEngineDealerConfigCompanion, ClientPluginGame_clientGameClientConfigCompanion, ClientPluginEngineRules, ClientPluginEngineRulesSetType, ClientPluginEngineOptions, ClientPluginEnginePlayersMode, ClientPluginEnginePointsMode, ClientPluginEngineLeague, ClientPluginEngineRoomMode, ClientPluginKotlinByteIterator, ClientPluginKotlinx_serialization_coreSerializersModule, ClientPluginKotlinx_serialization_jsonJsonDefault, ClientPluginKotlinx_serialization_jsonJsonElement, ClientPluginKotlinx_serialization_jsonJsonConfiguration, ClientPluginKodein_diDITrigger, ClientPluginKotlinx_datetimeInstant, ClientPluginEngineRoundHistory, ClientPluginEngineGameHistoryMetadata, ClientPluginEngineGameHistoryCompanion, ClientPluginGame_clientGameTypeCompanion, ClientPluginEngineJassPlayer, ClientPluginEngineSpectator, ClientPluginEngineSceneInfo, ClientPluginEngineCardOnTable, ClientPluginEngineBribe, ClientPluginEngineCardDeck, ClientPluginEngineGameInfo, ClientPluginEngineJassTableCompanion, ClientPluginCorePlayerConnectionState, ClientPluginGame_clientExpectantWrapper<T>, ClientPluginGame_clientCombinationToAnnounceState, ClientPluginGame_clientLastBribeState, ClientPluginGame_clientGameInfoState, ClientPluginEngineErrorState, ClientPluginGame_clientClientStatePayloadCompanion, ClientPluginEnginePlayerRatingOutput, ClientPluginEnginePlayerRatingInput, ClientPluginEngineLeaguesConfig, ClientPluginGame_clientUpdateConfigModel, ClientPluginGame_clientPlayerViewModel, ClientPluginEngineGameLifecycleState, ClientPluginGame_clientClientAppSavedState, ClientPluginGame_clientPointsContentViewModel, ClientPluginGame_clientChatSceneContractObserverStrategy, ClientPluginCoreGameCard, ClientPluginCoreCardPayload, ClientPluginGame_clientCardDeckViewModel, ClientPluginLoggerCombinedRaspberryLoggerCompanion, ClientPluginKotlinCancellationException, ClientPluginCoreLogTypeCompanion, ClientPluginEngineGameStore, ClientPluginEngineAppState, ClientPluginCoreSuitCompanion, ClientPluginEngineRoundContractType, ClientPluginEngineBidType, ClientPluginEngineTrumpCardStepPartnerMode, ClientPluginEngineCombinationType, ClientPluginEnginePointsDistributeMode, ClientPluginEngineRulesCompanion, ClientPluginEngineBotIntelligenceLevel, ClientPluginEngineOptionsCompanion, ClientPluginEnginePlayersModeCompanion, ClientPluginEnginePointsModeCompanion, ClientPluginKotlinx_serialization_coreSerialKind, ClientPluginKotlinNothing, ClientPluginKotlinx_serialization_jsonJsonElementCompanion, ClientPluginKotlinx_serialization_jsonClassDiscriminatorMode, ClientPluginKodein_diDIBuilderDelegateBinder<T>, ClientPluginKodein_diDIKey<__contravariant C, __contravariant A, __covariant T>, ClientPluginKotlinUnit, ClientPluginKotlinx_datetimeInstantCompanion, ClientPluginEngineRoundHistoryItem, ClientPluginEngineRoundContractState, ClientPluginEngineRoundHistoryCompanion, ClientPluginEngineRandomData, ClientPluginEnginePlayerHistoryAnalytics, ClientPluginEngineGameHistoryMetadataCompanion, ClientPluginCorePlayerState, ClientPluginEnginePlayerOptions, ClientPluginEnginePlayerHand, ClientPluginEnginePlayerPoints, ClientPluginEngineJassPlayerCompanion, ClientPluginEngineCardOnTableCompanion, ClientPluginEngineCardDeckCompanion, ClientPluginEngineRound, ClientPluginEnginePreviousRound, ClientPluginEngineGameInfoCompanion, ClientPluginEngineJassTableLite, ClientPluginCorePlayerConnectionStateCompanion, ClientPluginEngineMessageMessageLifecycle, ClientPluginEngineCombination, ClientPluginCoreTerminationGameReason, ClientPluginEngineLeagueInfo, ClientPluginEngineLeaguesConfigCompanion, ClientPluginEngineGameLifecycleStateCompanion, ClientPluginGame_clientPointsCellViewModel, ClientPluginCoreGameCardCompanion, ClientPluginCoreGameCardCardName, ClientPluginEngineTerminationState, ClientPluginEngineRoundContractTypeCompanion, ClientPluginEngineBidTypeCompanion, ClientPluginEngineCombinationTypeCompanion, ClientPluginEngineBotIntelligenceLevelCompanion, ClientPluginKodein_diScopeRegistry, ClientPluginKotlinKTypeProjection, ClientPluginKodein_diDIDefinition<C, A, T>, ClientPluginKotlinTriple<__covariant A, __covariant B, __covariant C>, ClientPluginKodein_diSearchSpecs, ClientPluginEngineRoundContractStateCompanion, ClientPluginEngineCombinationAnalytics, ClientPluginEngineAchievements, ClientPluginCorePlayerStatePlayerWaitingState, ClientPluginCorePlayerConnection, ClientPluginCorePlayerStateCompanion, ClientPluginEnginePlayerOptionsRoundNumber, ClientPluginEnginePlayerOptionsCompanion, ClientPluginEngineLuckyFactorRecord, ClientPluginEngineCombinationRecord, ClientPluginEnginePlayerHandCompanion, ClientPluginEngineCombinationDetails, ClientPluginEngineRoundCompanion, ClientPluginEnginePointItem, ClientPluginEngineConfigLite, ClientPluginEngineJassPlayerLite, ClientPluginEngineCombinationReason, ClientPluginGame_clientPlayerPointsViewModel, ClientPluginGame_clientBadge, ClientPluginKodein_diReference<__covariant T>, ClientPluginKotlinKVariance, ClientPluginKotlinKTypeProjectionCompanion, ClientPluginKodein_diDIDefining<C, A, T>, ClientPluginCorePlayerStatePlayerWaitingStateCompanion, ClientPluginCorePlayerConnectionCompanion, ClientPluginEnginePlayerOptionsRoundNumberCompanion, ClientPluginEngineLuckyFactorRecordCompanion, ClientPluginEngineCombinationState, ClientPluginEngineCombinationDetailsCompanion, ClientPluginGame_clientBadgeType, ClientPluginEngineCombinationStateCompanion, ClientPluginGame_clientBadgeTypeCompanion;

@protocol ClientPluginEventController, ClientPluginClientGameEngineController, ClientPluginLoggerRaspberryLogger, ClientPluginJobSubscriptions, ClientPluginMethodController, ClientPluginEnginesController, ClientPluginLoggerLoggerEnabledTrigger, ClientPluginCoreObservableLoggerOutput, ClientPluginGame_clientShowErrorLoggerOutput, ClientPluginKotlinComparable, ClientPluginKotlinx_coroutines_coreFlow, ClientPluginKotlinx_serialization_coreKSerializer, ClientPluginEngineEncoderDecoderProvider, ClientPluginClientPluginDecoderProvider, ClientPluginKodein_diDI, ClientPluginGameEngineFactory, ClientPluginGame_clientClientGameHistoryInteractor, ClientPluginGame_clientClientStatisticsInteractor, ClientPluginGame_clientPersistentAppStateInteractor, ClientPluginGameEngineLifecycle, ClientPluginGame_clientGameMechanic, ClientPluginGame_clientClientEncoderDecoderProvider, ClientPluginGame_clientCalcPointsScene, ClientPluginGame_clientChatScene, ClientPluginGame_clientChoosePartnerScene, ClientPluginGame_clientEarnPointsSceneContract, ClientPluginGame_clientTableScene, ClientPluginGame_clientTradeSceneContract, ClientPluginGame_clientControlsContract, ClientPluginGame_clientExpectantContract, ClientPluginGame_clientPlayersSceneContract, ClientPluginGame_clientSceneActionsContract, ClientPluginLoggerLoggerOutput, ClientPluginKotlinx_coroutines_coreCoroutineScope, ClientPluginKotlinx_coroutines_coreJob, ClientPluginKotlinSuspendFunction1, ClientPluginChannelSubscription, ClientPluginGame_clientGameClient, ClientPluginKotlinIterator, ClientPluginKotlinx_coroutines_coreFlowCollector, ClientPluginCorePlayerIdContract, ClientPluginKotlinx_serialization_coreEncoder, ClientPluginKotlinx_serialization_coreSerialDescriptor, ClientPluginKotlinx_serialization_coreSerializationStrategy, ClientPluginKotlinx_serialization_coreDecoder, ClientPluginKotlinx_serialization_coreDeserializationStrategy, ClientPluginKotlinx_serialization_coreSerialFormat, ClientPluginKotlinx_serialization_coreStringFormat, ClientPluginKodein_diDIBuilder, ClientPluginKotlinKProperty, ClientPluginKodein_diDIContainer, ClientPluginKodein_diDIContext, ClientPluginKodein_diDIAware, ClientPluginEngineAppStatePayload, ClientPluginCoreBufferedAction, ClientPluginGame_clientBufferStateContract, ClientPluginGame_clientExpectantsStateContract, ClientPluginEngineMessage, ClientPluginCoreExpectantAction, ClientPluginEngineRatingCalculator, ClientPluginEngineGameStoreContract, ClientPluginGame_clientCalcPointsSceneContract, ClientPluginGame_clientChatSceneContract, ClientPluginGame_clientChoosePartnerSceneContract, ClientPluginGame_clientTableSceneContract, ClientPluginGame_clientTableSceneCommon, ClientPluginKotlinCoroutineContext, ClientPluginKotlinx_coroutines_coreChildHandle, ClientPluginKotlinx_coroutines_coreChildJob, ClientPluginKotlinx_coroutines_coreDisposableHandle, ClientPluginKotlinSequence, ClientPluginKotlinx_coroutines_coreSelectClause0, ClientPluginKotlinCoroutineContextKey, ClientPluginKotlinCoroutineContextElement, ClientPluginKotlinFunction, ClientPluginKotlinSuspendFunction2, ClientPluginCoreClearable, ClientPluginEngineStateProvider, ClientPluginEngineGameTransition, ClientPluginKotlinx_serialization_coreCompositeEncoder, ClientPluginKotlinAnnotation, ClientPluginKotlinx_serialization_coreCompositeDecoder, ClientPluginKotlinx_serialization_coreSerializersModuleCollector, ClientPluginKotlinKClass, ClientPluginKotlinx_serialization_jsonJsonNamingStrategy, ClientPluginKodein_diDIBinding, ClientPluginKodein_diDIBuilderDirectBinder, ClientPluginKodein_diDIBuilderTypeBinder, ClientPluginKaveritTypeToken, ClientPluginKodein_diDIBuilderArgSetBinder, ClientPluginKodein_diDIBuilderSetBinder, ClientPluginKodein_diContextTranslator, ClientPluginKodein_diDIBuilderConstantBinder, ClientPluginKodein_diDirectDI, ClientPluginKodein_diDIContainerBuilder, ClientPluginKodein_diDIBindBuilder, ClientPluginKodein_diScope, ClientPluginKodein_diDIBindBuilderWithScope, ClientPluginKotlinKType, ClientPluginKotlinKAnnotatedElement, ClientPluginKotlinKCallable, ClientPluginKodein_diDITree, ClientPluginKotlinLazy, ClientPluginCorePlayer, ClientPluginEngineShuffleCardsContract, ClientPluginEngineSceneData, ClientPluginEngineActData, ClientPluginCoreAction, ClientPluginGame_clientPointClause, ClientPluginKotlinx_coroutines_coreParentJob, ClientPluginKotlinx_coroutines_coreSelectInstance, ClientPluginKotlinx_coroutines_coreSelectClause, ClientPluginCoreReducer, ClientPluginCoreMiddleware, ClientPluginCoreStore, ClientPluginCoreTimerTag, ClientPluginKotlinKDeclarationContainer, ClientPluginKotlinKClassifier, ClientPluginKodein_diDIBindingCopier, ClientPluginKodein_diBindingDI, ClientPluginKodein_diBinding, ClientPluginKodein_diDirectDIAware, ClientPluginKodein_diDirectDIBase, ClientPluginKodein_diExternalSource, ClientPluginKotlinx_datetimeDateTimeFormat, ClientPluginEnginePlayerHistoryItem, ClientPluginEngineCombinationContract, ClientPluginCoreNotValidateIfGameFinished, ClientPluginKodein_diWithContext, ClientPluginKodein_diScopeCloseable, ClientPluginKotlinAppendable;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

#pragma push_macro("_Nullable_result")
#if !__has_feature(nullability_nullable_result)
#undef _Nullable_result
#define _Nullable_result _Nullable
#endif

__attribute__((swift_name("KotlinBase")))
@interface ClientPluginBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end

@interface ClientPluginBase (ClientPluginBaseCopying) <NSCopying>
@end

__attribute__((swift_name("KotlinMutableSet")))
@interface ClientPluginMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end

__attribute__((swift_name("KotlinMutableDictionary")))
@interface ClientPluginMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end

@interface NSError (NSErrorClientPluginKotlinException)
@property (readonly) id _Nullable kotlinException;
@end

__attribute__((swift_name("KotlinNumber")))
@interface ClientPluginNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end

__attribute__((swift_name("KotlinByte")))
@interface ClientPluginByte : ClientPluginNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end

__attribute__((swift_name("KotlinUByte")))
@interface ClientPluginUByte : ClientPluginNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end

__attribute__((swift_name("KotlinShort")))
@interface ClientPluginShort : ClientPluginNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end

__attribute__((swift_name("KotlinUShort")))
@interface ClientPluginUShort : ClientPluginNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end

__attribute__((swift_name("KotlinInt")))
@interface ClientPluginInt : ClientPluginNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end

__attribute__((swift_name("KotlinUInt")))
@interface ClientPluginUInt : ClientPluginNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end

__attribute__((swift_name("KotlinLong")))
@interface ClientPluginLong : ClientPluginNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end

__attribute__((swift_name("KotlinULong")))
@interface ClientPluginULong : ClientPluginNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end

__attribute__((swift_name("KotlinFloat")))
@interface ClientPluginFloat : ClientPluginNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end

__attribute__((swift_name("KotlinDouble")))
@interface ClientPluginDouble : ClientPluginNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end

__attribute__((swift_name("KotlinBoolean")))
@interface ClientPluginBoolean : ClientPluginNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end

__attribute__((swift_name("ClientGameEngineController")))
@protocol ClientPluginClientGameEngineController
@required
- (void)onAttachEventSink:(id<ClientPluginEventController>)eventSink __attribute__((swift_name("onAttach(eventSink:)")));
- (void)onDetach __attribute__((swift_name("onDetach()")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments completionHandler:(void (^)(ClientPluginMethodResult * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("onMethodCall(methodName:arguments:completionHandler:)")));
- (ClientPluginMethodResult * _Nullable)subscribeRequest:(ClientPluginEventSubscriptionRequestDto *)request __attribute__((swift_name("subscribe(request:)")));
- (ClientPluginMethodResult * _Nullable)unsubscribeRequest:(ClientPluginEventSubscriptionRequestDto *)request __attribute__((swift_name("unsubscribe(request:)")));
@property (readonly) BOOL isActive __attribute__((swift_name("isActive")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ClientGameEngineControllerImpl")))
@interface ClientPluginClientGameEngineControllerImpl : ClientPluginBase <ClientPluginClientGameEngineController>
- (instancetype)initWithGameEngineServices:(ClientPluginGameEngineServices *)gameEngineServices logger:(id<ClientPluginLoggerRaspberryLogger>)logger jobSubscriptions:(id<ClientPluginJobSubscriptions>)jobSubscriptions __attribute__((swift_name("init(gameEngineServices:logger:jobSubscriptions:)"))) __attribute__((objc_designated_initializer));
- (void)onAttachEventSink:(id<ClientPluginEventController>)eventSink __attribute__((swift_name("onAttach(eventSink:)")));
- (void)onDetach __attribute__((swift_name("onDetach()")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments completionHandler:(void (^)(ClientPluginMethodResult * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("onMethodCall(methodName:arguments:completionHandler:)")));
- (ClientPluginMethodResult * _Nullable)subscribeRequest:(ClientPluginEventSubscriptionRequestDto *)request __attribute__((swift_name("subscribe(request:)")));
- (ClientPluginMethodResult * _Nullable)unsubscribeRequest:(ClientPluginEventSubscriptionRequestDto *)request __attribute__((swift_name("unsubscribe(request:)")));
@property (readonly) BOOL isActive __attribute__((swift_name("isActive")));
@end

__attribute__((swift_name("EnginesController")))
@protocol ClientPluginEnginesController
@required
- (void)attach __attribute__((swift_name("attach()")));
- (void)detach __attribute__((swift_name("detach()")));
- (void)onCancel __attribute__((swift_name("onCancel()")));
- (void)onListenEventSink:(id<ClientPluginEventController>)eventSink __attribute__((swift_name("onListen(eventSink:)")));
- (void)onMethodCallController:(id<ClientPluginMethodController>)controller __attribute__((swift_name("onMethodCall(controller:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginesControllerCompanion")))
@interface ClientPluginEnginesControllerCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEnginesControllerCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginEnginesController>)createDebugLoggerOutput:(id<ClientPluginLoggerLoggerEnabledTrigger> _Nullable)debugLoggerOutput releaseLoggerOutput:(id<ClientPluginLoggerLoggerEnabledTrigger> _Nullable)releaseLoggerOutput observableLoggerOutput:(id<ClientPluginCoreObservableLoggerOutput> _Nullable)observableLoggerOutput showErrorLoggerOutput:(id<ClientPluginGame_clientShowErrorLoggerOutput> _Nullable)showErrorLoggerOutput __attribute__((swift_name("create(debugLoggerOutput:releaseLoggerOutput:observableLoggerOutput:showErrorLoggerOutput:)")));
- (id<ClientPluginEnginesController>)createWithLoggersLoggers:(ClientPluginEngineLoggers *)loggers __attribute__((swift_name("createWithLoggers(loggers:)")));
- (void)pluginLog:(id<ClientPluginLoggerRaspberryLogger>)receiver message:(NSString *)message __attribute__((swift_name("pluginLog(_:message:)")));
@property (readonly) NSString *PLUGIN_PATH_PREFIX __attribute__((swift_name("PLUGIN_PATH_PREFIX")));
@end

__attribute__((swift_name("KotlinThrowable")))
@interface ClientPluginKotlinThrowable : ClientPluginBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));

/**
 * @note annotations
 *   kotlin.experimental.ExperimentalNativeApi
*/
- (ClientPluginKotlinArray<NSString *> *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
- (NSError *)asError __attribute__((swift_name("asError()")));
@end

__attribute__((swift_name("KotlinException")))
@interface ClientPluginKotlinException : ClientPluginKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("KotlinRuntimeException")))
@interface ClientPluginKotlinRuntimeException : ClientPluginKotlinException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PluginEngineException")))
@interface ClientPluginPluginEngineException : ClientPluginKotlinRuntimeException
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PluginEngineNotCreatedException")))
@interface ClientPluginPluginEngineNotCreatedException : ClientPluginKotlinRuntimeException
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DeveloperConfig")))
@interface ClientPluginDeveloperConfig : ClientPluginBase
- (instancetype)initWithIsAutoStepCurrentPlayer:(BOOL)isAutoStepCurrentPlayer areOpponentsCardsOpened:(BOOL)areOpponentsCardsOpened __attribute__((swift_name("init(isAutoStepCurrentPlayer:areOpponentsCardsOpened:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginDeveloperConfig *)doCopyIsAutoStepCurrentPlayer:(BOOL)isAutoStepCurrentPlayer areOpponentsCardsOpened:(BOOL)areOpponentsCardsOpened __attribute__((swift_name("doCopy(isAutoStepCurrentPlayer:areOpponentsCardsOpened:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL areOpponentsCardsOpened __attribute__((swift_name("areOpponentsCardsOpened")));
@property (readonly) BOOL isAutoStepCurrentPlayer __attribute__((swift_name("isAutoStepCurrentPlayer")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineConfig")))
@interface ClientPluginEngineConfig : ClientPluginBase
- (instancetype)initWithGameId:(NSString *)gameId currentPlayerId:(NSString *)currentPlayerId clientConfig:(ClientPluginGame_clientClientConfig *)clientConfig coreConfig:(ClientPluginCoreCoreConfig * _Nullable)coreConfig engineLogicConfig:(ClientPluginEngineEngineLogicConfig * _Nullable)engineLogicConfig engineBotConfig:(ClientPluginEngineEngineBotConfig * _Nullable)engineBotConfig dealerConfig:(ClientPluginEngineDealerConfig * _Nullable)dealerConfig gameClientConfig:(ClientPluginGame_clientGameClientConfig * _Nullable)gameClientConfig __attribute__((swift_name("init(gameId:currentPlayerId:clientConfig:coreConfig:engineLogicConfig:engineBotConfig:dealerConfig:gameClientConfig:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineConfig *)doCopyGameId:(NSString *)gameId currentPlayerId:(NSString *)currentPlayerId clientConfig:(ClientPluginGame_clientClientConfig *)clientConfig coreConfig:(ClientPluginCoreCoreConfig * _Nullable)coreConfig engineLogicConfig:(ClientPluginEngineEngineLogicConfig * _Nullable)engineLogicConfig engineBotConfig:(ClientPluginEngineEngineBotConfig * _Nullable)engineBotConfig dealerConfig:(ClientPluginEngineDealerConfig * _Nullable)dealerConfig gameClientConfig:(ClientPluginGame_clientGameClientConfig * _Nullable)gameClientConfig __attribute__((swift_name("doCopy(gameId:currentPlayerId:clientConfig:coreConfig:engineLogicConfig:engineBotConfig:dealerConfig:gameClientConfig:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginGame_clientClientConfig *clientConfig __attribute__((swift_name("clientConfig")));
@property (readonly) ClientPluginCoreCoreConfig * _Nullable coreConfig __attribute__((swift_name("coreConfig")));
@property (readonly) NSString *currentPlayerId __attribute__((swift_name("currentPlayerId")));
@property (readonly) ClientPluginEngineDealerConfig * _Nullable dealerConfig __attribute__((swift_name("dealerConfig")));
@property (readonly) ClientPluginEngineEngineBotConfig * _Nullable engineBotConfig __attribute__((swift_name("engineBotConfig")));
@property (readonly) ClientPluginEngineEngineLogicConfig * _Nullable engineLogicConfig __attribute__((swift_name("engineLogicConfig")));
@property (readonly) ClientPluginGame_clientGameClientConfig * _Nullable gameClientConfig __attribute__((swift_name("gameClientConfig")));
@property (readonly) NSString *gameId __attribute__((swift_name("gameId")));
@end

__attribute__((swift_name("KotlinComparable")))
@protocol ClientPluginKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end

__attribute__((swift_name("KotlinEnum")))
@interface ClientPluginKotlinEnum<E> : ClientPluginBase <ClientPluginKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginKotlinEnumCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(E)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EqualsStrategy")))
@interface ClientPluginEqualsStrategy : ClientPluginKotlinEnum<ClientPluginEqualsStrategy *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginEqualsStrategy *equals __attribute__((swift_name("equals")));
@property (class, readonly) ClientPluginEqualsStrategy *startWith __attribute__((swift_name("startWith")));
+ (ClientPluginKotlinArray<ClientPluginEqualsStrategy *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEqualsStrategy *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventSubscribe")))
@interface ClientPluginEventSubscribe : ClientPluginBase
- (instancetype)initWithPath:(NSString *)path createFlow:(id<ClientPluginKotlinx_coroutines_coreFlow> (^)(id _Nullable))createFlow equalsStrategy:(ClientPluginEqualsStrategy *)equalsStrategy __attribute__((swift_name("init(path:createFlow:equalsStrategy:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEventSubscribe *)doCopyPath:(NSString *)path createFlow:(id<ClientPluginKotlinx_coroutines_coreFlow> (^)(id _Nullable))createFlow equalsStrategy:(ClientPluginEqualsStrategy *)equalsStrategy __attribute__((swift_name("doCopy(path:createFlow:equalsStrategy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isCurrentPathPath:(NSString *)path __attribute__((swift_name("isCurrentPath(path:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<ClientPluginKotlinx_coroutines_coreFlow> (^createFlow)(id _Nullable) __attribute__((swift_name("createFlow")));
@property (readonly) ClientPluginEqualsStrategy *equalsStrategy __attribute__((swift_name("equalsStrategy")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GameStartArguments")))
@interface ClientPluginGameStartArguments : ClientPluginBase
- (instancetype)initWithGameId:(NSString *)gameId config:(ClientPluginEngineConfig_ *)config users:(NSArray<ClientPluginCoreGameUserInfo *> *)users savedStateJson:(NSString * _Nullable)savedStateJson __attribute__((swift_name("init(gameId:config:users:savedStateJson:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGameStartArguments *)doCopyGameId:(NSString *)gameId config:(ClientPluginEngineConfig_ *)config users:(NSArray<ClientPluginCoreGameUserInfo *> *)users savedStateJson:(NSString * _Nullable)savedStateJson __attribute__((swift_name("doCopy(gameId:config:users:savedStateJson:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginEngineConfig_ *config __attribute__((swift_name("config")));
@property (readonly) NSString *gameId __attribute__((swift_name("gameId")));
@property (readonly) NSString * _Nullable savedStateJson __attribute__((swift_name("savedStateJson")));
@property (readonly) NSArray<ClientPluginCoreGameUserInfo *> *users __attribute__((swift_name("users")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((swift_name("ResponseTypeDto")))
@interface ClientPluginResponseTypeDto : ClientPluginBase
@property (class, readonly, getter=companion) ClientPluginResponseTypeDtoCompanion *companion __attribute__((swift_name("companion")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.SerialName(value="boolean")
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BooleanResponseTypeDto")))
@interface ClientPluginBooleanResponseTypeDto : ClientPluginResponseTypeDto
- (instancetype)initWithValue:(BOOL)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginBooleanResponseTypeDtoCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginBooleanResponseTypeDto *)doCopyValue:(BOOL)value __attribute__((swift_name("doCopy(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BooleanResponseTypeDto.Companion")))
@interface ClientPluginBooleanResponseTypeDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginBooleanResponseTypeDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.SerialName(value="bytes")
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ByteArrayResponseTypeDto")))
@interface ClientPluginByteArrayResponseTypeDto : ClientPluginResponseTypeDto
- (instancetype)initWithValue:(ClientPluginKotlinByteArray *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginByteArrayResponseTypeDtoCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginByteArrayResponseTypeDto *)doCopyValue:(ClientPluginKotlinByteArray *)value __attribute__((swift_name("doCopy(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginKotlinByteArray *value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ByteArrayResponseTypeDto.Companion")))
@interface ClientPluginByteArrayResponseTypeDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginByteArrayResponseTypeDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DeveloperConfigDto")))
@interface ClientPluginDeveloperConfigDto : ClientPluginBase
- (instancetype)initWithIsAutoStepCurrentPlayer:(ClientPluginBoolean * _Nullable)isAutoStepCurrentPlayer areOpponentsCardsOpened:(ClientPluginBoolean * _Nullable)areOpponentsCardsOpened __attribute__((swift_name("init(isAutoStepCurrentPlayer:areOpponentsCardsOpened:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginDeveloperConfigDtoCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginDeveloperConfigDto *)doCopyIsAutoStepCurrentPlayer:(ClientPluginBoolean * _Nullable)isAutoStepCurrentPlayer areOpponentsCardsOpened:(ClientPluginBoolean * _Nullable)areOpponentsCardsOpened __attribute__((swift_name("doCopy(isAutoStepCurrentPlayer:areOpponentsCardsOpened:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginBoolean * _Nullable areOpponentsCardsOpened __attribute__((swift_name("areOpponentsCardsOpened")));
@property (readonly) ClientPluginBoolean * _Nullable isAutoStepCurrentPlayer __attribute__((swift_name("isAutoStepCurrentPlayer")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DeveloperConfigDto.Companion")))
@interface ClientPluginDeveloperConfigDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginDeveloperConfigDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((swift_name("EventSubscriptionResponseDto")))
@interface ClientPluginEventSubscriptionResponseDto : ClientPluginBase
@property (class, readonly, getter=companion) ClientPluginEventSubscriptionResponseDtoCompanion *companion __attribute__((swift_name("companion")));
@property (readonly) NSString *path __attribute__((swift_name("path")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
 *   kotlinx.serialization.SerialName(value="error")
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ErrorSubscriptionResponseDto")))
@interface ClientPluginErrorSubscriptionResponseDto : ClientPluginEventSubscriptionResponseDto
- (instancetype)initWithPath:(NSString *)path error:(ClientPluginSubscriptionErrorDto *)error __attribute__((swift_name("init(path:error:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginErrorSubscriptionResponseDtoCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginErrorSubscriptionResponseDto *)doCopyPath:(NSString *)path error:(ClientPluginSubscriptionErrorDto *)error __attribute__((swift_name("doCopy(path:error:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginSubscriptionErrorDto *error __attribute__((swift_name("error")));
@property (readonly) NSString *path __attribute__((swift_name("path")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ErrorSubscriptionResponseDto.Companion")))
@interface ClientPluginErrorSubscriptionResponseDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginErrorSubscriptionResponseDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventSubscriptionRequestDto")))
@interface ClientPluginEventSubscriptionRequestDto : ClientPluginBase
- (instancetype)initWithPath:(NSString *)path arguments:(NSString * _Nullable)arguments __attribute__((swift_name("init(path:arguments:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEventSubscriptionRequestDtoCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEventSubscriptionRequestDto *)doCopyPath:(NSString *)path arguments:(NSString * _Nullable)arguments __attribute__((swift_name("doCopy(path:arguments:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable arguments __attribute__((swift_name("arguments")));
@property (readonly) NSString *path __attribute__((swift_name("path")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventSubscriptionRequestDto.Companion")))
@interface ClientPluginEventSubscriptionRequestDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEventSubscriptionRequestDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventSubscriptionResponseDto.Companion")))
@interface ClientPluginEventSubscriptionResponseDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEventSubscriptionResponseDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.SerialName(value="int")
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IntResponseTypeDto")))
@interface ClientPluginIntResponseTypeDto : ClientPluginResponseTypeDto
- (instancetype)initWithValue:(int32_t)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginIntResponseTypeDtoCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginIntResponseTypeDto *)doCopyValue:(int32_t)value __attribute__((swift_name("doCopy(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IntResponseTypeDto.Companion")))
@interface ClientPluginIntResponseTypeDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginIntResponseTypeDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResponseTypeDto.Companion")))
@interface ClientPluginResponseTypeDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginResponseTypeDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.SerialName(value="string")
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StringResponseTypeDto")))
@interface ClientPluginStringResponseTypeDto : ClientPluginResponseTypeDto
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginStringResponseTypeDtoCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginStringResponseTypeDto *)doCopyValue:(NSString *)value __attribute__((swift_name("doCopy(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StringResponseTypeDto.Companion")))
@interface ClientPluginStringResponseTypeDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginStringResponseTypeDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SubscriptionErrorDto")))
@interface ClientPluginSubscriptionErrorDto : ClientPluginBase
- (instancetype)initWithMessage:(NSString * _Nullable)message stackTrace:(NSString * _Nullable)stackTrace __attribute__((swift_name("init(message:stackTrace:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginSubscriptionErrorDtoCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginSubscriptionErrorDto *)doCopyMessage:(NSString * _Nullable)message stackTrace:(NSString * _Nullable)stackTrace __attribute__((swift_name("doCopy(message:stackTrace:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly) NSString * _Nullable stackTrace __attribute__((swift_name("stackTrace")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SubscriptionErrorDto.Companion")))
@interface ClientPluginSubscriptionErrorDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginSubscriptionErrorDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
 *   kotlinx.serialization.SerialName(value="success")
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SuccessSubscriptionResponseDto")))
@interface ClientPluginSuccessSubscriptionResponseDto : ClientPluginEventSubscriptionResponseDto
- (instancetype)initWithPath:(NSString *)path success:(ClientPluginResponseTypeDto * _Nullable)success __attribute__((swift_name("init(path:success:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginSuccessSubscriptionResponseDtoCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginSuccessSubscriptionResponseDto *)doCopyPath:(NSString *)path success:(ClientPluginResponseTypeDto * _Nullable)success __attribute__((swift_name("doCopy(path:success:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *path __attribute__((swift_name("path")));
@property (readonly) ClientPluginResponseTypeDto * _Nullable success __attribute__((swift_name("success")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SuccessSubscriptionResponseDto.Companion")))
@interface ClientPluginSuccessSubscriptionResponseDtoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginSuccessSubscriptionResponseDtoCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end

__attribute__((swift_name("ClientPluginDecoderProvider")))
@protocol ClientPluginClientPluginDecoderProvider
@required
- (id)decodeArgsSerializer:(id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer args:(id)args __attribute__((swift_name("decodeArgs(serializer:args:)")));
- (ClientPluginResponseTypeDto *)encodeToResponseTypeSerializer:(id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer value:(id)value __attribute__((swift_name("encodeToResponseType(serializer:value:)")));
@end

__attribute__((swift_name("EngineEncoderDecoderProvider")))
@protocol ClientPluginEngineEncoderDecoderProvider
@required
- (ClientPluginEngineConfig *)decodeEngineConfigJson:(NSString *)json __attribute__((swift_name("decodeEngineConfig(json:)")));
- (NSString *)encodeEngineConfigConfig:(ClientPluginEngineConfig *)config __attribute__((swift_name("encodeEngineConfig(config:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineEncoderDecoderProviderImpl")))
@interface ClientPluginEngineEncoderDecoderProviderImpl : ClientPluginBase <ClientPluginEngineEncoderDecoderProvider>
- (instancetype)initWithFormat:(ClientPluginKotlinx_serialization_jsonJson *)format __attribute__((swift_name("init(format:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineConfig *)decodeEngineConfigJson:(NSString *)json __attribute__((swift_name("decodeEngineConfig(json:)")));
- (NSString *)encodeEngineConfigConfig:(ClientPluginEngineConfig *)config __attribute__((swift_name("encodeEngineConfig(config:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("JsonClientPluginDecoderProviderImpl")))
@interface ClientPluginJsonClientPluginDecoderProviderImpl : ClientPluginBase <ClientPluginClientPluginDecoderProvider>
- (instancetype)initWithJson:(ClientPluginKotlinx_serialization_jsonJson *)json __attribute__((swift_name("init(json:)"))) __attribute__((objc_designated_initializer));
- (id)decodeArgsSerializer:(id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer args:(id)args __attribute__((swift_name("decodeArgs(serializer:args:)")));
- (ClientPluginResponseTypeDto *)encodeToResponseTypeSerializer:(id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer value:(id)value __attribute__((swift_name("encodeToResponseType(serializer:value:)")));
@end

__attribute__((swift_name("GameEngineFactory")))
@protocol ClientPluginGameEngineFactory
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onCreateEngineEngineConfig:(ClientPluginEngineConfig *)engineConfig externalDI:(ClientPluginKodein_diDIModule * _Nullable)externalDI completionHandler:(void (^)(id<ClientPluginKodein_diDI> _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("onCreateEngine(engineConfig:externalDI:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GameEngineFactoryImpl")))
@interface ClientPluginGameEngineFactoryImpl : ClientPluginBase <ClientPluginGameEngineFactory>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onCreateEngineEngineConfig:(ClientPluginEngineConfig *)engineConfig externalDI:(ClientPluginKodein_diDIModule * _Nullable)externalDI completionHandler:(void (^)(id<ClientPluginKodein_diDI> _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("onCreateEngine(engineConfig:externalDI:completionHandler:)")));
@end

__attribute__((swift_name("GameEngineLifecycle")))
@protocol ClientPluginGameEngineLifecycle
@required
- (void)onEngineCreatedId:(NSString *)id engineDI:(id<ClientPluginKodein_diDI>)engineDI __attribute__((swift_name("onEngineCreated(id:engineDI:)")));
- (void)onEngineDestroyedId:(NSString *)id __attribute__((swift_name("onEngineDestroyed(id:)")));
@end

__attribute__((swift_name("Game_clientClientGameHistoryInteractor")))
@protocol ClientPluginGame_clientClientGameHistoryInteractor
@required
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeEvents __attribute__((swift_name("observeEvents()")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)saveGameHistoryGameHistory:(ClientPluginEngineGameHistory *)gameHistory config:(ClientPluginEngineConfig_ *)config users:(ClientPluginKotlinArray<ClientPluginCoreGameUserInfo *> *)users gameType:(ClientPluginGame_clientGameType *)gameType completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("saveGameHistory(gameHistory:config:users:gameType:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)saveRoundHistoryGameHistory:(ClientPluginEngineGameHistory *)gameHistory config:(ClientPluginEngineConfig_ *)config users:(ClientPluginKotlinArray<ClientPluginCoreGameUserInfo *> *)users gameType:(ClientPluginGame_clientGameType *)gameType completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("saveRoundHistory(gameHistory:config:users:gameType:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PluginClientGameHistoryInteractorImpl")))
@interface ClientPluginPluginClientGameHistoryInteractorImpl : ClientPluginBase <ClientPluginGame_clientClientGameHistoryInteractor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeEvents __attribute__((swift_name("observeEvents()")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)saveGameHistoryGameHistory:(ClientPluginEngineGameHistory *)gameHistory config:(ClientPluginEngineConfig_ *)config users:(ClientPluginKotlinArray<ClientPluginCoreGameUserInfo *> *)users gameType:(ClientPluginGame_clientGameType *)gameType completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("saveGameHistory(gameHistory:config:users:gameType:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)saveRoundHistoryGameHistory:(ClientPluginEngineGameHistory *)gameHistory config:(ClientPluginEngineConfig_ *)config users:(ClientPluginKotlinArray<ClientPluginCoreGameUserInfo *> *)users gameType:(ClientPluginGame_clientGameType *)gameType completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("saveRoundHistory(gameHistory:config:users:gameType:completionHandler:)")));
@end

__attribute__((swift_name("Game_clientClientStatisticsInteractor")))
@protocol ClientPluginGame_clientClientStatisticsInteractor
@required
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeEvents __attribute__((swift_name("observeEvents()")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onGamePlayedWin:(BOOL)win completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onGamePlayed(win:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onRoundPlayedCommitted:(BOOL)committed win:(BOOL)win playerWin:(BOOL)playerWin completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onRoundPlayed(committed:win:playerWin:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PluginClientStatisticsInteractorImpl")))
@interface ClientPluginPluginClientStatisticsInteractorImpl : ClientPluginBase <ClientPluginGame_clientClientStatisticsInteractor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeEvents __attribute__((swift_name("observeEvents()")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onGamePlayedWin:(BOOL)win completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onGamePlayed(win:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onRoundPlayedCommitted:(BOOL)committed win:(BOOL)win playerWin:(BOOL)playerWin completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onRoundPlayed(committed:win:playerWin:completionHandler:)")));
@end

__attribute__((swift_name("Game_clientPersistentAppStateInteractor")))
@protocol ClientPluginGame_clientPersistentAppStateInteractor
@required
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeEvents __attribute__((swift_name("observeEvents()")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)removeGameId:(NSString *)gameId completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("remove(gameId:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)saveTable:(ClientPluginEngineJassTable *)table state:(ClientPluginGame_clientClientStatePayload *)state sendResponse:(BOOL)sendResponse completionHandler:(void (^)(NSString * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("save(table:state:sendResponse:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PluginPersistentAppStateInteractorImpl")))
@interface ClientPluginPluginPersistentAppStateInteractorImpl : ClientPluginBase <ClientPluginGame_clientPersistentAppStateInteractor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeEvents __attribute__((swift_name("observeEvents()")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)removeGameId:(NSString *)gameId completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("remove(gameId:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)saveTable:(ClientPluginEngineJassTable *)table state:(ClientPluginGame_clientClientStatePayload *)state sendResponse:(BOOL)sendResponse completionHandler:(void (^)(NSString * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("save(table:state:sendResponse:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GameEngineFactoryService")))
@interface ClientPluginGameEngineFactoryService : ClientPluginBase
- (instancetype)initWithGameEngineFactory:(id<ClientPluginGameEngineFactory>)gameEngineFactory engineEncoderDecoderProvider:(id<ClientPluginEngineEncoderDecoderProvider>)engineEncoderDecoderProvider clientPluginDecoderProvider:(id<ClientPluginClientPluginDecoderProvider>)clientPluginDecoderProvider json:(ClientPluginKotlinx_serialization_jsonJson *)json loggers:(ClientPluginEngineLoggers *)loggers __attribute__((swift_name("init(gameEngineFactory:engineEncoderDecoderProvider:clientPluginDecoderProvider:json:loggers:)"))) __attribute__((objc_designated_initializer));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeLogsArgs:(id)args __attribute__((swift_name("observeLogs(args:)")));
- (void)onAttachLifecycle:(id<ClientPluginGameEngineLifecycle>)lifecycle __attribute__((swift_name("onAttach(lifecycle:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onCreateEngineConfigJson:(NSString *)configJson completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("onCreateEngine(configJson:completionHandler:)")));
- (void)onDestroyEngineId:(NSString *)id __attribute__((swift_name("onDestroyEngine(id:)")));
- (void)onDetach __attribute__((swift_name("onDetach()")));
- (void)setEnvironmentProduction:(BOOL)production __attribute__((swift_name("setEnvironment(production:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GameEngineServices")))
@interface ClientPluginGameEngineServices : ClientPluginBase
- (instancetype)initWithMechanicService:(ClientPluginMechanicService *)mechanicService calcPointsSceneService:(ClientPluginCalcPointsSceneService *)calcPointsSceneService chatSceneService:(ClientPluginChatSceneService *)chatSceneService earnPointsSceneService:(ClientPluginEarnPointsSceneService *)earnPointsSceneService controlsContractService:(ClientPluginControlsContractService *)controlsContractService expectantContractService:(ClientPluginExpectantContractService *)expectantContractService playersSceneService:(ClientPluginPlayersSceneService *)playersSceneService sceneActionsService:(ClientPluginSceneActionsService *)sceneActionsService tradeSceneService:(ClientPluginTradeSceneService *)tradeSceneService tableSceneService:(ClientPluginTableSceneService *)tableSceneService choosePartnerSceneService:(ClientPluginChoosePartnerSceneService *)choosePartnerSceneService __attribute__((swift_name("init(mechanicService:calcPointsSceneService:chatSceneService:earnPointsSceneService:controlsContractService:expectantContractService:playersSceneService:sceneActionsService:tradeSceneService:tableSceneService:choosePartnerSceneService:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments __attribute__((swift_name("onMethodCall(methodName:arguments:)")));
@property (readonly) NSArray<ClientPluginEventSubscribe *> *subscriptions __attribute__((swift_name("subscriptions")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GameEngineUtilsService")))
@interface ClientPluginGameEngineUtilsService : ClientPluginBase
- (instancetype)initWithUserRatingInteractor:(ClientPluginEngineUserRatingInteractor *)userRatingInteractor format:(ClientPluginKotlinx_serialization_jsonJson *)format __attribute__((swift_name("init(userRatingInteractor:format:)"))) __attribute__((objc_designated_initializer));
- (NSString *)calculateNewUsersRatingJson:(NSString *)json __attribute__((swift_name("calculateNewUsersRating(json:)")));
- (NSString *)getAvailableLeaguesForUserRating:(double)rating __attribute__((swift_name("getAvailableLeaguesForUser(rating:)")));
- (NSString *)getLeaguesConfig __attribute__((swift_name("getLeaguesConfig()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MechanicService")))
@interface ClientPluginMechanicService : ClientPluginBase
- (instancetype)initWithMechanic:(id<ClientPluginGame_clientGameMechanic>)mechanic json:(ClientPluginKotlinx_serialization_jsonJson *)json clientPluginDecoderProvider:(id<ClientPluginClientPluginDecoderProvider>)clientPluginDecoderProvider clientEncoderDecoderProvider:(id<ClientPluginGame_clientClientEncoderDecoderProvider>)clientEncoderDecoderProvider __attribute__((swift_name("init(mechanic:json:clientPluginDecoderProvider:clientEncoderDecoderProvider:)"))) __attribute__((objc_designated_initializer));
- (void)clientMessageMessage:(NSString *)message __attribute__((swift_name("clientMessage(message:)")));
- (void)errorError:(NSString * _Nullable)error __attribute__((swift_name("error(error:)")));
- (void)foregroundForeground:(BOOL)foreground __attribute__((swift_name("foreground(foreground:)")));
- (void)gameSaved __attribute__((swift_name("gameSaved()")));
- (NSString *)getClientConfig __attribute__((swift_name("getClientConfig()")));
- (NSString *)getConfig __attribute__((swift_name("getConfig()")));
- (void)mechanicActionsAction:(NSString *)action __attribute__((swift_name("mechanicActions(action:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeChatOpened __attribute__((swift_name("observeChatOpened()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeClientConfig __attribute__((swift_name("observeClientConfig()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeConfig __attribute__((swift_name("observeConfig()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeError __attribute__((swift_name("observeError()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeFromClientAction __attribute__((swift_name("observeFromClientAction()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeGameHistoryEvents __attribute__((swift_name("observeGameHistoryEvents()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeGameInfo __attribute__((swift_name("observeGameInfo()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeGameLifecycle __attribute__((swift_name("observeGameLifecycle()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeInfoMessage __attribute__((swift_name("observeInfoMessage()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeLastBribe __attribute__((swift_name("observeLastBribe()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePersistentEvents __attribute__((swift_name("observePersistentEvents()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayersHandsMetadata __attribute__((swift_name("observePlayersHandsMetadata()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeRestartGameExpectant __attribute__((swift_name("observeRestartGameExpectant()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeScene __attribute__((swift_name("observeScene()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeSettingsOpened __attribute__((swift_name("observeSettingsOpened()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeShowTutorial __attribute__((swift_name("observeShowTutorial()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeStatisticsEvents __attribute__((swift_name("observeStatisticsEvents()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeUsers __attribute__((swift_name("observeUsers()")));
- (void)onCreate __attribute__((swift_name("onCreate()")));
- (void)onDestroy __attribute__((swift_name("onDestroy()")));
- (void)onStartSingleGameArguments:(NSString *)arguments __attribute__((swift_name("onStartSingleGame(arguments:)")));
- (void)playerExit __attribute__((swift_name("playerExit()")));
- (void)switchChatOpen:(BOOL)open __attribute__((swift_name("switchChat(open:)")));
- (void)switchGameInfoOpen:(BOOL)open __attribute__((swift_name("switchGameInfo(open:)")));
- (void)switchLastBribeOpen:(BOOL)open __attribute__((swift_name("switchLastBribe(open:)")));
- (void)switchSettingsOpen:(BOOL)open __attribute__((swift_name("switchSettings(open:)")));
- (void)tutorialPassedSkipped:(BOOL)skipped __attribute__((swift_name("tutorialPassed(skipped:)")));
- (void)updateConfigConfig:(NSString *)config __attribute__((swift_name("updateConfig(config:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MethodResult")))
@interface ClientPluginMethodResult : ClientPluginBase
- (instancetype)initWithResult:(id _Nullable)result __attribute__((swift_name("init(result:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginMethodResultCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginMethodResult *)doCopyResult:(id _Nullable)result __attribute__((swift_name("doCopy(result:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id _Nullable result __attribute__((swift_name("result")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MethodResult.Companion")))
@interface ClientPluginMethodResultCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginMethodResultCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginMethodResult *)successResult:(id _Nullable)result __attribute__((swift_name("success(result:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CalcPointsSceneService")))
@interface ClientPluginCalcPointsSceneService : ClientPluginBase
- (instancetype)initWithJson:(ClientPluginKotlinx_serialization_jsonJson *)json scene:(id<ClientPluginGame_clientCalcPointsScene>)scene __attribute__((swift_name("init(json:scene:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)getGameWinner __attribute__((swift_name("getGameWinner()")));
- (NSString *)getPoints __attribute__((swift_name("getPoints()")));
- (NSString *)getRoundWinner __attribute__((swift_name("getRoundWinner()")));
- (void)ready __attribute__((swift_name("ready()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ChatSceneService")))
@interface ClientPluginChatSceneService : ClientPluginBase
- (instancetype)initWithJson:(ClientPluginKotlinx_serialization_jsonJson *)json clientPluginDecoderProvider:(id<ClientPluginClientPluginDecoderProvider>)clientPluginDecoderProvider scene:(id<ClientPluginGame_clientChatScene>)scene __attribute__((swift_name("init(json:clientPluginDecoderProvider:scene:)"))) __attribute__((objc_designated_initializer));
- (NSString *)getMessages __attribute__((swift_name("getMessages()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeEnabledChat __attribute__((swift_name("observeEnabledChat()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeLeftVoiceCount __attribute__((swift_name("observeLeftVoiceCount()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeMessagesStrategy:(NSString *)strategy __attribute__((swift_name("observeMessages(strategy:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeNewMessage __attribute__((swift_name("observeNewMessage()")));
- (void)onMessageSent __attribute__((swift_name("onMessageSent()")));
- (void)sendMessageMessage:(NSString *)message __attribute__((swift_name("sendMessage(message:)")));
- (void)sendPhrasePhrase:(NSString *)phrase __attribute__((swift_name("sendPhrase(phrase:)")));
- (void)sendStickerUrl:(NSString *)url __attribute__((swift_name("sendSticker(url:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ChoosePartnerSceneService")))
@interface ClientPluginChoosePartnerSceneService : ClientPluginBase
- (instancetype)initWithClientPluginDecoderProvider:(id<ClientPluginClientPluginDecoderProvider>)clientPluginDecoderProvider scene:(id<ClientPluginGame_clientChoosePartnerScene>)scene __attribute__((swift_name("init(clientPluginDecoderProvider:scene:)"))) __attribute__((objc_designated_initializer));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeTeamDataExpectant __attribute__((swift_name("observeTeamDataExpectant()")));
- (void)partnerChoicePlayerId:(NSString *)playerId __attribute__((swift_name("partnerChoice(playerId:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EarnPointsSceneService")))
@interface ClientPluginEarnPointsSceneService : ClientPluginBase
- (instancetype)initWithClientPluginDecoderProvider:(id<ClientPluginClientPluginDecoderProvider>)clientPluginDecoderProvider scene:(id<ClientPluginGame_clientEarnPointsSceneContract>)scene json:(ClientPluginKotlinx_serialization_jsonJson *)json __attribute__((swift_name("init(clientPluginDecoderProvider:scene:json:)"))) __attribute__((objc_designated_initializer));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeCardOnTheTableByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observeCardOnTheTableById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeCardsOnTheTable __attribute__((swift_name("observeCardsOnTheTable()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeValidationError __attribute__((swift_name("observeValidationError()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeWinnerCombinationsExpectant __attribute__((swift_name("observeWinnerCombinationsExpectant()")));
- (void)onCardPutCard:(ClientPluginCoreProcessingCard *)card __attribute__((swift_name("onCardPut(card:)")));
- (void)onClickDisabledCardIndex:(int32_t)index __attribute__((swift_name("onClickDisabledCard(index:)")));
@property (readonly) ClientPluginKotlinx_serialization_jsonJson *json __attribute__((swift_name("json")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TableSceneService")))
@interface ClientPluginTableSceneService : ClientPluginBase
- (instancetype)initWithJson:(ClientPluginKotlinx_serialization_jsonJson *)json clientPluginDecoderProvider:(id<ClientPluginClientPluginDecoderProvider>)clientPluginDecoderProvider scene:(id<ClientPluginGame_clientTableScene>)scene __attribute__((swift_name("init(json:clientPluginDecoderProvider:scene:)"))) __attribute__((objc_designated_initializer));
- (void)combinationChoiceIsAccepted:(BOOL)isAccepted __attribute__((swift_name("combinationChoice(isAccepted:)")));
- (NSString *)getCardDeck __attribute__((swift_name("getCardDeck()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeAskAboutCombinations __attribute__((swift_name("observeAskAboutCombinations()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeCardDeck __attribute__((swift_name("observeCardDeck()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeCombinationsExpectant __attribute__((swift_name("observeCombinationsExpectant()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeControlsMode __attribute__((swift_name("observeControlsMode()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeLastBribeEnabled __attribute__((swift_name("observeLastBribeEnabled()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerHandByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerHandById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayersCards __attribute__((swift_name("observePlayersCards()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeTrump __attribute__((swift_name("observeTrump()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TradeSceneService")))
@interface ClientPluginTradeSceneService : ClientPluginBase
- (instancetype)initWithClientPluginDecoderProvider:(id<ClientPluginClientPluginDecoderProvider>)clientPluginDecoderProvider json:(ClientPluginKotlinx_serialization_jsonJson *)json scene:(id<ClientPluginGame_clientTradeSceneContract>)scene __attribute__((swift_name("init(clientPluginDecoderProvider:json:scene:)"))) __attribute__((objc_designated_initializer));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeTrumpSelectedExpectant __attribute__((swift_name("observeTrumpSelectedExpectant()")));
- (void)pass __attribute__((swift_name("pass()")));
- (void)playSuit:(NSString *)suit __attribute__((swift_name("play(suit:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ControlsContractService")))
@interface ClientPluginControlsContractService : ClientPluginBase
- (instancetype)initWithContract:(id<ClientPluginGame_clientControlsContract>)contract __attribute__((swift_name("init(contract:)"))) __attribute__((objc_designated_initializer));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeControlsEnabled __attribute__((swift_name("observeControlsEnabled()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ExpectantContractService")))
@interface ClientPluginExpectantContractService : ClientPluginBase
- (instancetype)initWithContract:(id<ClientPluginGame_clientExpectantContract>)contract __attribute__((swift_name("init(contract:)"))) __attribute__((objc_designated_initializer));
- (void)expectantStageCompletedExpectantId:(NSString *)expectantId __attribute__((swift_name("expectantStageCompleted(expectantId:)")));
- (void)expectantTimerFinishedExpectantId:(NSString *)expectantId __attribute__((swift_name("expectantTimerFinished(expectantId:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PlayersSceneService")))
@interface ClientPluginPlayersSceneService : ClientPluginBase
- (instancetype)initWithJson:(ClientPluginKotlinx_serialization_jsonJson *)json clientPluginDecoderProvider:(id<ClientPluginClientPluginDecoderProvider>)clientPluginDecoderProvider contract:(id<ClientPluginGame_clientPlayersSceneContract>)contract __attribute__((swift_name("init(json:clientPluginDecoderProvider:contract:)"))) __attribute__((objc_designated_initializer));
- (NSString *)getPlayers __attribute__((swift_name("getPlayers()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerConnectionStateByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerConnectionStateById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerMessageByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerMessageById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerTurnByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerTurnById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerTurns __attribute__((swift_name("observePlayerTurns()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayers __attribute__((swift_name("observePlayers()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SceneActionsService")))
@interface ClientPluginSceneActionsService : ClientPluginBase
- (instancetype)initWithContract:(id<ClientPluginGame_clientSceneActionsContract>)contract __attribute__((swift_name("init(contract:)"))) __attribute__((objc_designated_initializer));
- (void)setSceneActiveStateIsActive:(BOOL)isActive __attribute__((swift_name("setSceneActiveState(isActive:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineLoggers")))
@interface ClientPluginEngineLoggers : ClientPluginBase
- (instancetype)initWithLogOutputs:(ClientPluginKotlinArray<id<ClientPluginLoggerLoggerOutput>> *)logOutputs debugLoggerOutput:(id<ClientPluginLoggerLoggerEnabledTrigger> _Nullable)debugLoggerOutput releaseLoggerOutput:(id<ClientPluginLoggerLoggerEnabledTrigger> _Nullable)releaseLoggerOutput observableLoggerOutput:(id<ClientPluginCoreObservableLoggerOutput> _Nullable)observableLoggerOutput showErrorLoggerOutput:(id<ClientPluginGame_clientShowErrorLoggerOutput> _Nullable)showErrorLoggerOutput __attribute__((swift_name("init(logOutputs:debugLoggerOutput:releaseLoggerOutput:observableLoggerOutput:showErrorLoggerOutput:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineLoggersCompanion *companion __attribute__((swift_name("companion")));
@property (readonly) ClientPluginLoggerCombinedRaspberryLogger *combinedLogger __attribute__((swift_name("combinedLogger")));
@property (readonly) id<ClientPluginLoggerLoggerEnabledTrigger> _Nullable debugLoggerOutput __attribute__((swift_name("debugLoggerOutput")));
@property (readonly) id<ClientPluginCoreObservableLoggerOutput> _Nullable observableLoggerOutput __attribute__((swift_name("observableLoggerOutput")));
@property (readonly) id<ClientPluginLoggerLoggerEnabledTrigger> _Nullable releaseLoggerOutput __attribute__((swift_name("releaseLoggerOutput")));
@property (readonly) id<ClientPluginGame_clientShowErrorLoggerOutput> _Nullable showErrorLoggerOutput __attribute__((swift_name("showErrorLoggerOutput")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineLoggers.Companion")))
@interface ClientPluginEngineLoggersCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineLoggersCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineLoggers *)create __attribute__((swift_name("create()")));
@end

__attribute__((swift_name("ChannelSubscription")))
@protocol ClientPluginChannelSubscription
@required
- (void)cancel __attribute__((swift_name("cancel()")));
@property (readonly) BOOL isActive __attribute__((swift_name("isActive")));
@property (readonly) NSString *path __attribute__((swift_name("path")));
@end

__attribute__((swift_name("EventController")))
@protocol ClientPluginEventController
@required
- (void)endOfStream __attribute__((swift_name("endOfStream()")));
- (void)errorError:(ClientPluginPluginError *)error __attribute__((swift_name("error(error:)")));
- (void)successData:(id _Nullable)data __attribute__((swift_name("success(data:)")));
@end

__attribute__((swift_name("EventSubscriptionCallback")))
@protocol ClientPluginEventSubscriptionCallback
@required
- (void)onCancelArgument:(id _Nullable)argument __attribute__((swift_name("onCancel(argument:)")));
- (void)onListenSink:(id<ClientPluginEventController>)sink argument:(id _Nullable)argument __attribute__((swift_name("onListen(sink:argument:)")));
@end

__attribute__((swift_name("MethodController")))
@protocol ClientPluginMethodController
@required
- (void)errorError:(ClientPluginPluginError *)error __attribute__((swift_name("error(error:)")));
- (id _Nullable)getArguments __attribute__((swift_name("getArguments()")));
- (NSString *)getMethodName __attribute__((swift_name("getMethodName()")));
- (void)notImplemented __attribute__((swift_name("notImplemented()")));
- (void)successEventData:(id _Nullable)eventData __attribute__((swift_name("success(eventData:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PluginError")))
@interface ClientPluginPluginError : ClientPluginKotlinException
- (instancetype)initWithErrorCode:(NSString *)errorCode message:(NSString * _Nullable)message cause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(errorCode:message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (ClientPluginPluginError *)doCopyErrorCode:(NSString *)errorCode message:(NSString * _Nullable)message cause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("doCopy(errorCode:message:cause:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString *errorCode __attribute__((swift_name("errorCode")));
@property (readonly) NSString * _Nullable errorDetails __attribute__((swift_name("errorDetails")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end

__attribute__((swift_name("JobSubscriptions")))
@protocol ClientPluginJobSubscriptions
@required
- (void)onCancel __attribute__((swift_name("onCancel()")));
- (void)onListenEventSink:(id<ClientPluginEventController>)eventSink __attribute__((swift_name("onListen(eventSink:)")));
- (void)subscribeRequest:(ClientPluginEventSubscriptionRequestDto *)request subscribe:(ClientPluginEventSubscribe *)subscribe __attribute__((swift_name("subscribe(request:subscribe:)")));
- (void)unsubscribeRequest_:(ClientPluginEventSubscriptionRequestDto *)request __attribute__((swift_name("unsubscribe(request_:)")));
@property (readonly) BOOL isActive __attribute__((swift_name("isActive")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LazyFlow")))
@interface ClientPluginLazyFlow : ClientPluginBase
- (instancetype)initWithPath:(NSString *)path flow:(id<ClientPluginKotlinx_coroutines_coreFlow>)flow __attribute__((swift_name("init(path:flow:)"))) __attribute__((objc_designated_initializer));
- (void)cancel __attribute__((swift_name("cancel()")));
- (void)subscribeJson:(ClientPluginKotlinx_serialization_jsonJson *)json main:(id<ClientPluginKotlinx_coroutines_coreCoroutineScope>)main onResult:(void (^)(NSString *))onResult __attribute__((swift_name("subscribe(json:main:onResult:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isActive __attribute__((swift_name("isActive")));
@property (readonly) NSString *path __attribute__((swift_name("path")));
@end

@interface ClientPluginDeveloperConfig (Extensions)
- (ClientPluginDeveloperConfigDto *)mapToDto __attribute__((swift_name("mapToDto()")));
@end

@interface ClientPluginGameEngineFactoryService (Extensions)
- (NSArray<ClientPluginEventSubscribe *> *)getSubscriptions __attribute__((swift_name("getSubscriptions()")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments completionHandler:(void (^)(ClientPluginMethodResult * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("onMethodCall(methodName:arguments:completionHandler:)")));
@end

@interface ClientPluginGameEngineUtilsService (Extensions)
- (NSArray<ClientPluginEventSubscribe *> *)getSubscriptions __attribute__((swift_name("getSubscriptions()")));
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments __attribute__((swift_name("onMethodCall(methodName:arguments:)")));
@end

@interface ClientPluginMechanicService (Extensions)
- (NSArray<ClientPluginEventSubscribe *> *)getSubscriptions __attribute__((swift_name("getSubscriptions()")));
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments __attribute__((swift_name("onMethodCall(methodName:arguments:)")));
@end

@interface ClientPluginCalcPointsSceneService (Extensions)
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName __attribute__((swift_name("onMethodCall(methodName:)")));
@end

@interface ClientPluginChatSceneService (Extensions)
- (NSArray<ClientPluginEventSubscribe *> *)getSubscriptions __attribute__((swift_name("getSubscriptions()")));
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments __attribute__((swift_name("onMethodCall(methodName:arguments:)")));
@end

@interface ClientPluginChoosePartnerSceneService (Extensions)
- (NSArray<ClientPluginEventSubscribe *> *)getSubscriptions __attribute__((swift_name("getSubscriptions()")));
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments __attribute__((swift_name("onMethodCall(methodName:arguments:)")));
@end

@interface ClientPluginEarnPointsSceneService (Extensions)
- (NSArray<ClientPluginEventSubscribe *> *)getSubscriptions __attribute__((swift_name("getSubscriptions()")));
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments __attribute__((swift_name("onMethodCall(methodName:arguments:)")));
@end

@interface ClientPluginTableSceneService (Extensions)
- (NSArray<ClientPluginEventSubscribe *> *)getSubscriptions __attribute__((swift_name("getSubscriptions()")));
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments __attribute__((swift_name("onMethodCall(methodName:arguments:)")));
@end

@interface ClientPluginTradeSceneService (Extensions)
- (NSArray<ClientPluginEventSubscribe *> *)getSubscriptions __attribute__((swift_name("getSubscriptions()")));
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments __attribute__((swift_name("onMethodCall(methodName:arguments:)")));
@end

@interface ClientPluginControlsContractService (Extensions)
- (NSArray<ClientPluginEventSubscribe *> *)getSubscriptions __attribute__((swift_name("getSubscriptions()")));
@end

@interface ClientPluginExpectantContractService (Extensions)
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments __attribute__((swift_name("onMethodCall(methodName:arguments:)")));
@end

@interface ClientPluginPlayersSceneService (Extensions)
- (NSArray<ClientPluginEventSubscribe *> *)getSubscriptions __attribute__((swift_name("getSubscriptions()")));
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName __attribute__((swift_name("onMethodCall(methodName:)")));
@end

@interface ClientPluginSceneActionsService (Extensions)
- (ClientPluginMethodResult * _Nullable)onMethodCallMethodName:(NSString *)methodName arguments:(id _Nullable)arguments __attribute__((swift_name("onMethodCall(methodName:arguments:)")));
@end

@interface ClientPluginKotlinThrowable (Extensions)
@property (readonly) ClientPluginPluginError *pluginError __attribute__((swift_name("pluginError")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ClientPluginDecoderProviderKt")))
@interface ClientPluginClientPluginDecoderProviderKt : ClientPluginBase
+ (id)decodeArgs:(id<ClientPluginClientPluginDecoderProvider>)receiver args:(id)args __attribute__((swift_name("decodeArgs(_:args:)")));
+ (ClientPluginResponseTypeDto *)encodeToResponseType:(id<ClientPluginClientPluginDecoderProvider>)receiver value:(id)value __attribute__((swift_name("encodeToResponseType(_:value:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CorutinesExtKt")))
@interface ClientPluginCorutinesExtKt : ClientPluginBase
+ (id<ClientPluginKotlinx_coroutines_coreJob>)launchFlow:(id<ClientPluginKotlinx_coroutines_coreCoroutineScope>)receiver createFlow:(id<ClientPluginKotlinx_coroutines_coreFlow> (^)(void))createFlow onSuccess:(void (^)(id _Nullable))onSuccess onError:(void (^)(ClientPluginKotlinThrowable *))onError onComplete:(void (^)(void))onComplete __attribute__((swift_name("launchFlow(_:createFlow:onSuccess:onError:onComplete:)")));
+ (id<ClientPluginKotlinx_coroutines_coreFlow>)onDefault:(id<ClientPluginKotlinx_coroutines_coreFlow>)receiver handler:(void (^)(ClientPluginKotlinThrowable *))handler __attribute__((swift_name("onDefault(_:handler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
+ (void)safeCollect:(id<ClientPluginKotlinx_coroutines_coreFlow>)receiver action:(id<ClientPluginKotlinSuspendFunction1>)action completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("safeCollect(_:action:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DIKt")))
@interface ClientPluginDIKt : ClientPluginBase
+ (ClientPluginKodein_diDIModule *)clientPluginDecodersModule __attribute__((swift_name("clientPluginDecodersModule()")));
+ (ClientPluginKodein_diDIModule *)gameEngineFactoryModuleLoggers:(ClientPluginEngineLoggers *)loggers __attribute__((swift_name("gameEngineFactoryModule(loggers:)")));
@property (class, readonly) ClientPluginKodein_diDIModule *gameEngineServicesModule __attribute__((swift_name("gameEngineServicesModule")));
@property (class, readonly) ClientPluginKodein_diDIModule *jobsModule __attribute__((swift_name("jobsModule")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QueryUtilsKt")))
@interface ClientPluginQueryUtilsKt : ClientPluginBase
+ (NSString * _Nullable)getQuery:(NSString *)receiver queryName:(NSString *)queryName __attribute__((swift_name("getQuery(_:queryName:)")));
+ (ClientPluginKotlinPair<NSString *, NSString *> *)parseGameIDQuery:(NSString *)receiver __attribute__((swift_name("parseGameIDQuery(_:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SubscriptionHelperKt")))
@interface ClientPluginSubscriptionHelperKt : ClientPluginBase
+ (id<ClientPluginKotlinx_coroutines_coreFlow>)mapWithEventSubscriptionResponse:(id<ClientPluginKotlinx_coroutines_coreFlow>)receiver path:(NSString *)path __attribute__((swift_name("mapWithEventSubscriptionResponse(_:path:)")));
+ (id<ClientPluginChannelSubscription>)subscribeJob:(id<ClientPluginKotlinx_coroutines_coreCoroutineScope>)receiver path:(NSString *)path sink:(id<ClientPluginEventController>)sink createFlow:(id<ClientPluginKotlinx_coroutines_coreFlow> (^)(void))createFlow __attribute__((swift_name("subscribeJob(_:path:sink:createFlow:)")));
@end

__attribute__((swift_name("KotlinIllegalStateException")))
@interface ClientPluginKotlinIllegalStateException : ClientPluginKotlinRuntimeException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.4")
*/
__attribute__((swift_name("KotlinCancellationException")))
@interface ClientPluginKotlinCancellationException : ClientPluginKotlinIllegalStateException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(ClientPluginKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("LoggerLoggerOutput")))
@protocol ClientPluginLoggerLoggerOutput
@required
- (void)dTag:(NSString * _Nullable)tag message:(NSString *)message payload:(id _Nullable)payload __attribute__((swift_name("d(tag:message:payload:)")));
- (void)eTag:(NSString * _Nullable)tag message:(NSString * _Nullable)message exception:(ClientPluginKotlinThrowable * _Nullable)exception payload:(id _Nullable)payload __attribute__((swift_name("e(tag:message:exception:payload:)")));
- (void)iTag:(NSString * _Nullable)tag message:(NSString *)message payload:(id _Nullable)payload __attribute__((swift_name("i(tag:message:payload:)")));
- (void)vTag:(NSString * _Nullable)tag message:(NSString * _Nullable)message exception:(ClientPluginKotlinThrowable * _Nullable)exception payload:(id _Nullable)payload __attribute__((swift_name("v(tag:message:exception:payload:)")));
- (void)wTag:(NSString * _Nullable)tag message:(NSString * _Nullable)message exception:(ClientPluginKotlinThrowable * _Nullable)exception payload:(id _Nullable)payload __attribute__((swift_name("w(tag:message:exception:payload:)")));
@end

__attribute__((swift_name("LoggerRaspberryLogger")))
@protocol ClientPluginLoggerRaspberryLogger <ClientPluginLoggerLoggerOutput>
@required
@end

__attribute__((swift_name("LoggerLoggerEnabledTrigger")))
@protocol ClientPluginLoggerLoggerEnabledTrigger <ClientPluginLoggerRaspberryLogger>
@required
@property BOOL enabled __attribute__((swift_name("enabled")));
@end

__attribute__((swift_name("CoreObservableLoggerOutput")))
@protocol ClientPluginCoreObservableLoggerOutput <ClientPluginLoggerLoggerOutput, ClientPluginLoggerLoggerEnabledTrigger>
@required
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeTypes:(ClientPluginKotlinArray<ClientPluginCoreLogType *> *)types __attribute__((swift_name("observe(types:)")));
@property NSString * _Nullable (^payloadToKeyMapper)(id _Nullable) __attribute__((swift_name("payloadToKeyMapper")));
@end

__attribute__((swift_name("Game_clientShowErrorLoggerOutput")))
@protocol ClientPluginGame_clientShowErrorLoggerOutput <ClientPluginLoggerLoggerOutput>
@required
@property id<ClientPluginGame_clientGameClient> (^ _Nullable gameClientProvider)(void) __attribute__((swift_name("gameClientProvider")));
@property BOOL skipWarnings __attribute__((swift_name("skipWarnings")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface ClientPluginKotlinArray<T> : ClientPluginBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(ClientPluginInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<ClientPluginKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientClientConfig")))
@interface ClientPluginGame_clientClientConfig : ClientPluginBase
- (instancetype)initWithGameType:(ClientPluginGame_clientGameType *)gameType sortedSuits:(NSArray<ClientPluginCoreSuit *> *)sortedSuits __attribute__((swift_name("init(gameType:sortedSuits:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientClientConfig *)doCopyGameType:(ClientPluginGame_clientGameType *)gameType sortedSuits:(NSArray<ClientPluginCoreSuit *> *)sortedSuits __attribute__((swift_name("doCopy(gameType:sortedSuits:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginGame_clientGameType *gameType __attribute__((swift_name("gameType")));
@property (readonly) BOOL isMultiplayer __attribute__((swift_name("isMultiplayer")));
@property (readonly) BOOL isP2P __attribute__((swift_name("isP2P")));
@property (readonly) BOOL isSingle __attribute__((swift_name("isSingle")));
@property (readonly) BOOL isTutorial __attribute__((swift_name("isTutorial")));
@property (readonly) NSArray<ClientPluginCoreSuit *> *sortedSuits __attribute__((swift_name("sortedSuits")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCoreConfig")))
@interface ClientPluginCoreCoreConfig : ClientPluginBase
- (instancetype)initWithFileLogsEnabled:(BOOL)fileLogsEnabled deleteFilesOnClear:(BOOL)deleteFilesOnClear logsDirectoryPath:(NSString * _Nullable)logsDirectoryPath fileNameFormat:(NSString *)fileNameFormat __attribute__((swift_name("init(fileLogsEnabled:deleteFilesOnClear:logsDirectoryPath:fileNameFormat:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginCoreCoreConfigCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginCoreCoreConfig *)doCopyFileLogsEnabled:(BOOL)fileLogsEnabled deleteFilesOnClear:(BOOL)deleteFilesOnClear logsDirectoryPath:(NSString * _Nullable)logsDirectoryPath fileNameFormat:(NSString *)fileNameFormat __attribute__((swift_name("doCopy(fileLogsEnabled:deleteFilesOnClear:logsDirectoryPath:fileNameFormat:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL deleteFilesOnClear __attribute__((swift_name("deleteFilesOnClear")));
@property (readonly) BOOL fileLogsEnabled __attribute__((swift_name("fileLogsEnabled")));
@property (readonly) NSString *fileNameFormat __attribute__((swift_name("fileNameFormat")));
@property (readonly) NSString * _Nullable logsDirectoryPath __attribute__((swift_name("logsDirectoryPath")));
@property (readonly) NSString *requireDirectoryPath __attribute__((swift_name("requireDirectoryPath")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineEngineLogicConfig")))
@interface ClientPluginEngineEngineLogicConfig : ClientPluginBase
- (instancetype)initWithSupportBackwardActions:(BOOL)supportBackwardActions validateTestMode:(BOOL)validateTestMode verboseGameHistory:(BOOL)verboseGameHistory gameHistoryAnalytics:(BOOL)gameHistoryAnalytics updateRating:(BOOL)updateRating validateFromClientActionTags:(BOOL)validateFromClientActionTags validateIfMainThread:(BOOL)validateIfMainThread playerTimeoutFactorToFinishStep:(double)playerTimeoutFactorToFinishStep openRoomIfFinished:(BOOL)openRoomIfFinished minPlayersToOpenRoom:(int32_t)minPlayersToOpenRoom __attribute__((swift_name("init(supportBackwardActions:validateTestMode:verboseGameHistory:gameHistoryAnalytics:updateRating:validateFromClientActionTags:validateIfMainThread:playerTimeoutFactorToFinishStep:openRoomIfFinished:minPlayersToOpenRoom:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineEngineLogicConfigCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineEngineLogicConfig *)doCopySupportBackwardActions:(BOOL)supportBackwardActions validateTestMode:(BOOL)validateTestMode verboseGameHistory:(BOOL)verboseGameHistory gameHistoryAnalytics:(BOOL)gameHistoryAnalytics updateRating:(BOOL)updateRating validateFromClientActionTags:(BOOL)validateFromClientActionTags validateIfMainThread:(BOOL)validateIfMainThread playerTimeoutFactorToFinishStep:(double)playerTimeoutFactorToFinishStep openRoomIfFinished:(BOOL)openRoomIfFinished minPlayersToOpenRoom:(int32_t)minPlayersToOpenRoom __attribute__((swift_name("doCopy(supportBackwardActions:validateTestMode:verboseGameHistory:gameHistoryAnalytics:updateRating:validateFromClientActionTags:validateIfMainThread:playerTimeoutFactorToFinishStep:openRoomIfFinished:minPlayersToOpenRoom:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL gameHistoryAnalytics __attribute__((swift_name("gameHistoryAnalytics")));
@property (readonly) int32_t minPlayersToOpenRoom __attribute__((swift_name("minPlayersToOpenRoom")));
@property (readonly) BOOL openRoomIfFinished __attribute__((swift_name("openRoomIfFinished")));
@property (readonly) double playerTimeoutFactorToFinishStep __attribute__((swift_name("playerTimeoutFactorToFinishStep")));
@property (readonly) BOOL supportBackwardActions __attribute__((swift_name("supportBackwardActions")));
@property (readonly) BOOL updateRating __attribute__((swift_name("updateRating")));
@property (readonly) BOOL validateFromClientActionTags __attribute__((swift_name("validateFromClientActionTags")));
@property (readonly) BOOL validateIfMainThread __attribute__((swift_name("validateIfMainThread")));
@property (readonly) BOOL validateTestMode __attribute__((swift_name("validateTestMode")));
@property (readonly) BOOL verboseGameHistory __attribute__((swift_name("verboseGameHistory")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineEngineBotConfig")))
@interface ClientPluginEngineEngineBotConfig : ClientPluginBase
- (instancetype)initWithBotExitsWhenNGameNumberIsFinished:(ClientPluginInt * _Nullable)botExitsWhenNGameNumberIsFinished autoStepLastCard:(BOOL)autoStepLastCard simulateBotThinkingDelay:(BOOL)simulateBotThinkingDelay isDelayForBot:(BOOL)isDelayForBot __attribute__((swift_name("init(botExitsWhenNGameNumberIsFinished:autoStepLastCard:simulateBotThinkingDelay:isDelayForBot:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineEngineBotConfig *)doCopyBotExitsWhenNGameNumberIsFinished:(ClientPluginInt * _Nullable)botExitsWhenNGameNumberIsFinished autoStepLastCard:(BOOL)autoStepLastCard simulateBotThinkingDelay:(BOOL)simulateBotThinkingDelay isDelayForBot:(BOOL)isDelayForBot __attribute__((swift_name("doCopy(botExitsWhenNGameNumberIsFinished:autoStepLastCard:simulateBotThinkingDelay:isDelayForBot:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL autoStepLastCard __attribute__((swift_name("autoStepLastCard")));
@property (readonly) ClientPluginInt * _Nullable botExitsWhenNGameNumberIsFinished __attribute__((swift_name("botExitsWhenNGameNumberIsFinished")));
@property (readonly) BOOL isDelayForBot __attribute__((swift_name("isDelayForBot")));
@property (readonly) BOOL simulateBotThinkingDelay __attribute__((swift_name("simulateBotThinkingDelay")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineDealerConfig")))
@interface ClientPluginEngineDealerConfig : ClientPluginBase
- (instancetype)initWithDealerStrategy:(ClientPluginEngineDealerStrategy *)dealerStrategy isBotsAreHappy:(BOOL)isBotsAreHappy cards:(ClientPluginKotlinArray<ClientPluginInt *> * _Nullable)cards shouldDisplayDealerStrategy:(BOOL)shouldDisplayDealerStrategy __attribute__((swift_name("init(dealerStrategy:isBotsAreHappy:cards:shouldDisplayDealerStrategy:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineDealerConfigCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineDealerConfig *)doCopyDealerStrategy:(ClientPluginEngineDealerStrategy *)dealerStrategy isBotsAreHappy:(BOOL)isBotsAreHappy cards:(ClientPluginKotlinArray<ClientPluginInt *> * _Nullable)cards shouldDisplayDealerStrategy:(BOOL)shouldDisplayDealerStrategy __attribute__((swift_name("doCopy(dealerStrategy:isBotsAreHappy:cards:shouldDisplayDealerStrategy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginKotlinArray<ClientPluginInt *> * _Nullable cards __attribute__((swift_name("cards")));
@property (readonly) ClientPluginEngineDealerStrategy *dealerStrategy __attribute__((swift_name("dealerStrategy")));
@property (readonly) BOOL isBotsAreHappy __attribute__((swift_name("isBotsAreHappy")));
@property (readonly) BOOL shouldDisplayDealerStrategy __attribute__((swift_name("shouldDisplayDealerStrategy")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientGameClientConfig")))
@interface ClientPluginGame_clientGameClientConfig : ClientPluginBase
- (instancetype)initWithValidateSceneActiveState:(BOOL)validateSceneActiveState validateCardOnMove:(BOOL)validateCardOnMove validateExpectantActions:(BOOL)validateExpectantActions isAutoStepCurrentPlayer:(BOOL)isAutoStepCurrentPlayer areOpponentsCardsOpened:(BOOL)areOpponentsCardsOpened sessionAnalytics:(BOOL)sessionAnalytics showPlayerConnectionState:(BOOL)showPlayerConnectionState __attribute__((swift_name("init(validateSceneActiveState:validateCardOnMove:validateExpectantActions:isAutoStepCurrentPlayer:areOpponentsCardsOpened:sessionAnalytics:showPlayerConnectionState:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginGame_clientGameClientConfigCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginGame_clientGameClientConfig *)doCopyValidateSceneActiveState:(BOOL)validateSceneActiveState validateCardOnMove:(BOOL)validateCardOnMove validateExpectantActions:(BOOL)validateExpectantActions isAutoStepCurrentPlayer:(BOOL)isAutoStepCurrentPlayer areOpponentsCardsOpened:(BOOL)areOpponentsCardsOpened sessionAnalytics:(BOOL)sessionAnalytics showPlayerConnectionState:(BOOL)showPlayerConnectionState __attribute__((swift_name("doCopy(validateSceneActiveState:validateCardOnMove:validateExpectantActions:isAutoStepCurrentPlayer:areOpponentsCardsOpened:sessionAnalytics:showPlayerConnectionState:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL areOpponentsCardsOpened __attribute__((swift_name("areOpponentsCardsOpened")));
@property (readonly) BOOL isAutoStepCurrentPlayer __attribute__((swift_name("isAutoStepCurrentPlayer")));
@property (readonly) BOOL sessionAnalytics __attribute__((swift_name("sessionAnalytics")));
@property (readonly) BOOL showPlayerConnectionState __attribute__((swift_name("showPlayerConnectionState")));
@property (readonly) BOOL validateCardOnMove __attribute__((swift_name("validateCardOnMove")));
@property (readonly) BOOL validateExpectantActions __attribute__((swift_name("validateExpectantActions")));
@property (readonly) BOOL validateSceneActiveState __attribute__((swift_name("validateSceneActiveState")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinEnumCompanion")))
@interface ClientPluginKotlinEnumCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginKotlinEnumCompanion *shared __attribute__((swift_name("shared")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreFlow")))
@protocol ClientPluginKotlinx_coroutines_coreFlow
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)collectCollector:(id<ClientPluginKotlinx_coroutines_coreFlowCollector>)collector completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("collect(collector:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineConfig_")))
@interface ClientPluginEngineConfig_ : ClientPluginBase
- (instancetype)initWithRules:(ClientPluginEngineRules *)rules rulesSetType:(ClientPluginEngineRulesSetType *)rulesSetType options:(ClientPluginEngineOptions *)options playersMode:(ClientPluginEnginePlayersMode *)playersMode pointsMode:(ClientPluginEnginePointsMode *)pointsMode timeoutTimeMillis:(int32_t)timeoutTimeMillis isPrivate:(BOOL)isPrivate dealerStrategy:(ClientPluginEngineDealerStrategy * _Nullable)dealerStrategy league:(ClientPluginEngineLeague * _Nullable)league mode:(ClientPluginEngineRoomMode *)mode __attribute__((swift_name("init(rules:rulesSetType:options:playersMode:pointsMode:timeoutTimeMillis:isPrivate:dealerStrategy:league:mode:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineConfig_ *)doCopyRules:(ClientPluginEngineRules *)rules rulesSetType:(ClientPluginEngineRulesSetType *)rulesSetType options:(ClientPluginEngineOptions *)options playersMode:(ClientPluginEnginePlayersMode *)playersMode pointsMode:(ClientPluginEnginePointsMode *)pointsMode timeoutTimeMillis:(int32_t)timeoutTimeMillis isPrivate:(BOOL)isPrivate dealerStrategy:(ClientPluginEngineDealerStrategy * _Nullable)dealerStrategy league:(ClientPluginEngineLeague * _Nullable)league mode:(ClientPluginEngineRoomMode *)mode __attribute__((swift_name("doCopy(rules:rulesSetType:options:playersMode:pointsMode:timeoutTimeMillis:isPrivate:dealerStrategy:league:mode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginEngineDealerStrategy * _Nullable dealerStrategy __attribute__((swift_name("dealerStrategy")));
@property (readonly) BOOL isPrivate __attribute__((swift_name("isPrivate")));
@property (readonly) ClientPluginEngineLeague * _Nullable league __attribute__((swift_name("league")));
@property (readonly) ClientPluginEngineRoomMode *mode __attribute__((swift_name("mode")));
@property (readonly) ClientPluginEngineOptions *options __attribute__((swift_name("options")));
@property (readonly) int32_t playerHandCardsSize __attribute__((swift_name("playerHandCardsSize")));
@property (readonly) ClientPluginEnginePlayersMode *playersMode __attribute__((swift_name("playersMode")));
@property (readonly) ClientPluginEnginePointsMode *pointsMode __attribute__((swift_name("pointsMode")));
@property (readonly) ClientPluginEngineRules *rules __attribute__((swift_name("rules")));
@property (readonly) ClientPluginEngineRulesSetType *rulesSetType __attribute__((swift_name("rulesSetType")));
@property (readonly) int32_t timeoutTimeMillis __attribute__((swift_name("timeoutTimeMillis")));
@end

__attribute__((swift_name("CorePlayerIdContract")))
@protocol ClientPluginCorePlayerIdContract
@required
@property (readonly) NSString *playerId __attribute__((swift_name("playerId")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreGameUserInfo")))
@interface ClientPluginCoreGameUserInfo : ClientPluginBase <ClientPluginCorePlayerIdContract>
- (instancetype)initWithPlayerId:(NSString *)playerId name:(NSString *)name avatarUrl:(NSString * _Nullable)avatarUrl isBot:(BOOL)isBot initialLuckyFactor:(ClientPluginFloat * _Nullable)initialLuckyFactor rating:(ClientPluginDouble * _Nullable)rating payload:(id _Nullable)payload __attribute__((swift_name("init(playerId:name:avatarUrl:isBot:initialLuckyFactor:rating:payload:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginCoreGameUserInfo *)doCopyPlayerId:(NSString *)playerId name:(NSString *)name avatarUrl:(NSString * _Nullable)avatarUrl isBot:(BOOL)isBot initialLuckyFactor:(ClientPluginFloat * _Nullable)initialLuckyFactor rating:(ClientPluginDouble * _Nullable)rating payload:(id _Nullable)payload __attribute__((swift_name("doCopy(playerId:name:avatarUrl:isBot:initialLuckyFactor:rating:payload:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (double)getRatingOrDefaultInitialUserRating:(int32_t)initialUserRating __attribute__((swift_name("getRatingOrDefault(initialUserRating:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable avatarUrl __attribute__((swift_name("avatarUrl")));
@property (readonly) ClientPluginFloat * _Nullable initialLuckyFactor __attribute__((swift_name("initialLuckyFactor")));
@property (readonly) BOOL isBot __attribute__((swift_name("isBot")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) id _Nullable payload __attribute__((swift_name("payload")));
@property (readonly) NSString *playerId __attribute__((swift_name("playerId")));
@property (readonly) ClientPluginDouble * _Nullable rating __attribute__((swift_name("rating")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerializationStrategy")))
@protocol ClientPluginKotlinx_serialization_coreSerializationStrategy
@required
- (void)serializeEncoder:(id<ClientPluginKotlinx_serialization_coreEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<ClientPluginKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreDeserializationStrategy")))
@protocol ClientPluginKotlinx_serialization_coreDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<ClientPluginKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
@property (readonly) id<ClientPluginKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreKSerializer")))
@protocol ClientPluginKotlinx_serialization_coreKSerializer <ClientPluginKotlinx_serialization_coreSerializationStrategy, ClientPluginKotlinx_serialization_coreDeserializationStrategy>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface ClientPluginKotlinByteArray : ClientPluginBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(ClientPluginByte *(^)(ClientPluginInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (ClientPluginKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerialFormat")))
@protocol ClientPluginKotlinx_serialization_coreSerialFormat
@required
@property (readonly) ClientPluginKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreStringFormat")))
@protocol ClientPluginKotlinx_serialization_coreStringFormat <ClientPluginKotlinx_serialization_coreSerialFormat>
@required
- (id _Nullable)decodeFromStringDeserializer:(id<ClientPluginKotlinx_serialization_coreDeserializationStrategy>)deserializer string:(NSString *)string __attribute__((swift_name("decodeFromString(deserializer:string:)")));
- (NSString *)encodeToStringSerializer:(id<ClientPluginKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToString(serializer:value:)")));
@end

__attribute__((swift_name("Kotlinx_serialization_jsonJson")))
@interface ClientPluginKotlinx_serialization_jsonJson : ClientPluginBase <ClientPluginKotlinx_serialization_coreStringFormat>
@property (class, readonly, getter=companion) ClientPluginKotlinx_serialization_jsonJsonDefault *companion __attribute__((swift_name("companion")));
- (id _Nullable)decodeFromJsonElementDeserializer:(id<ClientPluginKotlinx_serialization_coreDeserializationStrategy>)deserializer element:(ClientPluginKotlinx_serialization_jsonJsonElement *)element __attribute__((swift_name("decodeFromJsonElement(deserializer:element:)")));
- (id _Nullable)decodeFromStringString:(NSString *)string __attribute__((swift_name("decodeFromString(string:)")));
- (id _Nullable)decodeFromStringDeserializer:(id<ClientPluginKotlinx_serialization_coreDeserializationStrategy>)deserializer string:(NSString *)string __attribute__((swift_name("decodeFromString(deserializer:string:)")));
- (ClientPluginKotlinx_serialization_jsonJsonElement *)encodeToJsonElementSerializer:(id<ClientPluginKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToJsonElement(serializer:value:)")));
- (NSString *)encodeToStringSerializer:(id<ClientPluginKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToString(serializer:value:)")));
- (ClientPluginKotlinx_serialization_jsonJsonElement *)parseToJsonElementString:(NSString *)string __attribute__((swift_name("parseToJsonElement(string:)")));
@property (readonly) ClientPluginKotlinx_serialization_jsonJsonConfiguration *configuration __attribute__((swift_name("configuration")));
@property (readonly) ClientPluginKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kodein_diDIModule")))
@interface ClientPluginKodein_diDIModule : ClientPluginBase
- (instancetype)initWithAllowSilentOverride:(BOOL)allowSilentOverride prefix:(NSString *)prefix init:(void (^)(id<ClientPluginKodein_diDIBuilder>))init __attribute__((swift_name("init(allowSilentOverride:prefix:init:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithName:(NSString *)name allowSilentOverride:(BOOL)allowSilentOverride prefix:(NSString *)prefix init:(void (^)(id<ClientPluginKodein_diDIBuilder>))init __attribute__((swift_name("init(name:allowSilentOverride:prefix:init:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginKodein_diDIModule *)doCopyAllowSilentOverride:(BOOL)allowSilentOverride prefix:(NSString *)prefix init:(void (^)(id<ClientPluginKodein_diDIBuilder>))init __attribute__((swift_name("doCopy(allowSilentOverride:prefix:init:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (ClientPluginKodein_diDIModule *)getValueThisRef:(id _Nullable)thisRef property:(id<ClientPluginKotlinKProperty>)property __attribute__((swift_name("getValue(thisRef:property:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL allowSilentOverride __attribute__((swift_name("allowSilentOverride")));
@property (readonly, getter=doInit) void (^init)(id<ClientPluginKodein_diDIBuilder>) __attribute__((swift_name("init")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *prefix __attribute__((swift_name("prefix")));
@end

__attribute__((swift_name("Kodein_diDIAware")))
@protocol ClientPluginKodein_diDIAware
@required
@property (readonly) id<ClientPluginKodein_diDI> di __attribute__((swift_name("di")));
@property (readonly) id<ClientPluginKodein_diDIContext> diContext __attribute__((swift_name("diContext")));
@property (readonly) ClientPluginKodein_diDITrigger * _Nullable diTrigger __attribute__((swift_name("diTrigger")));
@end

__attribute__((swift_name("Kodein_diDI")))
@protocol ClientPluginKodein_diDI <ClientPluginKodein_diDIAware>
@required
@property (readonly) id<ClientPluginKodein_diDIContainer> container __attribute__((swift_name("container")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineGameHistory")))
@interface ClientPluginEngineGameHistory : ClientPluginBase
- (instancetype)initWithGameId:(NSString *)gameId createdAt:(ClientPluginKotlinx_datetimeInstant *)createdAt rounds:(ClientPluginKotlinArray<ClientPluginEngineRoundHistory *> *)rounds gameWinners:(ClientPluginKotlinArray<NSString *> * _Nullable)gameWinners metadata:(ClientPluginEngineGameHistoryMetadata * _Nullable)metadata __attribute__((swift_name("init(gameId:createdAt:rounds:gameWinners:metadata:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineGameHistoryCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineGameHistory *)doCopyGameId:(NSString *)gameId createdAt:(ClientPluginKotlinx_datetimeInstant *)createdAt rounds:(ClientPluginKotlinArray<ClientPluginEngineRoundHistory *> *)rounds gameWinners:(ClientPluginKotlinArray<NSString *> * _Nullable)gameWinners metadata:(ClientPluginEngineGameHistoryMetadata * _Nullable)metadata __attribute__((swift_name("doCopy(gameId:createdAt:rounds:gameWinners:metadata:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginKotlinx_datetimeInstant *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) NSString *date __attribute__((swift_name("date")));
@property (readonly) NSString *gameId __attribute__((swift_name("gameId")));
@property (readonly) ClientPluginKotlinArray<NSString *> * _Nullable gameWinners __attribute__((swift_name("gameWinners")));
@property (readonly) BOOL isFirstRound __attribute__((swift_name("isFirstRound")));
@property (readonly) ClientPluginEngineRoundHistory *lastRoundHistory __attribute__((swift_name("lastRoundHistory")));
@property (readonly) ClientPluginEngineGameHistoryMetadata * _Nullable metadata __attribute__((swift_name("metadata")));
@property (readonly) ClientPluginKotlinArray<ClientPluginEngineRoundHistory *> *rounds __attribute__((swift_name("rounds")));
@property (readonly) int32_t roundsCount __attribute__((swift_name("roundsCount")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientGameType")))
@interface ClientPluginGame_clientGameType : ClientPluginKotlinEnum<ClientPluginGame_clientGameType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginGame_clientGameTypeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginGame_clientGameType *single __attribute__((swift_name("single")));
@property (class, readonly) ClientPluginGame_clientGameType *server __attribute__((swift_name("server")));
@property (class, readonly) ClientPluginGame_clientGameType *p2pParticipant __attribute__((swift_name("p2pParticipant")));
@property (class, readonly) ClientPluginGame_clientGameType *p2pHost __attribute__((swift_name("p2pHost")));
@property (class, readonly) ClientPluginGame_clientGameType *tutorial __attribute__((swift_name("tutorial")));
+ (ClientPluginKotlinArray<ClientPluginGame_clientGameType *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginGame_clientGameType *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineJassTable")))
@interface ClientPluginEngineJassTable : ClientPluginBase
- (instancetype)initWithId:(NSString *)id version:(NSString *)version createdAt:(ClientPluginKotlinx_datetimeInstant *)createdAt config:(ClientPluginEngineConfig_ *)config players:(NSArray<ClientPluginEngineJassPlayer *> *)players spectators:(NSArray<ClientPluginEngineSpectator *> *)spectators sceneInfo:(ClientPluginEngineSceneInfo *)sceneInfo cardsOnTable:(NSArray<ClientPluginEngineCardOnTable *> *)cardsOnTable bribes:(NSArray<ClientPluginEngineBribe *> *)bribes cardDeck:(ClientPluginEngineCardDeck * _Nullable)cardDeck requestedCardDecks:(NSArray<ClientPluginEngineCardDeck *> *)requestedCardDecks gameInfo:(ClientPluginEngineGameInfo *)gameInfo gameHistory:(ClientPluginEngineGameHistory *)gameHistory __attribute__((swift_name("init(id:version:createdAt:config:players:spectators:sceneInfo:cardsOnTable:bribes:cardDeck:requestedCardDecks:gameInfo:gameHistory:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineJassTableCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineJassTable *)doCopyId:(NSString *)id version:(NSString *)version createdAt:(ClientPluginKotlinx_datetimeInstant *)createdAt config:(ClientPluginEngineConfig_ *)config players:(NSArray<ClientPluginEngineJassPlayer *> *)players spectators:(NSArray<ClientPluginEngineSpectator *> *)spectators sceneInfo:(ClientPluginEngineSceneInfo *)sceneInfo cardsOnTable:(NSArray<ClientPluginEngineCardOnTable *> *)cardsOnTable bribes:(NSArray<ClientPluginEngineBribe *> *)bribes cardDeck:(ClientPluginEngineCardDeck * _Nullable)cardDeck requestedCardDecks:(NSArray<ClientPluginEngineCardDeck *> *)requestedCardDecks gameInfo:(ClientPluginEngineGameInfo *)gameInfo gameHistory:(ClientPluginEngineGameHistory *)gameHistory __attribute__((swift_name("doCopy(id:version:createdAt:config:players:spectators:sceneInfo:cardsOnTable:bribes:cardDeck:requestedCardDecks:gameInfo:gameHistory:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginEngineBribe *> *bribes __attribute__((swift_name("bribes")));
@property (readonly) ClientPluginEngineCardDeck * _Nullable cardDeck __attribute__((swift_name("cardDeck")));
@property (readonly) NSArray<ClientPluginEngineCardOnTable *> *cardsOnTable __attribute__((swift_name("cardsOnTable")));
@property (readonly) ClientPluginEngineConfig_ *config __attribute__((swift_name("config")));
@property (readonly) ClientPluginKotlinx_datetimeInstant *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) ClientPluginCoreSuit * _Nullable currentTrump __attribute__((swift_name("currentTrump")));
@property (readonly) ClientPluginKotlinx_datetimeInstant *gameCreatedAt __attribute__((swift_name("gameCreatedAt")));
@property (readonly) int64_t gameDuration __attribute__((swift_name("gameDuration")));
@property (readonly) ClientPluginEngineGameHistory *gameHistory __attribute__((swift_name("gameHistory")));
@property (readonly) NSString *gameId __attribute__((swift_name("gameId")));
@property (readonly) ClientPluginEngineGameInfo *gameInfo __attribute__((swift_name("gameInfo")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) BOOL isCardDeckExist __attribute__((swift_name("isCardDeckExist")));
@property (readonly) BOOL isGameFinished __attribute__((swift_name("isGameFinished")));
@property (readonly) NSArray<ClientPluginEngineJassPlayer *> *players __attribute__((swift_name("players")));
@property (readonly) NSArray<ClientPluginKotlinPair<NSString *, ClientPluginCorePlayerConnectionState *> *> *playersConnections __attribute__((swift_name("playersConnections")));
@property (readonly) NSArray<ClientPluginEngineCardDeck *> *requestedCardDecks __attribute__((swift_name("requestedCardDecks")));
@property (readonly) ClientPluginEngineCardDeck *requireCardDeck __attribute__((swift_name("requireCardDeck")));
@property (readonly) ClientPluginEngineSceneInfo *sceneInfo __attribute__((swift_name("sceneInfo")));
@property (readonly) NSArray<ClientPluginEngineSpectator *> *spectators __attribute__((swift_name("spectators")));
@property (readonly) ClientPluginKotlinArray<ClientPluginCoreGameUserInfo *> *users __attribute__((swift_name("users")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end

__attribute__((swift_name("EngineAppStatePayload")))
@protocol ClientPluginEngineAppStatePayload
@required
@end

__attribute__((swift_name("Game_clientBufferStateContract")))
@protocol ClientPluginGame_clientBufferStateContract
@required
@property (readonly) NSArray<id<ClientPluginCoreBufferedAction>> *bufferedActions __attribute__((swift_name("bufferedActions")));
@end

__attribute__((swift_name("Game_clientExpectantsStateContract")))
@protocol ClientPluginGame_clientExpectantsStateContract
@required
@property (readonly) NSArray<ClientPluginGame_clientExpectantWrapper<id> *> *expectants __attribute__((swift_name("expectants")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientClientStatePayload")))
@interface ClientPluginGame_clientClientStatePayload : ClientPluginBase <ClientPluginEngineAppStatePayload, ClientPluginGame_clientBufferStateContract, ClientPluginGame_clientExpectantsStateContract>
- (instancetype)initWithConfig:(ClientPluginGame_clientClientConfig *)config messages:(NSArray<id<ClientPluginEngineMessage>> *)messages infoMessage:(id<ClientPluginEngineMessage> _Nullable)infoMessage expectants:(NSArray<ClientPluginGame_clientExpectantWrapper<id<ClientPluginCoreExpectantAction>> *> *)expectants bufferedActions:(NSArray<id<ClientPluginCoreBufferedAction>> *)bufferedActions isForeground:(BOOL)isForeground isSceneActive:(BOOL)isSceneActive chatOpened:(BOOL)chatOpened settingsOpened:(BOOL)settingsOpened combinations:(ClientPluginGame_clientCombinationToAnnounceState * _Nullable)combinations lastBribe:(ClientPluginGame_clientLastBribeState * _Nullable)lastBribe gameInfo:(ClientPluginGame_clientGameInfoState * _Nullable)gameInfo processingCard:(ClientPluginCoreProcessingCard * _Nullable)processingCard error:(ClientPluginEngineErrorState * _Nullable)error tutorial:(NSString * _Nullable)tutorial __attribute__((swift_name("init(config:messages:infoMessage:expectants:bufferedActions:isForeground:isSceneActive:chatOpened:settingsOpened:combinations:lastBribe:gameInfo:processingCard:error:tutorial:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginGame_clientClientStatePayloadCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginGame_clientClientStatePayload *)doCopyConfig:(ClientPluginGame_clientClientConfig *)config messages:(NSArray<id<ClientPluginEngineMessage>> *)messages infoMessage:(id<ClientPluginEngineMessage> _Nullable)infoMessage expectants:(NSArray<ClientPluginGame_clientExpectantWrapper<id<ClientPluginCoreExpectantAction>> *> *)expectants bufferedActions:(NSArray<id<ClientPluginCoreBufferedAction>> *)bufferedActions isForeground:(BOOL)isForeground isSceneActive:(BOOL)isSceneActive chatOpened:(BOOL)chatOpened settingsOpened:(BOOL)settingsOpened combinations:(ClientPluginGame_clientCombinationToAnnounceState * _Nullable)combinations lastBribe:(ClientPluginGame_clientLastBribeState * _Nullable)lastBribe gameInfo:(ClientPluginGame_clientGameInfoState * _Nullable)gameInfo processingCard:(ClientPluginCoreProcessingCard * _Nullable)processingCard error:(ClientPluginEngineErrorState * _Nullable)error tutorial:(NSString * _Nullable)tutorial __attribute__((swift_name("doCopy(config:messages:infoMessage:expectants:bufferedActions:isForeground:isSceneActive:chatOpened:settingsOpened:combinations:lastBribe:gameInfo:processingCard:error:tutorial:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<id<ClientPluginCoreBufferedAction>> *bufferedActions __attribute__((swift_name("bufferedActions")));
@property (readonly) BOOL chatOpened __attribute__((swift_name("chatOpened")));
@property (readonly) ClientPluginGame_clientCombinationToAnnounceState * _Nullable combinations __attribute__((swift_name("combinations")));
@property (readonly) ClientPluginGame_clientClientConfig *config __attribute__((swift_name("config")));
@property (readonly) ClientPluginEngineErrorState * _Nullable error __attribute__((swift_name("error")));
@property (readonly) NSArray<ClientPluginGame_clientExpectantWrapper<id<ClientPluginCoreExpectantAction>> *> *expectants __attribute__((swift_name("expectants")));
@property (readonly) ClientPluginGame_clientGameInfoState * _Nullable gameInfo __attribute__((swift_name("gameInfo")));
@property (readonly) id<ClientPluginEngineMessage> _Nullable infoMessage __attribute__((swift_name("infoMessage")));
@property (readonly) BOOL isCardsDistribution __attribute__((swift_name("isCardsDistribution")));
@property (readonly) BOOL isForeground __attribute__((swift_name("isForeground")));
@property (readonly) BOOL isSceneActive __attribute__((swift_name("isSceneActive")));
@property (readonly) ClientPluginGame_clientLastBribeState * _Nullable lastBribe __attribute__((swift_name("lastBribe")));
@property (readonly) NSArray<id<ClientPluginEngineMessage>> *messages __attribute__((swift_name("messages")));
@property (readonly) ClientPluginCoreProcessingCard * _Nullable processingCard __attribute__((swift_name("processingCard")));
@property (readonly) BOOL settingsOpened __attribute__((swift_name("settingsOpened")));
@property (readonly) NSString * _Nullable tutorial __attribute__((swift_name("tutorial")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineUserRatingInteractor")))
@interface ClientPluginEngineUserRatingInteractor : ClientPluginBase
- (instancetype)initWithRatingCalculator:(id<ClientPluginEngineRatingCalculator>)ratingCalculator __attribute__((swift_name("init(ratingCalculator:)"))) __attribute__((objc_designated_initializer));
- (NSArray<ClientPluginEnginePlayerRatingOutput *> *)calculateNewUsersRatingRatingList:(NSArray<ClientPluginEnginePlayerRatingInput *> *)ratingList playersMode:(ClientPluginEnginePlayersMode *)playersMode pointsMode:(ClientPluginEnginePointsMode *)pointsMode __attribute__((swift_name("calculateNewUsersRating(ratingList:playersMode:pointsMode:)")));
- (ClientPluginEngineLeaguesConfig *)getLeaguesConfig __attribute__((swift_name("getLeaguesConfig()")));
- (BOOL)isValidRatingForLeagueLeague:(ClientPluginEngineLeague *)league rating:(double)rating __attribute__((swift_name("isValidRatingForLeague(league:rating:)")));
- (ClientPluginEngineLeague *)mapRatingToLeagueRating:(double)rating __attribute__((swift_name("mapRatingToLeague(rating:)")));
@end

__attribute__((swift_name("Game_clientPlayersSceneContract")))
@protocol ClientPluginGame_clientPlayersSceneContract
@required
- (NSArray<ClientPluginGame_clientPlayerViewModel *> *)getPlayers __attribute__((swift_name("getPlayers()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerConnectionStateByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerConnectionStateById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerMessageByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerMessageById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerTurnByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerTurnById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerTurns __attribute__((swift_name("observePlayerTurns()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayers __attribute__((swift_name("observePlayers()")));
@end

__attribute__((swift_name("EngineGameStoreContract")))
@protocol ClientPluginEngineGameStoreContract
@required
@property (readonly) ClientPluginEngineGameLifecycleState *gameLifecycleState __attribute__((swift_name("gameLifecycleState")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) BOOL isTableExists __attribute__((swift_name("isTableExists")));
@property (readonly) ClientPluginEngineJassTable *table __attribute__((swift_name("table")));
@end

__attribute__((swift_name("Game_clientGameMechanic")))
@protocol ClientPluginGame_clientGameMechanic <ClientPluginGame_clientPlayersSceneContract, ClientPluginEngineGameStoreContract>
@required
- (void)clientMessageMessage:(id<ClientPluginEngineMessage>)message __attribute__((swift_name("clientMessage(message:)")));
- (void)errorError_:(ClientPluginEngineErrorState * _Nullable)error __attribute__((swift_name("error(error_:)")));
- (void)foregroundForeground:(BOOL)foreground __attribute__((swift_name("foreground(foreground:)")));
- (ClientPluginGame_clientClientConfig *)getClientConfig __attribute__((swift_name("getClientConfig()")));
- (ClientPluginEngineConfig_ *)getConfig __attribute__((swift_name("getConfig()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeChatOpened __attribute__((swift_name("observeChatOpened()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeClientConfig __attribute__((swift_name("observeClientConfig()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeConfig __attribute__((swift_name("observeConfig()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeError __attribute__((swift_name("observeError()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeGameHistoryEvents __attribute__((swift_name("observeGameHistoryEvents()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeGameInfo __attribute__((swift_name("observeGameInfo()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeGameLifecycle __attribute__((swift_name("observeGameLifecycle()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeGameType __attribute__((swift_name("observeGameType()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeInfoMessage __attribute__((swift_name("observeInfoMessage()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeLastBribe __attribute__((swift_name("observeLastBribe()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayersHandsMetadata __attribute__((swift_name("observePlayersHandsMetadata()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeRestartGameExpectant __attribute__((swift_name("observeRestartGameExpectant()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeScene __attribute__((swift_name("observeScene()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeSceneId __attribute__((swift_name("observeSceneId()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeSettingsOpened __attribute__((swift_name("observeSettingsOpened()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeShowTutorial __attribute__((swift_name("observeShowTutorial()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeUsers __attribute__((swift_name("observeUsers()")));
- (void)onCreate __attribute__((swift_name("onCreate()")));
- (void)onDestroy __attribute__((swift_name("onDestroy()")));
- (void)playerExit __attribute__((swift_name("playerExit()")));
- (void)switchChatOpen:(BOOL)open __attribute__((swift_name("switchChat(open:)")));
- (void)switchGameInfoOpen:(BOOL)open __attribute__((swift_name("switchGameInfo(open:)")));
- (void)switchLastBribeOpen:(BOOL)open __attribute__((swift_name("switchLastBribe(open:)")));
- (void)switchSettingsOpen:(BOOL)open __attribute__((swift_name("switchSettings(open:)")));
- (void)tutorialPassedSkipped:(BOOL)skipped __attribute__((swift_name("tutorialPassed(skipped:)")));
- (void)updateConfigUpdateConfig:(ClientPluginGame_clientUpdateConfigModel *)updateConfig __attribute__((swift_name("updateConfig(updateConfig:)")));
- (void)waitingForConnection __attribute__((swift_name("waitingForConnection()")));
@end

__attribute__((swift_name("Game_clientClientEncoderDecoderProvider")))
@protocol ClientPluginGame_clientClientEncoderDecoderProvider
@required
- (ClientPluginGame_clientClientAppSavedState *)decodeClientAppStateJson:(NSString *)json __attribute__((swift_name("decodeClientAppState(json:)")));
- (NSString *)encodeClientAppStateClientAppSavedState:(ClientPluginGame_clientClientAppSavedState *)clientAppSavedState __attribute__((swift_name("encodeClientAppState(clientAppSavedState:)")));
@end

__attribute__((swift_name("Game_clientSceneActionsContract")))
@protocol ClientPluginGame_clientSceneActionsContract
@required
- (void)setSceneActiveStateIsActive:(BOOL)isActive __attribute__((swift_name("setSceneActiveState(isActive:)")));
@end

__attribute__((swift_name("Game_clientCalcPointsSceneContract")))
@protocol ClientPluginGame_clientCalcPointsSceneContract
@required
- (NSArray<NSString *> * _Nullable)getGameWinner __attribute__((swift_name("getGameWinner()")));
- (ClientPluginGame_clientPointsContentViewModel *)getPoints __attribute__((swift_name("getPoints()")));
- (NSArray<NSString *> *)getRoundWinner __attribute__((swift_name("getRoundWinner()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeControlsModeCalcPoints __attribute__((swift_name("observeControlsModeCalcPoints()")));
- (void)ready __attribute__((swift_name("ready()")));
@end

__attribute__((swift_name("Game_clientCalcPointsScene")))
@protocol ClientPluginGame_clientCalcPointsScene <ClientPluginGame_clientSceneActionsContract, ClientPluginGame_clientCalcPointsSceneContract>
@required
@end

__attribute__((swift_name("Game_clientChatSceneContract")))
@protocol ClientPluginGame_clientChatSceneContract
@required
- (NSArray<id<ClientPluginEngineMessage>> *)getMessages __attribute__((swift_name("getMessages()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeEnabledChat __attribute__((swift_name("observeEnabledChat()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeLeftVoiceCount __attribute__((swift_name("observeLeftVoiceCount()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeMessagesStrategy:(ClientPluginGame_clientChatSceneContractObserverStrategy *)strategy __attribute__((swift_name("observeMessages(strategy:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeNewMessage __attribute__((swift_name("observeNewMessage()")));
- (void)onMessageSent __attribute__((swift_name("onMessageSent()")));
- (void)sendMessageMessage:(NSString *)message __attribute__((swift_name("sendMessage(message:)")));
- (void)sendPhrasePhrase:(NSString *)phrase __attribute__((swift_name("sendPhrase(phrase:)")));
- (void)sendStickerUrl:(NSString *)url __attribute__((swift_name("sendSticker(url:)")));
@end

__attribute__((swift_name("Game_clientChatScene")))
@protocol ClientPluginGame_clientChatScene <ClientPluginGame_clientChatSceneContract>
@required
@end

__attribute__((swift_name("Game_clientExpectantContract")))
@protocol ClientPluginGame_clientExpectantContract
@required
- (void)expectantStageCompletedId:(NSString *)id __attribute__((swift_name("expectantStageCompleted(id:)")));
- (void)expectantTimerFinishedId:(NSString *)id __attribute__((swift_name("expectantTimerFinished(id:)")));
@end

__attribute__((swift_name("Game_clientChoosePartnerSceneContract")))
@protocol ClientPluginGame_clientChoosePartnerSceneContract
@required
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeControlsModeTeams __attribute__((swift_name("observeControlsModeTeams()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeTeamDataExpectant __attribute__((swift_name("observeTeamDataExpectant()")));
- (void)partnerChoicePlayerId:(NSString *)playerId __attribute__((swift_name("partnerChoice(playerId:)")));
@end

__attribute__((swift_name("Game_clientChoosePartnerScene")))
@protocol ClientPluginGame_clientChoosePartnerScene <ClientPluginGame_clientSceneActionsContract, ClientPluginGame_clientExpectantContract, ClientPluginGame_clientChoosePartnerSceneContract>
@required
@end

__attribute__((swift_name("Game_clientEarnPointsSceneContract")))
@protocol ClientPluginGame_clientEarnPointsSceneContract
@required
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeBribeExpectant __attribute__((swift_name("observeBribeExpectant()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeCardOnTheTableByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observeCardOnTheTableById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeCardsOnTheTable __attribute__((swift_name("observeCardsOnTheTable()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeValidationError __attribute__((swift_name("observeValidationError()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeWinnerCombinationsExpectant __attribute__((swift_name("observeWinnerCombinationsExpectant()")));
- (void)onCardPutCard:(ClientPluginCoreProcessingCard *)card __attribute__((swift_name("onCardPut(card:)")));
- (void)onClickDisabledCardCard:(ClientPluginCoreGameCard *)card __attribute__((swift_name("onClickDisabledCard(card:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreProcessingCard")))
@interface ClientPluginCoreProcessingCard : ClientPluginBase
- (instancetype)initWithGameCard:(ClientPluginCoreGameCard *)gameCard payload:(ClientPluginCoreCardPayload * _Nullable)payload __attribute__((swift_name("init(gameCard:payload:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginCoreProcessingCard *)doCopyGameCard:(ClientPluginCoreGameCard *)gameCard payload:(ClientPluginCoreCardPayload * _Nullable)payload __attribute__((swift_name("doCopy(gameCard:payload:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginCoreGameCard *gameCard __attribute__((swift_name("gameCard")));
@property (readonly) ClientPluginCoreCardPayload * _Nullable payload __attribute__((swift_name("payload")));
@end

__attribute__((swift_name("Game_clientControlsContract")))
@protocol ClientPluginGame_clientControlsContract
@required
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeControlsEnabled __attribute__((swift_name("observeControlsEnabled()")));
@end

__attribute__((swift_name("Game_clientTableSceneContract")))
@protocol ClientPluginGame_clientTableSceneContract
@required
- (void)combinationChoiceIsAccepted:(BOOL)isAccepted __attribute__((swift_name("combinationChoice(isAccepted:)")));
- (ClientPluginGame_clientCardDeckViewModel *)getCardDeck __attribute__((swift_name("getCardDeck()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeAskAboutCombinations __attribute__((swift_name("observeAskAboutCombinations()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeCardDeck __attribute__((swift_name("observeCardDeck()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeCardsState __attribute__((swift_name("observeCardsState()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeCombinationsExpectant __attribute__((swift_name("observeCombinationsExpectant()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeLastBribeEnabled __attribute__((swift_name("observeLastBribeEnabled()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerCombinationsByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerCombinationsById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observePlayerHandByIdPlayerId:(NSString *)playerId __attribute__((swift_name("observePlayerHandById(playerId:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeTrump __attribute__((swift_name("observeTrump()")));
@end

__attribute__((swift_name("Game_clientTableSceneCommon")))
@protocol ClientPluginGame_clientTableSceneCommon <ClientPluginGame_clientControlsContract, ClientPluginGame_clientExpectantContract, ClientPluginGame_clientTableSceneContract>
@required
@end

__attribute__((swift_name("Game_clientTableScene")))
@protocol ClientPluginGame_clientTableScene <ClientPluginGame_clientTableSceneCommon, ClientPluginGame_clientSceneActionsContract>
@required
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeControlsMode __attribute__((swift_name("observeControlsMode()")));
@end

__attribute__((swift_name("Game_clientTradeSceneContract")))
@protocol ClientPluginGame_clientTradeSceneContract
@required
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeControlsModeTrade __attribute__((swift_name("observeControlsModeTrade()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeTrumpSelectedExpectant __attribute__((swift_name("observeTrumpSelectedExpectant()")));
- (void)pass __attribute__((swift_name("pass()")));
- (void)playSuit:(ClientPluginCoreSuit * _Nullable)suit __attribute__((swift_name("play(suit:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LoggerCombinedRaspberryLogger")))
@interface ClientPluginLoggerCombinedRaspberryLogger : ClientPluginBase <ClientPluginLoggerRaspberryLogger>
- (instancetype)initWithLogOutputs:(NSMutableArray<id<ClientPluginLoggerLoggerOutput>> *)logOutputs __attribute__((swift_name("init(logOutputs:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginLoggerCombinedRaspberryLoggerCompanion *companion __attribute__((swift_name("companion")));
- (void)addOutputOutput:(id<ClientPluginLoggerLoggerOutput>)output __attribute__((swift_name("addOutput(output:)")));
- (void)dTag:(NSString * _Nullable)tag message:(NSString *)message payload:(id _Nullable)payload __attribute__((swift_name("d(tag:message:payload:)")));
- (void)eTag:(NSString * _Nullable)tag message:(NSString * _Nullable)message exception:(ClientPluginKotlinThrowable * _Nullable)exception payload:(id _Nullable)payload __attribute__((swift_name("e(tag:message:exception:payload:)")));
- (void)iTag:(NSString * _Nullable)tag message:(NSString *)message payload:(id _Nullable)payload __attribute__((swift_name("i(tag:message:payload:)")));
- (void)vTag:(NSString * _Nullable)tag message:(NSString * _Nullable)message exception:(ClientPluginKotlinThrowable * _Nullable)exception payload:(id _Nullable)payload __attribute__((swift_name("v(tag:message:exception:payload:)")));
- (void)wTag:(NSString * _Nullable)tag message:(NSString * _Nullable)message exception:(ClientPluginKotlinThrowable * _Nullable)exception payload:(id _Nullable)payload __attribute__((swift_name("w(tag:message:exception:payload:)")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineScope")))
@protocol ClientPluginKotlinx_coroutines_coreCoroutineScope
@required
@property (readonly) id<ClientPluginKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.3")
*/
__attribute__((swift_name("KotlinCoroutineContext")))
@protocol ClientPluginKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<ClientPluginKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<ClientPluginKotlinCoroutineContextElement> _Nullable)getKey:(id<ClientPluginKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<ClientPluginKotlinCoroutineContext>)minusKeyKey:(id<ClientPluginKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<ClientPluginKotlinCoroutineContext>)plusContext:(id<ClientPluginKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol ClientPluginKotlinCoroutineContextElement <ClientPluginKotlinCoroutineContext>
@required
@property (readonly) id<ClientPluginKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreJob")))
@protocol ClientPluginKotlinx_coroutines_coreJob <ClientPluginKotlinCoroutineContextElement>
@required

/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
- (id<ClientPluginKotlinx_coroutines_coreChildHandle>)attachChildChild:(id<ClientPluginKotlinx_coroutines_coreChildJob>)child __attribute__((swift_name("attachChild(child:)")));
- (void)cancelCause:(ClientPluginKotlinCancellationException * _Nullable)cause __attribute__((swift_name("cancel(cause:)")));

/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
- (ClientPluginKotlinCancellationException *)getCancellationException __attribute__((swift_name("getCancellationException()")));
- (id<ClientPluginKotlinx_coroutines_coreDisposableHandle>)invokeOnCompletionHandler:(void (^)(ClientPluginKotlinThrowable * _Nullable))handler __attribute__((swift_name("invokeOnCompletion(handler:)")));

/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
- (id<ClientPluginKotlinx_coroutines_coreDisposableHandle>)invokeOnCompletionOnCancelling:(BOOL)onCancelling invokeImmediately:(BOOL)invokeImmediately handler:(void (^)(ClientPluginKotlinThrowable * _Nullable))handler __attribute__((swift_name("invokeOnCompletion(onCancelling:invokeImmediately:handler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)joinWithCompletionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("join(completionHandler:)")));
- (id<ClientPluginKotlinx_coroutines_coreJob>)plusOther:(id<ClientPluginKotlinx_coroutines_coreJob>)other __attribute__((swift_name("plus(other:)"))) __attribute__((unavailable("Operator '+' on two Job objects is meaningless. Job is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The job to the right of `+` just replaces the job the left of `+`.")));
- (BOOL)start __attribute__((swift_name("start()")));
@property (readonly) id<ClientPluginKotlinSequence> children __attribute__((swift_name("children")));
@property (readonly) BOOL isActive __attribute__((swift_name("isActive")));
@property (readonly) BOOL isCancelled __attribute__((swift_name("isCancelled")));
@property (readonly) BOOL isCompleted __attribute__((swift_name("isCompleted")));
@property (readonly) id<ClientPluginKotlinx_coroutines_coreSelectClause0> onJoin __attribute__((swift_name("onJoin")));

/**
 * @note annotations
 *   kotlinx.coroutines.ExperimentalCoroutinesApi
*/
@property (readonly) id<ClientPluginKotlinx_coroutines_coreJob> _Nullable parent __attribute__((swift_name("parent")));
@end

__attribute__((swift_name("KotlinFunction")))
@protocol ClientPluginKotlinFunction
@required
@end

__attribute__((swift_name("KotlinSuspendFunction1")))
@protocol ClientPluginKotlinSuspendFunction1 <ClientPluginKotlinFunction>
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinPair")))
@interface ClientPluginKotlinPair<__covariant A, __covariant B> : ClientPluginBase
- (instancetype)initWithFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("init(first:second:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginKotlinPair<A, B> *)doCopyFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("doCopy(first:second:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) A _Nullable first __attribute__((swift_name("first")));
@property (readonly) B _Nullable second __attribute__((swift_name("second")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreLogType")))
@interface ClientPluginCoreLogType : ClientPluginKotlinEnum<ClientPluginCoreLogType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginCoreLogTypeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginCoreLogType *debug __attribute__((swift_name("debug")));
@property (class, readonly) ClientPluginCoreLogType *error __attribute__((swift_name("error")));
@property (class, readonly) ClientPluginCoreLogType *warn __attribute__((swift_name("warn")));
@property (class, readonly) ClientPluginCoreLogType *verbose __attribute__((swift_name("verbose")));
@property (class, readonly) ClientPluginCoreLogType *info __attribute__((swift_name("info")));
+ (ClientPluginKotlinArray<ClientPluginCoreLogType *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginCoreLogType *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((swift_name("CoreClearable")))
@protocol ClientPluginCoreClearable
@required
- (void)clear __attribute__((swift_name("clear()")));
@end

__attribute__((swift_name("EngineStateProvider")))
@protocol ClientPluginEngineStateProvider <ClientPluginCoreClearable>
@required
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeState __attribute__((swift_name("observeState()")));
@property (readonly) ClientPluginEngineGameStore *gameStore __attribute__((swift_name("gameStore")));
@property (readonly) ClientPluginEngineAppState *state __attribute__((swift_name("state")));
@end

__attribute__((swift_name("Game_clientGameClient")))
@protocol ClientPluginGame_clientGameClient <ClientPluginEngineStateProvider>
@required
- (void)dispatchAsyncBlock:(id<ClientPluginKotlinSuspendFunction2>)block __attribute__((swift_name("dispatchAsync(block:)")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeClientStatePayload __attribute__((swift_name("observeClientStatePayload()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeStateIfTableExists __attribute__((swift_name("observeStateIfTableExists()")));
- (id<ClientPluginKotlinx_coroutines_coreFlow>)observeTableState __attribute__((swift_name("observeTableState()")));
@end

__attribute__((swift_name("KotlinIterator")))
@protocol ClientPluginKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreSuit")))
@interface ClientPluginCoreSuit : ClientPluginKotlinEnum<ClientPluginCoreSuit *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginCoreSuitCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginCoreSuit *diamond __attribute__((swift_name("diamond")));
@property (class, readonly) ClientPluginCoreSuit *spade __attribute__((swift_name("spade")));
@property (class, readonly) ClientPluginCoreSuit *heart __attribute__((swift_name("heart")));
@property (class, readonly) ClientPluginCoreSuit *club __attribute__((swift_name("club")));
+ (ClientPluginKotlinArray<ClientPluginCoreSuit *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginCoreSuit *> *entries __attribute__((swift_name("entries")));
- (NSString *)description __attribute__((swift_name("description()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreCoreConfig.Companion")))
@interface ClientPluginCoreCoreConfigCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginCoreCoreConfigCompanion *shared __attribute__((swift_name("shared")));
- (NSString *)formatterSegment:(NSString *)segment gameId:(NSString *)gameId roomId:(NSString *)roomId userId:(NSString *)userId __attribute__((swift_name("formatter(segment:gameId:roomId:userId:)")));
- (ClientPluginCoreCoreConfig *)getClientEngineTestModeFileLogsEnabled:(BOOL)fileLogsEnabled logsDirectoryPath:(NSString *)logsDirectoryPath fileNameFormat:(NSString *)fileNameFormat __attribute__((swift_name("getClientEngineTestMode(fileLogsEnabled:logsDirectoryPath:fileNameFormat:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineEngineLogicConfig.Companion")))
@interface ClientPluginEngineEngineLogicConfigCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineEngineLogicConfigCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineEngineLogicConfig *)getClientEngineTestMode __attribute__((swift_name("getClientEngineTestMode()")));
- (ClientPluginEngineEngineLogicConfig *)getServerEngineModeRoomMode:(NSString *)roomMode isPrivate:(BOOL)isPrivate transition:(id<ClientPluginEngineGameTransition> _Nullable)transition fromRound:(ClientPluginInt * _Nullable)fromRound isProduction:(BOOL)isProduction validateFromClientActionTags:(BOOL)validateFromClientActionTags openRoomIfFinished:(BOOL)openRoomIfFinished playerTimeoutFactorToFinishStep:(double)playerTimeoutFactorToFinishStep __attribute__((swift_name("getServerEngineMode(roomMode:isPrivate:transition:fromRound:isProduction:validateFromClientActionTags:openRoomIfFinished:playerTimeoutFactorToFinishStep:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineDealerStrategy")))
@interface ClientPluginEngineDealerStrategy : ClientPluginKotlinEnum<ClientPluginEngineDealerStrategy *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginEngineDealerStrategy *standard __attribute__((swift_name("standard")));
@property (class, readonly) ClientPluginEngineDealerStrategy *requestHostStandard __attribute__((swift_name("requestHostStandard")));
@property (class, readonly) ClientPluginEngineDealerStrategy *randomOrg __attribute__((swift_name("randomOrg")));
@property (class, readonly) ClientPluginEngineDealerStrategy *randomOrgSigned __attribute__((swift_name("randomOrgSigned")));
@property (class, readonly) ClientPluginEngineDealerStrategy *fake __attribute__((swift_name("fake")));
@property (class, readonly) ClientPluginEngineDealerStrategy *basedOnPrevious __attribute__((swift_name("basedOnPrevious")));
@property (class, readonly) ClientPluginEngineDealerStrategy *offlineShuffleSimulation __attribute__((swift_name("offlineShuffleSimulation")));
@property (class, readonly) ClientPluginEngineDealerStrategy *equalHandByPoints __attribute__((swift_name("equalHandByPoints")));
@property (class, readonly) ClientPluginEngineDealerStrategy *equalHandByValue __attribute__((swift_name("equalHandByValue")));
+ (ClientPluginKotlinArray<ClientPluginEngineDealerStrategy *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineDealerStrategy *> *entries __attribute__((swift_name("entries")));
- (BOOL)isRandomOrg __attribute__((swift_name("isRandomOrg()")));
- (BOOL)shouldEqualiseHand __attribute__((swift_name("shouldEqualiseHand()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineDealerConfig.Companion")))
@interface ClientPluginEngineDealerConfigCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineDealerConfigCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineDealerConfig *)fakeCards:(ClientPluginKotlinArray<ClientPluginInt *> *)cards __attribute__((swift_name("fake(cards:)")));
- (ClientPluginEngineDealerConfig *)getFromServerConfigDealerStrategy:(NSString *)dealerStrategy cardsJson:(NSString * _Nullable)cardsJson __attribute__((swift_name("getFromServerConfig(dealerStrategy:cardsJson:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientGameClientConfig.Companion")))
@interface ClientPluginGame_clientGameClientConfigCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginGame_clientGameClientConfigCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginGame_clientGameClientConfig *)getClientEngineTestMode __attribute__((swift_name("getClientEngineTestMode()")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreFlowCollector")))
@protocol ClientPluginKotlinx_coroutines_coreFlowCollector
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)emitValue:(id _Nullable)value completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("emit(value:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRules")))
@interface ClientPluginEngineRules : ClientPluginBase
- (instancetype)initWithPlayerWhoChooseSuitGoFirst:(BOOL)playerWhoChooseSuitGoFirst winnerShuffleCards:(BOOL)winnerShuffleCards playWithoutLiabilities:(BOOL)playWithoutLiabilities trumpCardGoToPlayerWhoShuffleCards:(BOOL)trumpCardGoToPlayerWhoShuffleCards dealerInitialCardsCount:(int32_t)dealerInitialCardsCount dealerFinalCardsCount:(int32_t)dealerFinalCardsCount dealerCounterClockwise:(BOOL)dealerCounterClockwise contractTypes:(ClientPluginKotlinArray<ClientPluginEngineRoundContractType *> *)contractTypes bidTypes:(ClientPluginKotlinArray<ClientPluginEngineBidType *> *)bidTypes needToPutHigherTrump:(BOOL)needToPutHigherTrump trumpCardStepPartnerMode:(ClientPluginEngineTrumpCardStepPartnerMode *)trumpCardStepPartnerMode combinationsWithFirstCard:(ClientPluginKotlinArray<ClientPluginEngineCombinationType *> *)combinationsWithFirstCard protectBella:(BOOL)protectBella oneTryToProtectBella:(BOOL)oneTryToProtectBella enableFourSevensCombination:(BOOL)enableFourSevensCombination enableTrumpSevenCombination:(BOOL)enableTrumpSevenCombination enableTrumpSevenCombinationAfterDistribution:(BOOL)enableTrumpSevenCombinationAfterDistribution checkTrumpCombination:(BOOL)checkTrumpCombination checkOnlyTrumpDebertz:(BOOL)checkOnlyTrumpDebertz distributePoints:(BOOL)distributePoints pointsDistributeMode:(ClientPluginEnginePointsDistributeMode *)pointsDistributeMode enableFineAfterThirdFailedContract:(BOOL)enableFineAfterThirdFailedContract fineAfterThirdFailedContract:(int32_t)fineAfterThirdFailedContract enableFineIfNoBribes:(BOOL)enableFineIfNoBribes fineIfNoBribes:(int32_t)fineIfNoBribes __attribute__((swift_name("init(playerWhoChooseSuitGoFirst:winnerShuffleCards:playWithoutLiabilities:trumpCardGoToPlayerWhoShuffleCards:dealerInitialCardsCount:dealerFinalCardsCount:dealerCounterClockwise:contractTypes:bidTypes:needToPutHigherTrump:trumpCardStepPartnerMode:combinationsWithFirstCard:protectBella:oneTryToProtectBella:enableFourSevensCombination:enableTrumpSevenCombination:enableTrumpSevenCombinationAfterDistribution:checkTrumpCombination:checkOnlyTrumpDebertz:distributePoints:pointsDistributeMode:enableFineAfterThirdFailedContract:fineAfterThirdFailedContract:enableFineIfNoBribes:fineIfNoBribes:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineRulesCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineRules *)doCopyPlayerWhoChooseSuitGoFirst:(BOOL)playerWhoChooseSuitGoFirst winnerShuffleCards:(BOOL)winnerShuffleCards playWithoutLiabilities:(BOOL)playWithoutLiabilities trumpCardGoToPlayerWhoShuffleCards:(BOOL)trumpCardGoToPlayerWhoShuffleCards dealerInitialCardsCount:(int32_t)dealerInitialCardsCount dealerFinalCardsCount:(int32_t)dealerFinalCardsCount dealerCounterClockwise:(BOOL)dealerCounterClockwise contractTypes:(ClientPluginKotlinArray<ClientPluginEngineRoundContractType *> *)contractTypes bidTypes:(ClientPluginKotlinArray<ClientPluginEngineBidType *> *)bidTypes needToPutHigherTrump:(BOOL)needToPutHigherTrump trumpCardStepPartnerMode:(ClientPluginEngineTrumpCardStepPartnerMode *)trumpCardStepPartnerMode combinationsWithFirstCard:(ClientPluginKotlinArray<ClientPluginEngineCombinationType *> *)combinationsWithFirstCard protectBella:(BOOL)protectBella oneTryToProtectBella:(BOOL)oneTryToProtectBella enableFourSevensCombination:(BOOL)enableFourSevensCombination enableTrumpSevenCombination:(BOOL)enableTrumpSevenCombination enableTrumpSevenCombinationAfterDistribution:(BOOL)enableTrumpSevenCombinationAfterDistribution checkTrumpCombination:(BOOL)checkTrumpCombination checkOnlyTrumpDebertz:(BOOL)checkOnlyTrumpDebertz distributePoints:(BOOL)distributePoints pointsDistributeMode:(ClientPluginEnginePointsDistributeMode *)pointsDistributeMode enableFineAfterThirdFailedContract:(BOOL)enableFineAfterThirdFailedContract fineAfterThirdFailedContract:(int32_t)fineAfterThirdFailedContract enableFineIfNoBribes:(BOOL)enableFineIfNoBribes fineIfNoBribes:(int32_t)fineIfNoBribes __attribute__((swift_name("doCopy(playerWhoChooseSuitGoFirst:winnerShuffleCards:playWithoutLiabilities:trumpCardGoToPlayerWhoShuffleCards:dealerInitialCardsCount:dealerFinalCardsCount:dealerCounterClockwise:contractTypes:bidTypes:needToPutHigherTrump:trumpCardStepPartnerMode:combinationsWithFirstCard:protectBella:oneTryToProtectBella:enableFourSevensCombination:enableTrumpSevenCombination:enableTrumpSevenCombinationAfterDistribution:checkTrumpCombination:checkOnlyTrumpDebertz:distributePoints:pointsDistributeMode:enableFineAfterThirdFailedContract:fineAfterThirdFailedContract:enableFineIfNoBribes:fineIfNoBribes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginKotlinArray<ClientPluginEngineBidType *> *bidTypes __attribute__((swift_name("bidTypes")));
@property (readonly) BOOL checkOnlyTrumpDebertz __attribute__((swift_name("checkOnlyTrumpDebertz")));
@property (readonly) BOOL checkTrumpCombination __attribute__((swift_name("checkTrumpCombination")));
@property (readonly) ClientPluginKotlinArray<ClientPluginEngineCombinationType *> *combinationsWithFirstCard __attribute__((swift_name("combinationsWithFirstCard")));
@property (readonly) ClientPluginKotlinArray<ClientPluginEngineRoundContractType *> *contractTypes __attribute__((swift_name("contractTypes")));
@property (readonly) BOOL dealerCounterClockwise __attribute__((swift_name("dealerCounterClockwise")));
@property (readonly) int32_t dealerFinalCardsCount __attribute__((swift_name("dealerFinalCardsCount")));
@property (readonly) int32_t dealerInitialCardsCount __attribute__((swift_name("dealerInitialCardsCount")));
@property (readonly) BOOL distributePoints __attribute__((swift_name("distributePoints"))) __attribute__((deprecated("use [pointsDistributeMode]")));
@property (readonly) BOOL enableFineAfterThirdFailedContract __attribute__((swift_name("enableFineAfterThirdFailedContract")));
@property (readonly) BOOL enableFineIfNoBribes __attribute__((swift_name("enableFineIfNoBribes")));
@property (readonly) BOOL enableFourSevensCombination __attribute__((swift_name("enableFourSevensCombination")));
@property (readonly) BOOL enableTrumpSevenCombination __attribute__((swift_name("enableTrumpSevenCombination")));
@property (readonly) BOOL enableTrumpSevenCombinationAfterDistribution __attribute__((swift_name("enableTrumpSevenCombinationAfterDistribution")));
@property (readonly) int32_t fineAfterThirdFailedContract __attribute__((swift_name("fineAfterThirdFailedContract")));
@property (readonly) int32_t fineIfNoBribes __attribute__((swift_name("fineIfNoBribes")));
@property (readonly) BOOL needToPutHigherTrump __attribute__((swift_name("needToPutHigherTrump")));
@property (readonly) BOOL oneTryToProtectBella __attribute__((swift_name("oneTryToProtectBella")));
@property (readonly) BOOL playWithoutLiabilities __attribute__((swift_name("playWithoutLiabilities")));
@property (readonly) BOOL playerWhoChooseSuitGoFirst __attribute__((swift_name("playerWhoChooseSuitGoFirst")));
@property (readonly) ClientPluginEnginePointsDistributeMode *pointsDistributeMode __attribute__((swift_name("pointsDistributeMode")));
@property (readonly) BOOL protectBella __attribute__((swift_name("protectBella")));
@property (readonly) BOOL trumpCardGoToPlayerWhoShuffleCards __attribute__((swift_name("trumpCardGoToPlayerWhoShuffleCards")));
@property (readonly) ClientPluginEngineTrumpCardStepPartnerMode *trumpCardStepPartnerMode __attribute__((swift_name("trumpCardStepPartnerMode")));
@property (readonly) BOOL winnerShuffleCards __attribute__((swift_name("winnerShuffleCards")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRulesSetType")))
@interface ClientPluginEngineRulesSetType : ClientPluginKotlinEnum<ClientPluginEngineRulesSetType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginEngineRulesSetType *belot __attribute__((swift_name("belot")));
@property (class, readonly) ClientPluginEngineRulesSetType *klabor __attribute__((swift_name("klabor")));
@property (class, readonly) ClientPluginEngineRulesSetType *debertzcommon __attribute__((swift_name("debertzcommon")));
@property (class, readonly) ClientPluginEngineRulesSetType *debertzkharkiv __attribute__((swift_name("debertzkharkiv")));
@property (class, readonly) ClientPluginEngineRulesSetType *debertzsaltivka __attribute__((swift_name("debertzsaltivka")));
@property (class, readonly) ClientPluginEngineRulesSetType *custom __attribute__((swift_name("custom")));
+ (ClientPluginKotlinArray<ClientPluginEngineRulesSetType *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineRulesSetType *> *entries __attribute__((swift_name("entries")));
- (ClientPluginEngineRules *)getRules __attribute__((swift_name("getRules()")));
@property (readonly) BOOL isCustom __attribute__((swift_name("isCustom")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineOptions")))
@interface ClientPluginEngineOptions : ClientPluginBase
- (instancetype)initWithIsEnableChat:(BOOL)isEnableChat isBotsEnabled:(BOOL)isBotsEnabled botIntelligenceLevel:(ClientPluginEngineBotIntelligenceLevel *)botIntelligenceLevel timeForCountdownMillis:(int32_t)timeForCountdownMillis timeForBotMillis:(int32_t)timeForBotMillis isEnableChoosePartnerScreen:(BOOL)isEnableChoosePartnerScreen __attribute__((swift_name("init(isEnableChat:isBotsEnabled:botIntelligenceLevel:timeForCountdownMillis:timeForBotMillis:isEnableChoosePartnerScreen:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineOptionsCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineOptions *)doCopyIsEnableChat:(BOOL)isEnableChat isBotsEnabled:(BOOL)isBotsEnabled botIntelligenceLevel:(ClientPluginEngineBotIntelligenceLevel *)botIntelligenceLevel timeForCountdownMillis:(int32_t)timeForCountdownMillis timeForBotMillis:(int32_t)timeForBotMillis isEnableChoosePartnerScreen:(BOOL)isEnableChoosePartnerScreen __attribute__((swift_name("doCopy(isEnableChat:isBotsEnabled:botIntelligenceLevel:timeForCountdownMillis:timeForBotMillis:isEnableChoosePartnerScreen:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginEngineBotIntelligenceLevel *botIntelligenceLevel __attribute__((swift_name("botIntelligenceLevel")));
@property (readonly) BOOL isBotsEnabled __attribute__((swift_name("isBotsEnabled")));
@property (readonly) BOOL isEnableChat __attribute__((swift_name("isEnableChat")));
@property (readonly) BOOL isEnableChoosePartnerScreen __attribute__((swift_name("isEnableChoosePartnerScreen")));
@property (readonly) int32_t timeForBotMillis __attribute__((swift_name("timeForBotMillis")));
@property (readonly) int32_t timeForCountdownMillis __attribute__((swift_name("timeForCountdownMillis")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayersMode")))
@interface ClientPluginEnginePlayersMode : ClientPluginKotlinEnum<ClientPluginEnginePlayersMode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginEnginePlayersModeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginEnginePlayersMode *twoPlayers __attribute__((swift_name("twoPlayers")));
@property (class, readonly) ClientPluginEnginePlayersMode *threePlayers __attribute__((swift_name("threePlayers")));
@property (class, readonly) ClientPluginEnginePlayersMode *fourPlayers __attribute__((swift_name("fourPlayers")));
@property (class, readonly) ClientPluginEnginePlayersMode *fourPlayersByTeam __attribute__((swift_name("fourPlayersByTeam")));
+ (ClientPluginKotlinArray<ClientPluginEnginePlayersMode *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEnginePlayersMode *> *entries __attribute__((swift_name("entries")));
@property (readonly) int32_t count __attribute__((swift_name("count")));
@property (readonly) BOOL isTeamGame __attribute__((swift_name("isTeamGame")));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePointsMode")))
@interface ClientPluginEnginePointsMode : ClientPluginKotlinEnum<ClientPluginEnginePointsMode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginEnginePointsModeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginEnginePointsMode *small __attribute__((swift_name("small")));
@property (class, readonly) ClientPluginEnginePointsMode *medium __attribute__((swift_name("medium")));
@property (class, readonly) ClientPluginEnginePointsMode *big __attribute__((swift_name("big")));
@property (class, readonly) ClientPluginEnginePointsMode *test __attribute__((swift_name("test")));
+ (ClientPluginKotlinArray<ClientPluginEnginePointsMode *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEnginePointsMode *> *entries __attribute__((swift_name("entries")));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineLeague")))
@interface ClientPluginEngineLeague : ClientPluginKotlinEnum<ClientPluginEngineLeague *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginEngineLeague *beginner __attribute__((swift_name("beginner")));
@property (class, readonly) ClientPluginEngineLeague *rookie __attribute__((swift_name("rookie")));
@property (class, readonly) ClientPluginEngineLeague *advanced __attribute__((swift_name("advanced")));
@property (class, readonly) ClientPluginEngineLeague *master __attribute__((swift_name("master")));
+ (ClientPluginKotlinArray<ClientPluginEngineLeague *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineLeague *> *entries __attribute__((swift_name("entries")));
- (int32_t)getMaxRatingMaxUserRating:(int32_t)maxUserRating __attribute__((swift_name("getMaxRating(maxUserRating:)")));
- (int32_t)getMinRating __attribute__((swift_name("getMinRating()")));
- (ClientPluginEngineLeague * _Nullable)getNextLeague __attribute__((swift_name("getNextLeague()")));
- (ClientPluginEngineLeague * _Nullable)getPreviousLeague __attribute__((swift_name("getPreviousLeague()")));
- (BOOL)isEnabled __attribute__((swift_name("isEnabled()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRoomMode")))
@interface ClientPluginEngineRoomMode : ClientPluginKotlinEnum<ClientPluginEngineRoomMode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginEngineRoomMode *default_ __attribute__((swift_name("default_")));
@property (class, readonly) ClientPluginEngineRoomMode *revenge __attribute__((swift_name("revenge")));
@property (class, readonly) ClientPluginEngineRoomMode *quick __attribute__((swift_name("quick")));
@property (class, readonly) ClientPluginEngineRoomMode *league __attribute__((swift_name("league")));
+ (ClientPluginKotlinArray<ClientPluginEngineRoomMode *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineRoomMode *> *entries __attribute__((swift_name("entries")));
@property (readonly) BOOL hasRatingSupport __attribute__((swift_name("hasRatingSupport")));
@property (readonly) BOOL isEnableChoosePartnerScreen __attribute__((swift_name("isEnableChoosePartnerScreen")));
@property (readonly) BOOL shufflePlayers __attribute__((swift_name("shufflePlayers")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreEncoder")))
@protocol ClientPluginKotlinx_serialization_coreEncoder
@required
- (id<ClientPluginKotlinx_serialization_coreCompositeEncoder>)beginCollectionDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize __attribute__((swift_name("beginCollection(descriptor:collectionSize:)")));
- (id<ClientPluginKotlinx_serialization_coreCompositeEncoder>)beginStructureDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (id<ClientPluginKotlinx_serialization_coreEncoder>)encodeInlineDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("encodeInline(descriptor:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (void)encodeNull __attribute__((swift_name("encodeNull()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (void)encodeNullableSerializableValueSerializer:(id<ClientPluginKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<ClientPluginKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
@property (readonly) ClientPluginKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerialDescriptor")))
@protocol ClientPluginKotlinx_serialization_coreSerialDescriptor
@required

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (NSArray<id<ClientPluginKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) NSArray<id<ClientPluginKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isInline __attribute__((swift_name("isInline")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) ClientPluginKotlinx_serialization_coreSerialKind *kind __attribute__((swift_name("kind")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreDecoder")))
@protocol ClientPluginKotlinx_serialization_coreDecoder
@required
- (id<ClientPluginKotlinx_serialization_coreCompositeDecoder>)beginStructureDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (id<ClientPluginKotlinx_serialization_coreDecoder>)decodeInlineDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeInline(descriptor:)")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (ClientPluginKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<ClientPluginKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<ClientPluginKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
@property (readonly) ClientPluginKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("KotlinByteIterator")))
@interface ClientPluginKotlinByteIterator : ClientPluginBase <ClientPluginKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (ClientPluginByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModule")))
@interface ClientPluginKotlinx_serialization_coreSerializersModule : ClientPluginBase

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (void)dumpToCollector:(id<ClientPluginKotlinx_serialization_coreSerializersModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id<ClientPluginKotlinx_serialization_coreKSerializer> _Nullable)getContextualKClass:(id<ClientPluginKotlinKClass>)kClass typeArgumentsSerializers:(NSArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeArgumentsSerializers __attribute__((swift_name("getContextual(kClass:typeArgumentsSerializers:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id<ClientPluginKotlinx_serialization_coreSerializationStrategy> _Nullable)getPolymorphicBaseClass:(id<ClientPluginKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id<ClientPluginKotlinx_serialization_coreDeserializationStrategy> _Nullable)getPolymorphicBaseClass:(id<ClientPluginKotlinKClass>)baseClass serializedClassName:(NSString * _Nullable)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJson.Default")))
@interface ClientPluginKotlinx_serialization_jsonJsonDefault : ClientPluginKotlinx_serialization_jsonJson
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)default_ __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginKotlinx_serialization_jsonJsonDefault *shared __attribute__((swift_name("shared")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable(with=NormalClass(value=kotlinx/serialization/json/JsonElementSerializer))
*/
__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement")))
@interface ClientPluginKotlinx_serialization_jsonJsonElement : ClientPluginBase
@property (class, readonly, getter=companion) ClientPluginKotlinx_serialization_jsonJsonElementCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJsonConfiguration")))
@interface ClientPluginKotlinx_serialization_jsonJsonConfiguration : ClientPluginBase
- (NSString *)description __attribute__((swift_name("description()")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) BOOL allowComments __attribute__((swift_name("allowComments")));
@property (readonly) BOOL allowSpecialFloatingPointValues __attribute__((swift_name("allowSpecialFloatingPointValues")));
@property (readonly) BOOL allowStructuredMapKeys __attribute__((swift_name("allowStructuredMapKeys")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) BOOL allowTrailingComma __attribute__((swift_name("allowTrailingComma")));
@property (readonly) NSString *classDiscriminator __attribute__((swift_name("classDiscriminator")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property ClientPluginKotlinx_serialization_jsonClassDiscriminatorMode *classDiscriminatorMode __attribute__((swift_name("classDiscriminatorMode")));
@property (readonly) BOOL coerceInputValues __attribute__((swift_name("coerceInputValues")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) BOOL decodeEnumsCaseInsensitive __attribute__((swift_name("decodeEnumsCaseInsensitive")));
@property (readonly) BOOL encodeDefaults __attribute__((swift_name("encodeDefaults")));
@property (readonly) BOOL explicitNulls __attribute__((swift_name("explicitNulls")));
@property (readonly) BOOL ignoreUnknownKeys __attribute__((swift_name("ignoreUnknownKeys")));
@property (readonly) BOOL isLenient __attribute__((swift_name("isLenient")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) id<ClientPluginKotlinx_serialization_jsonJsonNamingStrategy> _Nullable namingStrategy __attribute__((swift_name("namingStrategy")));
@property (readonly) BOOL prettyPrint __attribute__((swift_name("prettyPrint")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
@property (readonly) NSString *prettyPrintIndent __attribute__((swift_name("prettyPrintIndent")));
@property (readonly) BOOL useAlternativeNames __attribute__((swift_name("useAlternativeNames")));
@property (readonly) BOOL useArrayPolymorphism __attribute__((swift_name("useArrayPolymorphism")));
@end

__attribute__((swift_name("Kodein_diDIBindBuilder")))
@protocol ClientPluginKodein_diDIBindBuilder
@required
@property (readonly) id<ClientPluginKaveritTypeToken> contextType __attribute__((swift_name("contextType")));
@property (readonly) BOOL explicitContext __attribute__((swift_name("explicitContext")));
@end

__attribute__((swift_name("Kodein_diDIBindBuilderWithScope")))
@protocol ClientPluginKodein_diDIBindBuilderWithScope <ClientPluginKodein_diDIBindBuilder>
@required
@property (readonly) id<ClientPluginKodein_diScope> scope __attribute__((swift_name("scope")));
@end

__attribute__((swift_name("Kodein_diDIBuilder")))
@protocol ClientPluginKodein_diDIBuilder <ClientPluginKodein_diDIBindBuilder, ClientPluginKodein_diDIBindBuilderWithScope>
@required
- (void)AddBindInSetTag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides binding:(id<ClientPluginKodein_diDIBinding>)binding __attribute__((swift_name("AddBindInSet(tag:overrides:binding:)")));
- (id<ClientPluginKodein_diDIBuilderDirectBinder>)BindTag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides __attribute__((swift_name("Bind(tag:overrides:)"))) __attribute__((deprecated("This is not used, it will be removed")));
- (void)BindTag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides binding:(id<ClientPluginKodein_diDIBinding>)binding __attribute__((swift_name("Bind(tag:overrides:binding:)")));
- (id<ClientPluginKodein_diDIBuilderTypeBinder>)BindType:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides __attribute__((swift_name("Bind(type:tag:overrides:)")));
- (void)BindInArgSetTag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides argType:(id<ClientPluginKaveritTypeToken>)argType type:(id<ClientPluginKaveritTypeToken>)type creator:(void (^)(id<ClientPluginKodein_diDIBuilderArgSetBinder>))creator __attribute__((swift_name("BindInArgSet(tag:overrides:argType:type:creator:)")));
- (void)BindInSetTag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides type:(id<ClientPluginKaveritTypeToken>)type creator:(void (^)(id<ClientPluginKodein_diDIBuilderSetBinder>))creator __attribute__((swift_name("BindInSet(tag:overrides:type:creator:)")));
- (void)BindSetTag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides binding:(id<ClientPluginKodein_diDIBinding>)binding __attribute__((swift_name("BindSet(tag:overrides:binding:)"))) __attribute__((deprecated("Use AddBindInSet instead.")));
- (ClientPluginKodein_diDIBuilderDelegateBinder<id> *)DelegateType:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides __attribute__((swift_name("Delegate(type:tag:overrides:)")));
- (void)InBindArgSetTag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides argType:(id<ClientPluginKaveritTypeToken>)argType type:(id<ClientPluginKaveritTypeToken>)type creator:(void (^)(id<ClientPluginKodein_diDIBuilderArgSetBinder>))creator __attribute__((swift_name("InBindArgSet(tag:overrides:argType:type:creator:)")));
- (void)InBindSetTag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides type:(id<ClientPluginKaveritTypeToken>)type creator:(void (^)(id<ClientPluginKodein_diDIBuilderSetBinder>))creator __attribute__((swift_name("InBindSet(tag:overrides:type:creator:)")));
- (void)RegisterContextTranslatorTranslator:(id<ClientPluginKodein_diContextTranslator>)translator __attribute__((swift_name("RegisterContextTranslator(translator:)")));
- (id<ClientPluginKodein_diDIBuilderConstantBinder>)constantTag:(id)tag overrides:(ClientPluginBoolean * _Nullable)overrides __attribute__((swift_name("constant(tag:overrides:)")));
- (void)importModule:(ClientPluginKodein_diDIModule *)module allowOverride:(BOOL)allowOverride __attribute__((swift_name("import(module:allowOverride:)")));
- (void)importAllModules:(ClientPluginKotlinArray<ClientPluginKodein_diDIModule *> *)modules allowOverride:(BOOL)allowOverride __attribute__((swift_name("importAll(modules:allowOverride:)")));
- (void)importAllModules:(id)modules allowOverride_:(BOOL)allowOverride __attribute__((swift_name("importAll(modules:allowOverride_:)")));
- (void)importOnceModule:(ClientPluginKodein_diDIModule *)module allowOverride:(BOOL)allowOverride __attribute__((swift_name("importOnce(module:allowOverride:)")));
- (void)onReadyCb:(void (^)(id<ClientPluginKodein_diDirectDI>))cb __attribute__((swift_name("onReady(cb:)")));
@property (readonly) id<ClientPluginKodein_diDIContainerBuilder> containerBuilder __attribute__((swift_name("containerBuilder")));
@end

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol ClientPluginKotlinKAnnotatedElement
@required
@end

__attribute__((swift_name("KotlinKCallable")))
@protocol ClientPluginKotlinKCallable <ClientPluginKotlinKAnnotatedElement>
@required
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) id<ClientPluginKotlinKType> returnType __attribute__((swift_name("returnType")));
@end

__attribute__((swift_name("KotlinKProperty")))
@protocol ClientPluginKotlinKProperty <ClientPluginKotlinKCallable>
@required
@end

__attribute__((swift_name("Kodein_diDIContainer")))
@protocol ClientPluginKodein_diDIContainer
@required
- (NSArray<id (^)(id _Nullable)> *)allFactoriesKey:(ClientPluginKodein_diDIKey<id, id, id> *)key context:(id)context overrideLevel:(int32_t)overrideLevel __attribute__((swift_name("allFactories(key:context:overrideLevel:)")));
- (NSArray<id (^)(void)> *)allProvidersKey:(ClientPluginKodein_diDIKey<id, ClientPluginKotlinUnit *, id> *)key context:(id)context overrideLevel:(int32_t)overrideLevel __attribute__((swift_name("allProviders(key:context:overrideLevel:)")));
- (id (^)(id _Nullable))factoryKey:(ClientPluginKodein_diDIKey<id, id, id> *)key context:(id)context overrideLevel:(int32_t)overrideLevel __attribute__((swift_name("factory(key:context:overrideLevel:)")));
- (id (^ _Nullable)(id _Nullable))factoryOrNullKey:(ClientPluginKodein_diDIKey<id, id, id> *)key context:(id)context overrideLevel:(int32_t)overrideLevel __attribute__((swift_name("factoryOrNull(key:context:overrideLevel:)")));
- (id (^)(void))providerKey:(ClientPluginKodein_diDIKey<id, ClientPluginKotlinUnit *, id> *)key context:(id)context overrideLevel:(int32_t)overrideLevel __attribute__((swift_name("provider(key:context:overrideLevel:)")));
- (id (^ _Nullable)(void))providerOrNullKey:(ClientPluginKodein_diDIKey<id, ClientPluginKotlinUnit *, id> *)key context:(id)context overrideLevel:(int32_t)overrideLevel __attribute__((swift_name("providerOrNull(key:context:overrideLevel:)")));
@property (readonly) id<ClientPluginKodein_diDITree> tree __attribute__((swift_name("tree")));
@end

__attribute__((swift_name("Kodein_diDIContext")))
@protocol ClientPluginKodein_diDIContext
@required
@property (readonly) id<ClientPluginKaveritTypeToken> type __attribute__((swift_name("type")));
@property (readonly) id value_ __attribute__((swift_name("value_")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kodein_diDITrigger")))
@interface ClientPluginKodein_diDITrigger : ClientPluginBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)trigger __attribute__((swift_name("trigger()")));
@property (readonly) NSMutableArray<id<ClientPluginKotlinLazy>> *properties __attribute__((swift_name("properties")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable(with=NormalClass(value=kotlinx/datetime/serializers/InstantIso8601Serializer))
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_datetimeInstant")))
@interface ClientPluginKotlinx_datetimeInstant : ClientPluginBase <ClientPluginKotlinComparable>
@property (class, readonly, getter=companion) ClientPluginKotlinx_datetimeInstantCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(ClientPluginKotlinx_datetimeInstant *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (ClientPluginKotlinx_datetimeInstant *)minusDuration:(int64_t)duration __attribute__((swift_name("minus(duration:)")));
- (int64_t)minusOther:(ClientPluginKotlinx_datetimeInstant *)other __attribute__((swift_name("minus(other:)")));
- (ClientPluginKotlinx_datetimeInstant *)plusDuration:(int64_t)duration __attribute__((swift_name("plus(duration:)")));
- (int64_t)toEpochMilliseconds __attribute__((swift_name("toEpochMilliseconds()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t epochSeconds __attribute__((swift_name("epochSeconds")));
@property (readonly) int32_t nanosecondsOfSecond __attribute__((swift_name("nanosecondsOfSecond")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRoundHistory")))
@interface ClientPluginEngineRoundHistory : ClientPluginBase
- (instancetype)initWithRound:(int32_t)round items:(ClientPluginKotlinArray<ClientPluginEngineRoundHistoryItem *> *)items committed:(ClientPluginKotlinArray<NSString *> * _Nullable)committed contractState:(ClientPluginEngineRoundContractState * _Nullable)contractState winners:(ClientPluginKotlinArray<NSString *> * _Nullable)winners createdAt:(ClientPluginKotlinx_datetimeInstant *)createdAt __attribute__((swift_name("init(round:items:committed:contractState:winners:createdAt:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineRoundHistoryCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineRoundHistory *)doCopyRound:(int32_t)round items:(ClientPluginKotlinArray<ClientPluginEngineRoundHistoryItem *> *)items committed:(ClientPluginKotlinArray<NSString *> * _Nullable)committed contractState:(ClientPluginEngineRoundContractState * _Nullable)contractState winners:(ClientPluginKotlinArray<NSString *> * _Nullable)winners createdAt:(ClientPluginKotlinx_datetimeInstant *)createdAt __attribute__((swift_name("doCopy(round:items:committed:contractState:winners:createdAt:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isFailedRoundForPlayerPlayerId:(NSString *)playerId __attribute__((swift_name("isFailedRoundForPlayer(playerId:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (NSString *)toStringShort __attribute__((swift_name("toStringShort()")));
@property (readonly) ClientPluginKotlinArray<NSString *> * _Nullable committed __attribute__((swift_name("committed")));
@property (readonly) ClientPluginEngineRoundContractState * _Nullable contractState __attribute__((swift_name("contractState")));
@property (readonly) NSString * _Nullable contractStateName __attribute__((swift_name("contractStateName")));
@property (readonly) ClientPluginKotlinx_datetimeInstant *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) ClientPluginKotlinArray<ClientPluginEngineRoundHistoryItem *> *items __attribute__((swift_name("items")));
@property (readonly) int32_t round __attribute__((swift_name("round")));
@property (readonly) ClientPluginKotlinArray<NSString *> * _Nullable winners __attribute__((swift_name("winners")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineGameHistoryMetadata")))
@interface ClientPluginEngineGameHistoryMetadata : ClientPluginBase
- (instancetype)initWithRandomData:(ClientPluginEngineRandomData * _Nullable)randomData playersAnalytics:(NSArray<ClientPluginEnginePlayerHistoryAnalytics *> *)playersAnalytics __attribute__((swift_name("init(randomData:playersAnalytics:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineGameHistoryMetadataCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineGameHistoryMetadata *)doCopyRandomData:(ClientPluginEngineRandomData * _Nullable)randomData playersAnalytics:(NSArray<ClientPluginEnginePlayerHistoryAnalytics *> *)playersAnalytics __attribute__((swift_name("doCopy(randomData:playersAnalytics:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginEnginePlayerHistoryAnalytics *> *playersAnalytics __attribute__((swift_name("playersAnalytics")));
@property (readonly) ClientPluginEngineRandomData * _Nullable randomData __attribute__((swift_name("randomData")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineGameHistory.Companion")))
@interface ClientPluginEngineGameHistoryCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineGameHistoryCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineGameHistory *)initialGameId:(NSString *)gameId createdAt:(ClientPluginKotlinx_datetimeInstant *)createdAt __attribute__((swift_name("initial(gameId:createdAt:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientGameType.Companion")))
@interface ClientPluginGame_clientGameTypeCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginGame_clientGameTypeCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((swift_name("CorePlayer")))
@protocol ClientPluginCorePlayer <ClientPluginCorePlayerIdContract>
@required
@property (readonly) int32_t amountOfCards __attribute__((swift_name("amountOfCards")));
@property (readonly) NSArray<ClientPluginCoreGameCard *> *cards __attribute__((swift_name("cards")));
@property (readonly) ClientPluginCorePlayerState *state __attribute__((swift_name("state")));
@property (readonly) ClientPluginCoreGameUserInfo *userInfo __attribute__((swift_name("userInfo")));
@end

__attribute__((swift_name("EngineShuffleCardsContract")))
@protocol ClientPluginEngineShuffleCardsContract <ClientPluginCorePlayerIdContract>
@required
@property (readonly) BOOL isShuffleCards __attribute__((swift_name("isShuffleCards")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineJassPlayer")))
@interface ClientPluginEngineJassPlayer : ClientPluginBase <ClientPluginCorePlayer, ClientPluginEngineShuffleCardsContract>
- (instancetype)initWithUserInfo:(ClientPluginCoreGameUserInfo *)userInfo state:(ClientPluginCorePlayerState *)state cards:(NSArray<ClientPluginCoreGameCard *> *)cards options:(ClientPluginEnginePlayerOptions *)options hand:(ClientPluginEnginePlayerHand *)hand points:(ClientPluginEnginePlayerPoints *)points __attribute__((swift_name("init(userInfo:state:cards:options:hand:points:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineJassPlayerCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineJassPlayer *)doCopyUserInfo:(ClientPluginCoreGameUserInfo *)userInfo state:(ClientPluginCorePlayerState *)state cards:(NSArray<ClientPluginCoreGameCard *> *)cards options:(ClientPluginEnginePlayerOptions *)options hand:(ClientPluginEnginePlayerHand *)hand points:(ClientPluginEnginePlayerPoints *)points __attribute__((swift_name("doCopy(userInfo:state:cards:options:hand:points:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginCoreGameCard *> *cards __attribute__((swift_name("cards")));
@property (readonly) ClientPluginEnginePlayerHand *hand __attribute__((swift_name("hand")));
@property (readonly) BOOL isLive __attribute__((swift_name("isLive")));
@property (readonly) BOOL isLiveOrNotOnline __attribute__((swift_name("isLiveOrNotOnline")));
@property (readonly) BOOL isShuffleCards __attribute__((swift_name("isShuffleCards")));
@property (readonly) ClientPluginEnginePlayerOptions *options __attribute__((swift_name("options")));
@property (readonly) NSString *playerId __attribute__((swift_name("playerId")));
@property (readonly) ClientPluginEnginePlayerPoints *points __attribute__((swift_name("points")));
@property (readonly) ClientPluginCorePlayerState *state __attribute__((swift_name("state")));
@property (readonly) ClientPluginCoreGameUserInfo *userInfo __attribute__((swift_name("userInfo")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineSpectator")))
@interface ClientPluginEngineSpectator : ClientPluginBase <ClientPluginCorePlayerIdContract>
- (instancetype)initWithUserInfo:(ClientPluginCoreGameUserInfo *)userInfo __attribute__((swift_name("init(userInfo:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineSpectator *)doCopyUserInfo:(ClientPluginCoreGameUserInfo *)userInfo __attribute__((swift_name("doCopy(userInfo:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *playerId __attribute__((swift_name("playerId")));
@property (readonly) ClientPluginCoreGameUserInfo *userInfo __attribute__((swift_name("userInfo")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineSceneInfo")))
@interface ClientPluginEngineSceneInfo : ClientPluginBase
- (instancetype)initWithSceneId:(NSString *)sceneId previousSceneId:(NSString * _Nullable)previousSceneId actId:(NSString * _Nullable)actId sceneData:(id<ClientPluginEngineSceneData> _Nullable)sceneData actData:(id<ClientPluginEngineActData> _Nullable)actData __attribute__((swift_name("init(sceneId:previousSceneId:actId:sceneData:actData:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineSceneInfo *)doCopySceneId:(NSString *)sceneId previousSceneId:(NSString * _Nullable)previousSceneId actId:(NSString * _Nullable)actId sceneData:(id<ClientPluginEngineSceneData> _Nullable)sceneData actData:(id<ClientPluginEngineActData> _Nullable)actData __attribute__((swift_name("doCopy(sceneId:previousSceneId:actId:sceneData:actData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<ClientPluginEngineActData> _Nullable actData __attribute__((swift_name("actData")));
@property (readonly) NSString * _Nullable actId __attribute__((swift_name("actId")));
@property (readonly) BOOL isFinalScene __attribute__((swift_name("isFinalScene")));
@property (readonly) BOOL isGameFinished __attribute__((swift_name("isGameFinished")));
@property (readonly) NSString * _Nullable previousSceneId __attribute__((swift_name("previousSceneId")));
@property (readonly) id<ClientPluginEngineSceneData> _Nullable sceneData __attribute__((swift_name("sceneData")));
@property (readonly) NSString *sceneId __attribute__((swift_name("sceneId")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCardOnTable")))
@interface ClientPluginEngineCardOnTable : ClientPluginBase <ClientPluginCorePlayerIdContract>
- (instancetype)initWithPlayerId:(NSString *)playerId position:(int32_t)position card:(ClientPluginCoreGameCard *)card __attribute__((swift_name("init(playerId:position:card:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineCardOnTableCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineCardOnTable *)doCopyPlayerId:(NSString *)playerId position:(int32_t)position card:(ClientPluginCoreGameCard *)card __attribute__((swift_name("doCopy(playerId:position:card:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginCoreGameCard *card __attribute__((swift_name("card")));
@property (readonly) NSString *playerId __attribute__((swift_name("playerId")));
@property (readonly) int32_t position __attribute__((swift_name("position")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineBribe")))
@interface ClientPluginEngineBribe : ClientPluginBase
- (instancetype)initWithCards:(NSArray<ClientPluginEngineCardOnTable *> *)cards __attribute__((swift_name("init(cards:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineBribe *)doCopyCards:(NSArray<ClientPluginEngineCardOnTable *> *)cards __attribute__((swift_name("doCopy(cards:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginEngineCardOnTable *> *cards __attribute__((swift_name("cards")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCardDeck")))
@interface ClientPluginEngineCardDeck : ClientPluginBase
- (instancetype)initWithCards:(NSArray<ClientPluginCoreGameCard *> *)cards trumpCard:(ClientPluginCoreGameCard *)trumpCard frezaCard:(ClientPluginCoreGameCard * _Nullable)frezaCard __attribute__((swift_name("init(cards:trumpCard:frezaCard:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineCardDeckCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineCardDeck *)doCopyCards:(NSArray<ClientPluginCoreGameCard *> *)cards trumpCard:(ClientPluginCoreGameCard *)trumpCard frezaCard:(ClientPluginCoreGameCard * _Nullable)frezaCard __attribute__((swift_name("doCopy(cards:trumpCard:frezaCard:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginCoreGameCard *> *allCards __attribute__((swift_name("allCards")));
@property (readonly) NSArray<ClientPluginCoreGameCard *> *cards __attribute__((swift_name("cards")));
@property (readonly) ClientPluginCoreGameCard * _Nullable frezaCard __attribute__((swift_name("frezaCard")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@property (readonly) ClientPluginCoreGameCard *trumpCard __attribute__((swift_name("trumpCard")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineGameInfo")))
@interface ClientPluginEngineGameInfo : ClientPluginBase
- (instancetype)initWithCreatedAt:(ClientPluginKotlinx_datetimeInstant *)createdAt gameNumber:(int32_t)gameNumber round:(ClientPluginEngineRound *)round roundNumberTotal:(int32_t)roundNumberTotal previousRound:(ClientPluginEnginePreviousRound * _Nullable)previousRound __attribute__((swift_name("init(createdAt:gameNumber:round:roundNumberTotal:previousRound:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineGameInfoCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineGameInfo *)doCopyCreatedAt:(ClientPluginKotlinx_datetimeInstant *)createdAt gameNumber:(int32_t)gameNumber round:(ClientPluginEngineRound *)round roundNumberTotal:(int32_t)roundNumberTotal previousRound:(ClientPluginEnginePreviousRound * _Nullable)previousRound __attribute__((swift_name("doCopy(createdAt:gameNumber:round:roundNumberTotal:previousRound:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginKotlinx_datetimeInstant *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) int64_t duration __attribute__((swift_name("duration")));
@property (readonly) int32_t finishedGamesCount __attribute__((swift_name("finishedGamesCount")));
@property (readonly) int32_t finishedRoundsCountTotal __attribute__((swift_name("finishedRoundsCountTotal")));
@property (readonly) int32_t gameNumber __attribute__((swift_name("gameNumber")));
@property (readonly) int32_t gamesCount __attribute__((swift_name("gamesCount")));
@property (readonly) BOOL isFirstGame __attribute__((swift_name("isFirstGame")));
@property (readonly) ClientPluginEnginePreviousRound * _Nullable previousRound __attribute__((swift_name("previousRound")));
@property (readonly) ClientPluginEngineRound *round __attribute__((swift_name("round")));
@property (readonly) int32_t roundNumberTotal __attribute__((swift_name("roundNumberTotal")));
@property (readonly) int32_t roundsCountTotal __attribute__((swift_name("roundsCountTotal")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineJassTable.Companion")))
@interface ClientPluginEngineJassTableCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineJassTableCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineJassTable *)initialTableId:(NSString *)tableId gameId:(NSString *)gameId version:(NSString *)version config:(ClientPluginEngineConfig_ *)config players:(NSArray<ClientPluginEngineJassPlayer *> *)players spectators:(NSArray<ClientPluginEngineSpectator *> *)spectators __attribute__((swift_name("initial(tableId:gameId:version:config:players:spectators:)")));
- (ClientPluginEngineJassTable *)restartRoundRoomId:(NSString *)roomId gameId:(NSString *)gameId version:(NSString *)version tableLite:(ClientPluginEngineJassTableLite *)tableLite __attribute__((swift_name("restartRound(roomId:gameId:version:tableLite:)")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CorePlayerConnectionState")))
@interface ClientPluginCorePlayerConnectionState : ClientPluginKotlinEnum<ClientPluginCorePlayerConnectionState *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginCorePlayerConnectionStateCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginCorePlayerConnectionState *live __attribute__((swift_name("live")));
@property (class, readonly) ClientPluginCorePlayerConnectionState *timeout __attribute__((swift_name("timeout")));
@property (class, readonly) ClientPluginCorePlayerConnectionState *lostConnection __attribute__((swift_name("lostConnection")));
@property (class, readonly) ClientPluginCorePlayerConnectionState *left __attribute__((swift_name("left")));
@property (class, readonly) ClientPluginCorePlayerConnectionState *disconnecting __attribute__((swift_name("disconnecting")));
@property (class, readonly) ClientPluginCorePlayerConnectionState *deleted __attribute__((swift_name("deleted")));
+ (ClientPluginKotlinArray<ClientPluginCorePlayerConnectionState *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginCorePlayerConnectionState *> *entries __attribute__((swift_name("entries")));
@property (readonly) BOOL isClientDisconnectReason __attribute__((swift_name("isClientDisconnectReason")));
@property (readonly) BOOL isDeleted __attribute__((swift_name("isDeleted")));
@property (readonly) BOOL isDisconnected __attribute__((swift_name("isDisconnected")));
@property (readonly) BOOL isDisconnecting __attribute__((swift_name("isDisconnecting")));
@property (readonly) BOOL isLeft __attribute__((swift_name("isLeft")));
@property (readonly) BOOL isLive __attribute__((swift_name("isLive")));
@property (readonly) BOOL isLostConnection __attribute__((swift_name("isLostConnection")));
@property (readonly) BOOL isNotAlive __attribute__((swift_name("isNotAlive")));
@property (readonly) BOOL isServerDisconnectReason __attribute__((swift_name("isServerDisconnectReason")));
@property (readonly) BOOL isTimeOut __attribute__((swift_name("isTimeOut")));
@end

__attribute__((swift_name("CoreAction")))
@protocol ClientPluginCoreAction
@required
@property (readonly) NSString *actionTag __attribute__((swift_name("actionTag")));
@end

__attribute__((swift_name("CoreBufferedAction")))
@protocol ClientPluginCoreBufferedAction <ClientPluginCoreAction>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientExpectantWrapper")))
@interface ClientPluginGame_clientExpectantWrapper<T> : ClientPluginBase
- (instancetype)initWithId:(NSString *)id action:(T)action isFinished:(BOOL)isFinished __attribute__((swift_name("init(id:action:isFinished:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientExpectantWrapper<T> *)doCopyId:(NSString *)id action:(T)action isFinished:(BOOL)isFinished __attribute__((swift_name("doCopy(id:action:isFinished:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) T action __attribute__((swift_name("action")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) BOOL isFinished __attribute__((swift_name("isFinished")));
@property (readonly) BOOL isNotFinished __attribute__((swift_name("isNotFinished")));
@end

__attribute__((swift_name("EngineMessage")))
@protocol ClientPluginEngineMessage
@required
- (BOOL)isItemSameOther:(id<ClientPluginEngineMessage>)other __attribute__((swift_name("isItemSame(other:)")));
@property (readonly) ClientPluginKotlinx_datetimeInstant *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) BOOL isShowing __attribute__((swift_name("isShowing")));
@property (readonly) ClientPluginEngineMessageMessageLifecycle *lifecycle __attribute__((swift_name("lifecycle")));
@end

__attribute__((swift_name("CoreExpectantAction")))
@protocol ClientPluginCoreExpectantAction <ClientPluginCoreBufferedAction>
@required
- (BOOL)ignoreExpectant __attribute__((swift_name("ignoreExpectant()")));
- (BOOL)manualExpectantHandling __attribute__((swift_name("manualExpectantHandling()")));
- (BOOL)waitForExpectantAnimation __attribute__((swift_name("waitForExpectantAnimation()")));
@property (readonly) BOOL isAutoStartTimer __attribute__((swift_name("isAutoStartTimer")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientCombinationToAnnounceState")))
@interface ClientPluginGame_clientCombinationToAnnounceState : ClientPluginBase
- (instancetype)initWithCombinations:(NSArray<ClientPluginEngineCombination *> *)combinations acceptedCombinations:(NSArray<ClientPluginEngineCombination *> * _Nullable)acceptedCombinations declinedCombinations:(NSArray<ClientPluginEngineCombination *> * _Nullable)declinedCombinations __attribute__((swift_name("init(combinations:acceptedCombinations:declinedCombinations:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientCombinationToAnnounceState *)doCopyCombinations:(NSArray<ClientPluginEngineCombination *> *)combinations acceptedCombinations:(NSArray<ClientPluginEngineCombination *> * _Nullable)acceptedCombinations declinedCombinations:(NSArray<ClientPluginEngineCombination *> * _Nullable)declinedCombinations __attribute__((swift_name("doCopy(combinations:acceptedCombinations:declinedCombinations:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginEngineCombination *> * _Nullable acceptedCombinations __attribute__((swift_name("acceptedCombinations")));
@property (readonly) NSArray<ClientPluginEngineCombination *> *combinations __attribute__((swift_name("combinations")));
@property (readonly) NSArray<ClientPluginEngineCombination *> * _Nullable declinedCombinations __attribute__((swift_name("declinedCombinations")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientLastBribeState")))
@interface ClientPluginGame_clientLastBribeState : ClientPluginBase
- (instancetype)initWithWhoPutFirstCardPlayerId:(NSString *)whoPutFirstCardPlayerId whoGotBribePlayerId:(NSString *)whoGotBribePlayerId cards:(NSArray<ClientPluginEngineCardOnTable *> *)cards __attribute__((swift_name("init(whoPutFirstCardPlayerId:whoGotBribePlayerId:cards:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientLastBribeState *)doCopyWhoPutFirstCardPlayerId:(NSString *)whoPutFirstCardPlayerId whoGotBribePlayerId:(NSString *)whoGotBribePlayerId cards:(NSArray<ClientPluginEngineCardOnTable *> *)cards __attribute__((swift_name("doCopy(whoPutFirstCardPlayerId:whoGotBribePlayerId:cards:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginEngineCardOnTable *> *cards __attribute__((swift_name("cards")));
@property (readonly) NSString *whoGotBribePlayerId __attribute__((swift_name("whoGotBribePlayerId")));
@property (readonly) NSString *whoPutFirstCardPlayerId __attribute__((swift_name("whoPutFirstCardPlayerId")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientGameInfoState")))
@interface ClientPluginGame_clientGameInfoState : ClientPluginBase
- (instancetype)initWithTrump:(ClientPluginCoreSuit * _Nullable)trump whoShuffleCardsPlayerId:(NSString * _Nullable)whoShuffleCardsPlayerId whoChoseSuitPlayerId:(NSString * _Nullable)whoChoseSuitPlayerId __attribute__((swift_name("init(trump:whoShuffleCardsPlayerId:whoChoseSuitPlayerId:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientGameInfoState *)doCopyTrump:(ClientPluginCoreSuit * _Nullable)trump whoShuffleCardsPlayerId:(NSString * _Nullable)whoShuffleCardsPlayerId whoChoseSuitPlayerId:(NSString * _Nullable)whoChoseSuitPlayerId __attribute__((swift_name("doCopy(trump:whoShuffleCardsPlayerId:whoChoseSuitPlayerId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginCoreSuit * _Nullable trump __attribute__((swift_name("trump")));
@property (readonly) NSString * _Nullable whoChoseSuitPlayerId __attribute__((swift_name("whoChoseSuitPlayerId")));
@property (readonly) NSString * _Nullable whoShuffleCardsPlayerId __attribute__((swift_name("whoShuffleCardsPlayerId")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineErrorState")))
@interface ClientPluginEngineErrorState : ClientPluginBase
- (instancetype)initWithMessage:(NSString * _Nullable)message userInfo:(ClientPluginCoreGameUserInfo * _Nullable)userInfo reason:(ClientPluginCoreTerminationGameReason *)reason __attribute__((swift_name("init(message:userInfo:reason:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineErrorState *)doCopyMessage:(NSString * _Nullable)message userInfo:(ClientPluginCoreGameUserInfo * _Nullable)userInfo reason:(ClientPluginCoreTerminationGameReason *)reason __attribute__((swift_name("doCopy(message:userInfo:reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly) ClientPluginCoreTerminationGameReason *reason __attribute__((swift_name("reason")));
@property (readonly) ClientPluginCoreGameUserInfo * _Nullable userInfo __attribute__((swift_name("userInfo")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientClientStatePayload.Companion")))
@interface ClientPluginGame_clientClientStatePayloadCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginGame_clientClientStatePayloadCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginGame_clientClientStatePayload *)initialConfig:(ClientPluginGame_clientClientConfig *)config combinations:(ClientPluginGame_clientCombinationToAnnounceState * _Nullable)combinations __attribute__((swift_name("initial(config:combinations:)")));
- (ClientPluginGame_clientClientStatePayload *)resetConfig:(ClientPluginGame_clientClientConfig *)config isForeground:(BOOL)isForeground isSceneActive:(BOOL)isSceneActive messages:(NSArray<id<ClientPluginEngineMessage>> *)messages bufferedActions:(NSArray<id<ClientPluginCoreBufferedAction>> *)bufferedActions expectants:(NSArray<ClientPluginGame_clientExpectantWrapper<id<ClientPluginCoreExpectantAction>> *> *)expectants combinations:(ClientPluginGame_clientCombinationToAnnounceState * _Nullable)combinations processingCard:(ClientPluginCoreProcessingCard * _Nullable)processingCard error:(ClientPluginEngineErrorState * _Nullable)error __attribute__((swift_name("reset(config:isForeground:isSceneActive:messages:bufferedActions:expectants:combinations:processingCard:error:)")));
- (ClientPluginGame_clientClientStatePayload *)resetFullConfig:(ClientPluginGame_clientClientConfig *)config isForeground:(BOOL)isForeground isSceneActive:(BOOL)isSceneActive __attribute__((swift_name("resetFull(config:isForeground:isSceneActive:)")));
- (ClientPluginGame_clientClientStatePayload *)resetRoundConfig:(ClientPluginGame_clientClientConfig *)config isForeground:(BOOL)isForeground isSceneActive:(BOOL)isSceneActive messages:(NSArray<id<ClientPluginEngineMessage>> *)messages bufferedActions:(NSArray<id<ClientPluginCoreBufferedAction>> *)bufferedActions error:(ClientPluginEngineErrorState * _Nullable)error __attribute__((swift_name("resetRound(config:isForeground:isSceneActive:messages:bufferedActions:error:)")));
@end

__attribute__((swift_name("EngineRatingCalculator")))
@protocol ClientPluginEngineRatingCalculator
@required
- (double)calculateNewRatingPlayer:(ClientPluginEnginePlayerRatingInput *)player score:(int32_t)score opponents:(NSArray<ClientPluginEnginePlayerRatingInput *> *)opponents minScore:(int32_t)minScore maxScore:(int32_t)maxScore minUserRating:(int32_t)minUserRating maxUserRating:(int32_t)maxUserRating limitByMaxRating:(BOOL)limitByMaxRating gameComplexityCoefficient:(double)gameComplexityCoefficient __attribute__((swift_name("calculateNewRating(player:score:opponents:minScore:maxScore:minUserRating:maxUserRating:limitByMaxRating:gameComplexityCoefficient:)")));
- (double)calculateNewRatingCurrentRating:(double)currentRating opponentRating:(double)opponentRating playerScore:(int32_t)playerScore minScore:(int32_t)minScore maxScore:(int32_t)maxScore minUserRating:(int32_t)minUserRating maxUserRating:(int32_t)maxUserRating limitByMaxRating:(BOOL)limitByMaxRating gameComplexityCoefficient:(double)gameComplexityCoefficient kFactor:(int32_t)kFactor __attribute__((swift_name("calculateNewRating(currentRating:opponentRating:playerScore:minScore:maxScore:minUserRating:maxUserRating:limitByMaxRating:gameComplexityCoefficient:kFactor:)")));
- (NSArray<ClientPluginEnginePlayerRatingOutput *> *)calculatePlayersRatingMaxScore:(int32_t)maxScore minUserRating:(int32_t)minUserRating maxUserRating:(int32_t)maxUserRating limitByMaxRating:(BOOL)limitByMaxRating gameComplexityCoefficient:(double)gameComplexityCoefficient ratingList:(NSArray<ClientPluginEnginePlayerRatingInput *> *)ratingList __attribute__((swift_name("calculatePlayersRating(maxScore:minUserRating:maxUserRating:limitByMaxRating:gameComplexityCoefficient:ratingList:)")));
- (NSArray<ClientPluginEnginePlayerRatingOutput *> *)calculateTeamsRatingMaxScore:(int32_t)maxScore minUserRating:(int32_t)minUserRating maxUserRating:(int32_t)maxUserRating limitByMaxRating:(BOOL)limitByMaxRating gameComplexityCoefficient:(double)gameComplexityCoefficient ratingList:(NSArray<ClientPluginEnginePlayerRatingInput *> *)ratingList __attribute__((swift_name("calculateTeamsRating(maxScore:minUserRating:maxUserRating:limitByMaxRating:gameComplexityCoefficient:ratingList:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayerRatingOutput")))
@interface ClientPluginEnginePlayerRatingOutput : ClientPluginBase
- (instancetype)initWithPlayerId:(NSString *)playerId newRating:(double)newRating oldRating:(double)oldRating isWinner:(BOOL)isWinner __attribute__((swift_name("init(playerId:newRating:oldRating:isWinner:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEnginePlayerRatingOutput *)doCopyPlayerId:(NSString *)playerId newRating:(double)newRating oldRating:(double)oldRating isWinner:(BOOL)isWinner __attribute__((swift_name("doCopy(playerId:newRating:oldRating:isWinner:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isWinner __attribute__((swift_name("isWinner")));
@property (readonly, getter=doNewRating) double newRating __attribute__((swift_name("newRating")));
@property (readonly) double oldRating __attribute__((swift_name("oldRating")));
@property (readonly) NSString *playerId __attribute__((swift_name("playerId")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayerRatingInput")))
@interface ClientPluginEnginePlayerRatingInput : ClientPluginBase
- (instancetype)initWithPlayerId:(NSString *)playerId rating:(double)rating earnedPoints:(int32_t)earnedPoints isWinner:(BOOL)isWinner isBot:(BOOL)isBot playedGamesCount:(int32_t)playedGamesCount __attribute__((swift_name("init(playerId:rating:earnedPoints:isWinner:isBot:playedGamesCount:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEnginePlayerRatingInput *)doCopyPlayerId:(NSString *)playerId rating:(double)rating earnedPoints:(int32_t)earnedPoints isWinner:(BOOL)isWinner isBot:(BOOL)isBot playedGamesCount:(int32_t)playedGamesCount __attribute__((swift_name("doCopy(playerId:rating:earnedPoints:isWinner:isBot:playedGamesCount:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t earnedPoints __attribute__((swift_name("earnedPoints")));
@property (readonly) BOOL isBot __attribute__((swift_name("isBot")));
@property (readonly) BOOL isWinner __attribute__((swift_name("isWinner")));
@property (readonly) int32_t playedGamesCount __attribute__((swift_name("playedGamesCount")));
@property (readonly) NSString *playerId __attribute__((swift_name("playerId")));
@property (readonly) double rating __attribute__((swift_name("rating")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineLeaguesConfig")))
@interface ClientPluginEngineLeaguesConfig : ClientPluginBase
- (instancetype)initWithInitialUserRating:(int32_t)initialUserRating minUserRating:(int32_t)minUserRating maxUserRating:(int32_t)maxUserRating leagues:(ClientPluginKotlinArray<ClientPluginEngineLeagueInfo *> *)leagues __attribute__((swift_name("init(initialUserRating:minUserRating:maxUserRating:leagues:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineLeaguesConfigCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineLeaguesConfig *)doCopyInitialUserRating:(int32_t)initialUserRating minUserRating:(int32_t)minUserRating maxUserRating:(int32_t)maxUserRating leagues:(ClientPluginKotlinArray<ClientPluginEngineLeagueInfo *> *)leagues __attribute__((swift_name("doCopy(initialUserRating:minUserRating:maxUserRating:leagues:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (ClientPluginEngineLeague *)getLeagueByRatingRating:(double)rating __attribute__((swift_name("getLeagueByRating(rating:)")));
- (ClientPluginEngineLeagueInfo *)getLeagueInfoLeague:(ClientPluginEngineLeague *)league __attribute__((swift_name("getLeagueInfo(league:)")));
- (ClientPluginEngineLeagueInfo *)getLeagueInfoByRatingRating:(double)rating __attribute__((swift_name("getLeagueInfoByRating(rating:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t initialUserRating __attribute__((swift_name("initialUserRating")));
@property (readonly) ClientPluginKotlinArray<ClientPluginEngineLeagueInfo *> *leagues __attribute__((swift_name("leagues")));
@property (readonly) int32_t maxUserRating __attribute__((swift_name("maxUserRating")));
@property (readonly) int32_t minUserRating __attribute__((swift_name("minUserRating")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientUpdateConfigModel")))
@interface ClientPluginGame_clientUpdateConfigModel : ClientPluginBase
- (instancetype)initWithClientConfig:(ClientPluginGame_clientClientConfig * _Nullable)clientConfig options:(ClientPluginEngineOptions * _Nullable)options rulesSetType:(ClientPluginEngineRulesSetType * _Nullable)rulesSetType rules:(ClientPluginEngineRules * _Nullable)rules users:(NSArray<ClientPluginCoreGameUserInfo *> * _Nullable)users __attribute__((swift_name("init(clientConfig:options:rulesSetType:rules:users:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientUpdateConfigModel *)doCopyClientConfig:(ClientPluginGame_clientClientConfig * _Nullable)clientConfig options:(ClientPluginEngineOptions * _Nullable)options rulesSetType:(ClientPluginEngineRulesSetType * _Nullable)rulesSetType rules:(ClientPluginEngineRules * _Nullable)rules users:(NSArray<ClientPluginCoreGameUserInfo *> * _Nullable)users __attribute__((swift_name("doCopy(clientConfig:options:rulesSetType:rules:users:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginGame_clientClientConfig * _Nullable clientConfig __attribute__((swift_name("clientConfig")));
@property (readonly) ClientPluginEngineOptions * _Nullable options __attribute__((swift_name("options")));
@property (readonly) ClientPluginEngineRules * _Nullable rules __attribute__((swift_name("rules")));
@property (readonly) ClientPluginEngineRulesSetType * _Nullable rulesSetType __attribute__((swift_name("rulesSetType")));
@property (readonly) NSArray<ClientPluginCoreGameUserInfo *> * _Nullable users __attribute__((swift_name("users")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientPlayerViewModel")))
@interface ClientPluginGame_clientPlayerViewModel : ClientPluginBase <ClientPluginCorePlayerIdContract, ClientPluginEngineShuffleCardsContract>
- (instancetype)initWithUserInfo:(ClientPluginCoreGameUserInfo *)userInfo selectedSuit:(ClientPluginCoreSuit * _Nullable)selectedSuit isShuffleCards:(BOOL)isShuffleCards isCurrentPlayer:(BOOL)isCurrentPlayer isGameCreator:(BOOL)isGameCreator points:(int32_t)points isRatingUp:(ClientPluginBoolean * _Nullable)isRatingUp wasPlayerOffline:(ClientPluginBoolean * _Nullable)wasPlayerOffline __attribute__((swift_name("init(userInfo:selectedSuit:isShuffleCards:isCurrentPlayer:isGameCreator:points:isRatingUp:wasPlayerOffline:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientPlayerViewModel *)doCopyUserInfo:(ClientPluginCoreGameUserInfo *)userInfo selectedSuit:(ClientPluginCoreSuit * _Nullable)selectedSuit isShuffleCards:(BOOL)isShuffleCards isCurrentPlayer:(BOOL)isCurrentPlayer isGameCreator:(BOOL)isGameCreator points:(int32_t)points isRatingUp:(ClientPluginBoolean * _Nullable)isRatingUp wasPlayerOffline:(ClientPluginBoolean * _Nullable)wasPlayerOffline __attribute__((swift_name("doCopy(userInfo:selectedSuit:isShuffleCards:isCurrentPlayer:isGameCreator:points:isRatingUp:wasPlayerOffline:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isCurrentPlayer __attribute__((swift_name("isCurrentPlayer")));
@property (readonly) BOOL isGameCreator __attribute__((swift_name("isGameCreator")));
@property (readonly) ClientPluginBoolean * _Nullable isRatingUp __attribute__((swift_name("isRatingUp")));
@property (readonly) BOOL isShuffleCards __attribute__((swift_name("isShuffleCards")));
@property (readonly) NSString *playerId __attribute__((swift_name("playerId")));
@property (readonly) int32_t points __attribute__((swift_name("points")));
@property (readonly) ClientPluginCoreSuit * _Nullable selectedSuit __attribute__((swift_name("selectedSuit")));
@property (readonly) ClientPluginCoreGameUserInfo *userInfo __attribute__((swift_name("userInfo")));
@property (readonly) ClientPluginBoolean * _Nullable wasPlayerOffline __attribute__((swift_name("wasPlayerOffline")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineGameLifecycleState")))
@interface ClientPluginEngineGameLifecycleState : ClientPluginKotlinEnum<ClientPluginEngineGameLifecycleState *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginEngineGameLifecycleStateCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginEngineGameLifecycleState *created __attribute__((swift_name("created")));
@property (class, readonly) ClientPluginEngineGameLifecycleState *creating __attribute__((swift_name("creating")));
@property (class, readonly) ClientPluginEngineGameLifecycleState *finishing __attribute__((swift_name("finishing")));
@property (class, readonly) ClientPluginEngineGameLifecycleState *finished __attribute__((swift_name("finished")));
+ (ClientPluginKotlinArray<ClientPluginEngineGameLifecycleState *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineGameLifecycleState *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientClientAppSavedState")))
@interface ClientPluginGame_clientClientAppSavedState : ClientPluginBase
- (instancetype)initWithTable:(ClientPluginEngineJassTable *)table payload:(ClientPluginGame_clientClientStatePayload *)payload __attribute__((swift_name("init(table:payload:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientClientAppSavedState *)doCopyTable:(ClientPluginEngineJassTable *)table payload:(ClientPluginGame_clientClientStatePayload *)payload __attribute__((swift_name("doCopy(table:payload:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginGame_clientClientStatePayload *payload __attribute__((swift_name("payload")));
@property (readonly) ClientPluginEngineJassTable *table __attribute__((swift_name("table")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientPointsContentViewModel")))
@interface ClientPluginGame_clientPointsContentViewModel : ClientPluginBase
- (instancetype)initWithRoundPoints:(id<ClientPluginGame_clientPointClause>)roundPoints cells:(NSArray<ClientPluginGame_clientPointsCellViewModel *> *)cells playersAnalytics:(NSArray<ClientPluginEnginePlayerHistoryAnalytics *> * _Nullable)playersAnalytics __attribute__((swift_name("init(roundPoints:cells:playersAnalytics:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientPointsContentViewModel *)doCopyRoundPoints:(id<ClientPluginGame_clientPointClause>)roundPoints cells:(NSArray<ClientPluginGame_clientPointsCellViewModel *> *)cells playersAnalytics:(NSArray<ClientPluginEnginePlayerHistoryAnalytics *> * _Nullable)playersAnalytics __attribute__((swift_name("doCopy(roundPoints:cells:playersAnalytics:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginGame_clientPointsCellViewModel *> *cells __attribute__((swift_name("cells")));
@property (readonly) NSArray<ClientPluginEnginePlayerHistoryAnalytics *> * _Nullable playersAnalytics __attribute__((swift_name("playersAnalytics")));
@property (readonly) id<ClientPluginGame_clientPointClause> roundPoints __attribute__((swift_name("roundPoints")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientChatSceneContractObserverStrategy")))
@interface ClientPluginGame_clientChatSceneContractObserverStrategy : ClientPluginKotlinEnum<ClientPluginGame_clientChatSceneContractObserverStrategy *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginGame_clientChatSceneContractObserverStrategy *allUpdates __attribute__((swift_name("allUpdates")));
@property (class, readonly) ClientPluginGame_clientChatSceneContractObserverStrategy *firstOrNextRound __attribute__((swift_name("firstOrNextRound")));
+ (ClientPluginKotlinArray<ClientPluginGame_clientChatSceneContractObserverStrategy *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginGame_clientChatSceneContractObserverStrategy *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreGameCard")))
@interface ClientPluginCoreGameCard : ClientPluginBase
- (instancetype)initWithIndex:(int32_t)index __attribute__((swift_name("init(index:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginCoreGameCardCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginCoreGameCard *)doCopyIndex:(int32_t)index __attribute__((swift_name("doCopy(index:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t index __attribute__((swift_name("index")));
@property (readonly) BOOL isReal __attribute__((swift_name("isReal")));
@property (readonly) BOOL isStub __attribute__((swift_name("isStub")));
@property (readonly) ClientPluginCoreGameCardCardName *name __attribute__((swift_name("name")));
@property (readonly) int32_t number __attribute__((swift_name("number")));
@property (readonly) ClientPluginCoreSuit *suit __attribute__((swift_name("suit")));
@end

__attribute__((swift_name("CoreCardPayload")))
@interface ClientPluginCoreCardPayload : ClientPluginBase
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientCardDeckViewModel")))
@interface ClientPluginGame_clientCardDeckViewModel : ClientPluginBase
- (instancetype)initWithSize:(int32_t)size cardsToDistribute:(NSArray<ClientPluginCoreGameCard *> *)cardsToDistribute trumpCard:(ClientPluginCoreGameCard * _Nullable)trumpCard frezaCard:(ClientPluginCoreGameCard * _Nullable)frezaCard isUsed:(BOOL)isUsed __attribute__((swift_name("init(size:cardsToDistribute:trumpCard:frezaCard:isUsed:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientCardDeckViewModel *)doCopySize:(int32_t)size cardsToDistribute:(NSArray<ClientPluginCoreGameCard *> *)cardsToDistribute trumpCard:(ClientPluginCoreGameCard * _Nullable)trumpCard frezaCard:(ClientPluginCoreGameCard * _Nullable)frezaCard isUsed:(BOOL)isUsed __attribute__((swift_name("doCopy(size:cardsToDistribute:trumpCard:frezaCard:isUsed:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginCoreGameCard *> *cardsToDistribute __attribute__((swift_name("cardsToDistribute")));
@property (readonly) ClientPluginCoreGameCard * _Nullable frezaCard __attribute__((swift_name("frezaCard")));
@property (readonly) BOOL isUsed __attribute__((swift_name("isUsed")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@property (readonly) ClientPluginCoreGameCard * _Nullable trumpCard __attribute__((swift_name("trumpCard")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LoggerCombinedRaspberryLogger.Companion")))
@interface ClientPluginLoggerCombinedRaspberryLoggerCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginLoggerCombinedRaspberryLoggerCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginLoggerCombinedRaspberryLogger *)empty __attribute__((swift_name("empty()")));
- (ClientPluginLoggerCombinedRaspberryLogger *)platform __attribute__((swift_name("platform()")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreDisposableHandle")))
@protocol ClientPluginKotlinx_coroutines_coreDisposableHandle
@required
- (void)dispose __attribute__((swift_name("dispose()")));
@end


/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
__attribute__((swift_name("Kotlinx_coroutines_coreChildHandle")))
@protocol ClientPluginKotlinx_coroutines_coreChildHandle <ClientPluginKotlinx_coroutines_coreDisposableHandle>
@required

/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
- (BOOL)childCancelledCause:(ClientPluginKotlinThrowable *)cause __attribute__((swift_name("childCancelled(cause:)")));

/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
@property (readonly) id<ClientPluginKotlinx_coroutines_coreJob> _Nullable parent __attribute__((swift_name("parent")));
@end


/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
__attribute__((swift_name("Kotlinx_coroutines_coreChildJob")))
@protocol ClientPluginKotlinx_coroutines_coreChildJob <ClientPluginKotlinx_coroutines_coreJob>
@required

/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
- (void)parentCancelledParentJob:(id<ClientPluginKotlinx_coroutines_coreParentJob>)parentJob __attribute__((swift_name("parentCancelled(parentJob:)")));
@end

__attribute__((swift_name("KotlinSequence")))
@protocol ClientPluginKotlinSequence
@required
- (id<ClientPluginKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
@end


/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
__attribute__((swift_name("Kotlinx_coroutines_coreSelectClause")))
@protocol ClientPluginKotlinx_coroutines_coreSelectClause
@required
@property (readonly) id clauseObject __attribute__((swift_name("clauseObject")));
@property (readonly) ClientPluginKotlinUnit *(^(^ _Nullable onCancellationConstructor)(id<ClientPluginKotlinx_coroutines_coreSelectInstance>, id _Nullable, id _Nullable))(ClientPluginKotlinThrowable *, id _Nullable, id<ClientPluginKotlinCoroutineContext>) __attribute__((swift_name("onCancellationConstructor")));
@property (readonly) id _Nullable (^processResFunc)(id, id _Nullable, id _Nullable) __attribute__((swift_name("processResFunc")));
@property (readonly) void (^regFunc)(id, id<ClientPluginKotlinx_coroutines_coreSelectInstance>, id _Nullable) __attribute__((swift_name("regFunc")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreSelectClause0")))
@protocol ClientPluginKotlinx_coroutines_coreSelectClause0 <ClientPluginKotlinx_coroutines_coreSelectClause>
@required
@end

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol ClientPluginKotlinCoroutineContextKey
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreLogType.Companion")))
@interface ClientPluginCoreLogTypeCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginCoreLogTypeCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((swift_name("KotlinSuspendFunction2")))
@protocol ClientPluginKotlinSuspendFunction2 <ClientPluginKotlinFunction>
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 p2:(id _Nullable)p2 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:p2:completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineGameStore")))
@interface ClientPluginEngineGameStore : ClientPluginBase
- (instancetype)initWithDi:(id<ClientPluginKodein_diDI>)di tag:(NSString *)tag preloadedState:(ClientPluginEngineAppState *)preloadedState externalReducers:(NSArray<id<ClientPluginCoreReducer>> *)externalReducers externalMiddlewares:(NSArray<id<ClientPluginCoreMiddleware>> *)externalMiddlewares externalEnhancers:(NSArray<id<ClientPluginCoreStore> (^(^)(id<ClientPluginCoreStore> (^)(id<ClientPluginCoreReducer>, ClientPluginEngineAppState *, id _Nullable)))(id<ClientPluginCoreReducer>, ClientPluginEngineAppState *, id _Nullable)> *)externalEnhancers __attribute__((swift_name("init(di:tag:preloadedState:externalReducers:externalMiddlewares:externalEnhancers:)"))) __attribute__((objc_designated_initializer));
- (void)dispatchAsyncBlock:(id<ClientPluginKotlinSuspendFunction2>)block __attribute__((swift_name("dispatchAsync(block:)")));
@property (readonly) ClientPluginEngineAppState *state __attribute__((swift_name("state")));
@property (readonly) id<ClientPluginCoreStore> store __attribute__((swift_name("store")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineAppState")))
@interface ClientPluginEngineAppState : ClientPluginBase
- (instancetype)initWithTableOrNull:(ClientPluginEngineJassTable * _Nullable)tableOrNull payload:(id<ClientPluginEngineAppStatePayload> _Nullable)payload gameLifecycleState:(ClientPluginEngineGameLifecycleState *)gameLifecycleState terminationState:(ClientPluginEngineTerminationState * _Nullable)terminationState timers:(NSArray<id<ClientPluginCoreTimerTag>> *)timers __attribute__((swift_name("init(tableOrNull:payload:gameLifecycleState:terminationState:timers:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineAppState *)doCopyTableOrNull:(ClientPluginEngineJassTable * _Nullable)tableOrNull payload:(id<ClientPluginEngineAppStatePayload> _Nullable)payload gameLifecycleState:(ClientPluginEngineGameLifecycleState *)gameLifecycleState terminationState:(ClientPluginEngineTerminationState * _Nullable)terminationState timers:(NSArray<id<ClientPluginCoreTimerTag>> *)timers __attribute__((swift_name("doCopy(tableOrNull:payload:gameLifecycleState:terminationState:timers:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginEngineGameLifecycleState *gameLifecycleState __attribute__((swift_name("gameLifecycleState")));
@property (readonly) BOOL isTableExists __attribute__((swift_name("isTableExists")));
@property (readonly) id<ClientPluginEngineAppStatePayload> _Nullable payload __attribute__((swift_name("payload")));
@property (readonly) ClientPluginEngineTerminationState *requireTerminationState __attribute__((swift_name("requireTerminationState")));
@property (readonly) ClientPluginEngineJassTable *table __attribute__((swift_name("table")));
@property (readonly) ClientPluginEngineJassTable * _Nullable tableOrNull __attribute__((swift_name("tableOrNull")));
@property (readonly) ClientPluginEngineTerminationState * _Nullable terminationState __attribute__((swift_name("terminationState")));
@property (readonly) NSArray<id<ClientPluginCoreTimerTag>> *timers __attribute__((swift_name("timers")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreSuit.Companion")))
@interface ClientPluginCoreSuitCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginCoreSuitCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((swift_name("EngineGameTransition")))
@protocol ClientPluginEngineGameTransition
@required
@property (readonly) BOOL isEnableChoosePartnerScreen __attribute__((swift_name("isEnableChoosePartnerScreen")));
@property (readonly) NSArray<NSString *> * _Nullable previousPlayers __attribute__((swift_name("previousPlayers")));
@property (readonly) ClientPluginKotlinArray<NSString *> *readyPlayerIdsArray __attribute__((swift_name("readyPlayerIdsArray")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRoundContractType")))
@interface ClientPluginEngineRoundContractType : ClientPluginKotlinEnum<ClientPluginEngineRoundContractType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginEngineRoundContractTypeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginEngineRoundContractType *trumpTaken __attribute__((swift_name("trumpTaken")));
@property (class, readonly) ClientPluginEngineRoundContractType *trumpSelected __attribute__((swift_name("trumpSelected")));
@property (class, readonly) ClientPluginEngineRoundContractType *noTrumps __attribute__((swift_name("noTrumps")));
@property (class, readonly) ClientPluginEngineRoundContractType *allTrumps __attribute__((swift_name("allTrumps")));
+ (ClientPluginKotlinArray<ClientPluginEngineRoundContractType *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineRoundContractType *> *entries __attribute__((swift_name("entries")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineBidType")))
@interface ClientPluginEngineBidType : ClientPluginKotlinEnum<ClientPluginEngineBidType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginEngineBidTypeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginEngineBidType *points __attribute__((swift_name("points")));
@property (class, readonly) ClientPluginEngineBidType *capot __attribute__((swift_name("capot")));
@property (class, readonly) ClientPluginEngineBidType *coinche __attribute__((swift_name("coinche")));
@property (class, readonly) ClientPluginEngineBidType *general __attribute__((swift_name("general")));
@property (class, readonly) ClientPluginEngineBidType *surcoinche __attribute__((swift_name("surcoinche")));
+ (ClientPluginKotlinArray<ClientPluginEngineBidType *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineBidType *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineTrumpCardStepPartnerMode")))
@interface ClientPluginEngineTrumpCardStepPartnerMode : ClientPluginKotlinEnum<ClientPluginEngineTrumpCardStepPartnerMode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginEngineTrumpCardStepPartnerMode *anyCard __attribute__((swift_name("anyCard")));
@property (class, readonly) ClientPluginEngineTrumpCardStepPartnerMode *higherTrumpCard __attribute__((swift_name("higherTrumpCard")));
@property (class, readonly) ClientPluginEngineTrumpCardStepPartnerMode *anyTrumpCard __attribute__((swift_name("anyTrumpCard")));
+ (ClientPluginKotlinArray<ClientPluginEngineTrumpCardStepPartnerMode *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineTrumpCardStepPartnerMode *> *entries __attribute__((swift_name("entries")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCombinationType")))
@interface ClientPluginEngineCombinationType : ClientPluginKotlinEnum<ClientPluginEngineCombinationType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginEngineCombinationTypeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginEngineCombinationType *bela __attribute__((swift_name("bela")));
@property (class, readonly) ClientPluginEngineCombinationType *threeInRow __attribute__((swift_name("threeInRow")));
@property (class, readonly) ClientPluginEngineCombinationType *fourInRow __attribute__((swift_name("fourInRow")));
@property (class, readonly) ClientPluginEngineCombinationType *fiveInRow __attribute__((swift_name("fiveInRow")));
@property (class, readonly) ClientPluginEngineCombinationType *sevenInRow __attribute__((swift_name("sevenInRow")));
@property (class, readonly) ClientPluginEngineCombinationType *sevenTrump __attribute__((swift_name("sevenTrump")));
@property (class, readonly) ClientPluginEngineCombinationType *fourAces __attribute__((swift_name("fourAces")));
@property (class, readonly) ClientPluginEngineCombinationType *fourKings __attribute__((swift_name("fourKings")));
@property (class, readonly) ClientPluginEngineCombinationType *fourDames __attribute__((swift_name("fourDames")));
@property (class, readonly) ClientPluginEngineCombinationType *fourJacks __attribute__((swift_name("fourJacks")));
@property (class, readonly) ClientPluginEngineCombinationType *fourTens __attribute__((swift_name("fourTens")));
@property (class, readonly) ClientPluginEngineCombinationType *fourNines __attribute__((swift_name("fourNines")));
@property (class, readonly) ClientPluginEngineCombinationType *fourSevens __attribute__((swift_name("fourSevens")));
@property (class, readonly) ClientPluginEngineCombinationType *fineNoBribes __attribute__((swift_name("fineNoBribes")));
@property (class, readonly) ClientPluginEngineCombinationType *fineThirdFailedContract __attribute__((swift_name("fineThirdFailedContract")));
@property (class, readonly) ClientPluginEngineCombinationType *bonusLastBribe __attribute__((swift_name("bonusLastBribe")));
@property (class, readonly) ClientPluginEngineCombinationType *bonusFailedContract __attribute__((swift_name("bonusFailedContract")));
@property (class, readonly) ClientPluginEngineCombinationType *bonusControversialContract __attribute__((swift_name("bonusControversialContract")));
+ (ClientPluginKotlinArray<ClientPluginEngineCombinationType *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineCombinationType *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePointsDistributeMode")))
@interface ClientPluginEnginePointsDistributeMode : ClientPluginKotlinEnum<ClientPluginEnginePointsDistributeMode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginEnginePointsDistributeMode *all __attribute__((swift_name("all")));
@property (class, readonly) ClientPluginEnginePointsDistributeMode *highestPlayer __attribute__((swift_name("highestPlayer")));
@property (class, readonly) ClientPluginEnginePointsDistributeMode *higherThenFailedPlayer __attribute__((swift_name("higherThenFailedPlayer")));
+ (ClientPluginKotlinArray<ClientPluginEnginePointsDistributeMode *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEnginePointsDistributeMode *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRules.Companion")))
@interface ClientPluginEngineRulesCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineRulesCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineRules *)belot __attribute__((swift_name("belot()")));
- (ClientPluginEngineRules *)belotBalgarian __attribute__((swift_name("belotBalgarian()")));
- (ClientPluginEngineRules *)common __attribute__((swift_name("common()")));
- (ClientPluginEngineRules *)custom __attribute__((swift_name("custom()")));
- (ClientPluginEngineRules *)kharkiv __attribute__((swift_name("kharkiv()")));
- (ClientPluginEngineRules *)klabor __attribute__((swift_name("klabor()")));
- (ClientPluginEngineRules *)saltivka __attribute__((swift_name("saltivka()")));
@property (readonly) int32_t HAND_CARDS_FINAL __attribute__((swift_name("HAND_CARDS_FINAL")));
@property (readonly) int32_t HAND_CARDS_INITIAL __attribute__((swift_name("HAND_CARDS_INITIAL")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineBotIntelligenceLevel")))
@interface ClientPluginEngineBotIntelligenceLevel : ClientPluginKotlinEnum<ClientPluginEngineBotIntelligenceLevel *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginEngineBotIntelligenceLevelCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginEngineBotIntelligenceLevel *superHard __attribute__((swift_name("superHard")));
@property (class, readonly) ClientPluginEngineBotIntelligenceLevel *hard __attribute__((swift_name("hard")));
@property (class, readonly) ClientPluginEngineBotIntelligenceLevel *medium __attribute__((swift_name("medium")));
@property (class, readonly) ClientPluginEngineBotIntelligenceLevel *easy __attribute__((swift_name("easy")));
@property (class, readonly) ClientPluginEngineBotIntelligenceLevel *superEasy __attribute__((swift_name("superEasy")));
+ (ClientPluginKotlinArray<ClientPluginEngineBotIntelligenceLevel *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineBotIntelligenceLevel *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineOptions.Companion")))
@interface ClientPluginEngineOptionsCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineOptionsCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineOptions *)getServerOptionsIsEnableChat:(BOOL)isEnableChat isBotsEnabled:(BOOL)isBotsEnabled isEnableChoosePartnerScreen:(BOOL)isEnableChoosePartnerScreen __attribute__((swift_name("getServerOptions(isEnableChat:isBotsEnabled:isEnableChoosePartnerScreen:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayersMode.Companion")))
@interface ClientPluginEnginePlayersModeCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEnginePlayersModeCompanion *shared __attribute__((swift_name("shared")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (ClientPluginEnginePlayersMode *)createNumber:(int32_t)number __attribute__((swift_name("create(number:)")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePointsMode.Companion")))
@interface ClientPluginEnginePointsModeCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEnginePointsModeCompanion *shared __attribute__((swift_name("shared")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (ClientPluginEnginePointsMode *)createPoints:(int32_t)points __attribute__((swift_name("create(points:)")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreCompositeEncoder")))
@protocol ClientPluginKotlinx_serialization_coreCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (id<ClientPluginKotlinx_serialization_coreEncoder>)encodeInlineElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeInlineElement(descriptor:index:)")));
- (void)encodeIntElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (void)encodeNullableSerializableElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<ClientPluginKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<ClientPluginKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)endStructureDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) ClientPluginKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("KotlinAnnotation")))
@protocol ClientPluginKotlinAnnotation
@required
@end


/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
__attribute__((swift_name("Kotlinx_serialization_coreSerialKind")))
@interface ClientPluginKotlinx_serialization_coreSerialKind : ClientPluginBase
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreCompositeDecoder")))
@protocol ClientPluginKotlinx_serialization_coreCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (id<ClientPluginKotlinx_serialization_coreDecoder>)decodeInlineElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeInlineElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<ClientPluginKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:previousValue:)")));

/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<ClientPluginKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (int16_t)decodeShortElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
@property (readonly) ClientPluginKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface ClientPluginKotlinNothing : ClientPluginBase
@end


/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
__attribute__((swift_name("Kotlinx_serialization_coreSerializersModuleCollector")))
@protocol ClientPluginKotlinx_serialization_coreSerializersModuleCollector
@required
- (void)contextualKClass:(id<ClientPluginKotlinKClass>)kClass provider:(id<ClientPluginKotlinx_serialization_coreKSerializer> (^)(NSArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *))provider __attribute__((swift_name("contextual(kClass:provider:)")));
- (void)contextualKClass:(id<ClientPluginKotlinKClass>)kClass serializer:(id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<ClientPluginKotlinKClass>)baseClass actualClass:(id<ClientPluginKotlinKClass>)actualClass actualSerializer:(id<ClientPluginKotlinx_serialization_coreKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
- (void)polymorphicDefaultBaseClass:(id<ClientPluginKotlinKClass>)baseClass defaultDeserializerProvider:(id<ClientPluginKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefault(baseClass:defaultDeserializerProvider:)"))) __attribute__((deprecated("Deprecated in favor of function with more precise name: polymorphicDefaultDeserializer")));
- (void)polymorphicDefaultDeserializerBaseClass:(id<ClientPluginKotlinKClass>)baseClass defaultDeserializerProvider:(id<ClientPluginKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefaultDeserializer(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultSerializerBaseClass:(id<ClientPluginKotlinKClass>)baseClass defaultSerializerProvider:(id<ClientPluginKotlinx_serialization_coreSerializationStrategy> _Nullable (^)(id))defaultSerializerProvider __attribute__((swift_name("polymorphicDefaultSerializer(baseClass:defaultSerializerProvider:)")));
@end

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol ClientPluginKotlinKDeclarationContainer
@required
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
__attribute__((swift_name("KotlinKClassifier")))
@protocol ClientPluginKotlinKClassifier
@required
@end

__attribute__((swift_name("KotlinKClass")))
@protocol ClientPluginKotlinKClass <ClientPluginKotlinKDeclarationContainer, ClientPluginKotlinKAnnotatedElement, ClientPluginKotlinKClassifier>
@required

/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement.Companion")))
@interface ClientPluginKotlinx_serialization_jsonJsonElementCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginKotlinx_serialization_jsonJsonElementCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonClassDiscriminatorMode")))
@interface ClientPluginKotlinx_serialization_jsonClassDiscriminatorMode : ClientPluginKotlinEnum<ClientPluginKotlinx_serialization_jsonClassDiscriminatorMode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginKotlinx_serialization_jsonClassDiscriminatorMode *none __attribute__((swift_name("none")));
@property (class, readonly) ClientPluginKotlinx_serialization_jsonClassDiscriminatorMode *allJsonObjects __attribute__((swift_name("allJsonObjects")));
@property (class, readonly) ClientPluginKotlinx_serialization_jsonClassDiscriminatorMode *polymorphic __attribute__((swift_name("polymorphic")));
+ (ClientPluginKotlinArray<ClientPluginKotlinx_serialization_jsonClassDiscriminatorMode *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginKotlinx_serialization_jsonClassDiscriminatorMode *> *entries __attribute__((swift_name("entries")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.ExperimentalSerializationApi
*/
__attribute__((swift_name("Kotlinx_serialization_jsonJsonNamingStrategy")))
@protocol ClientPluginKotlinx_serialization_jsonJsonNamingStrategy
@required
- (NSString *)serialNameForJsonDescriptor:(id<ClientPluginKotlinx_serialization_coreSerialDescriptor>)descriptor elementIndex:(int32_t)elementIndex serialName:(NSString *)serialName __attribute__((swift_name("serialNameForJson(descriptor:elementIndex:serialName:)")));
@end

__attribute__((swift_name("Kodein_diBinding")))
@protocol ClientPluginKodein_diBinding
@required
- (id (^)(id _Nullable))getFactoryKey:(ClientPluginKodein_diDIKey<id, id, id> *)key di:(id<ClientPluginKodein_diBindingDI>)di __attribute__((swift_name("getFactory(key:di:)")));
@end

__attribute__((swift_name("Kodein_diDIBinding")))
@protocol ClientPluginKodein_diDIBinding <ClientPluginKodein_diBinding>
@required
- (NSString *)factoryFullName __attribute__((swift_name("factoryFullName()")));
- (NSString *)factoryName __attribute__((swift_name("factoryName()")));
@property (readonly) id<ClientPluginKaveritTypeToken> argType __attribute__((swift_name("argType")));
@property (readonly) id<ClientPluginKaveritTypeToken> contextType __attribute__((swift_name("contextType")));
@property (readonly) id<ClientPluginKodein_diDIBindingCopier> _Nullable copier __attribute__((swift_name("copier")));
@property (readonly) id<ClientPluginKaveritTypeToken> createdType __attribute__((swift_name("createdType")));
@property (readonly) NSString *description_ __attribute__((swift_name("description_")));
@property (readonly) NSString *fullDescription __attribute__((swift_name("fullDescription")));
@property (readonly) id<ClientPluginKodein_diScope> _Nullable scope __attribute__((swift_name("scope")));
@property (readonly) BOOL supportSubTypes __attribute__((swift_name("supportSubTypes")));
@end

__attribute__((swift_name("Kodein_diDIBuilderDirectBinder")))
@protocol ClientPluginKodein_diDIBuilderDirectBinder
@required
@end

__attribute__((swift_name("Kodein_diDIBuilderTypeBinder")))
@protocol ClientPluginKodein_diDIBuilderTypeBinder
@required
- (void)withBinding:(id<ClientPluginKodein_diDIBinding>)binding __attribute__((swift_name("with(binding:)")));
@end

__attribute__((swift_name("KaveritTypeToken")))
@protocol ClientPluginKaveritTypeToken
@required
- (ClientPluginKotlinArray<id<ClientPluginKaveritTypeToken>> *)getGenericParameters __attribute__((swift_name("getGenericParameters()")));
- (id<ClientPluginKaveritTypeToken>)getRaw __attribute__((swift_name("getRaw()")));
- (NSArray<id<ClientPluginKaveritTypeToken>> *)getSuper __attribute__((swift_name("getSuper()")));
- (BOOL)isAssignableFromTypeToken:(id<ClientPluginKaveritTypeToken>)typeToken __attribute__((swift_name("isAssignableFrom(typeToken:)")));
- (BOOL)isGeneric __attribute__((swift_name("isGeneric()")));
- (BOOL)isWildcard __attribute__((swift_name("isWildcard()")));
- (NSString *)qualifiedDispString __attribute__((swift_name("qualifiedDispString()")));
- (NSString *)qualifiedErasedDispString __attribute__((swift_name("qualifiedErasedDispString()")));
- (NSString *)simpleDispString __attribute__((swift_name("simpleDispString()")));
- (NSString *)simpleErasedDispString __attribute__((swift_name("simpleErasedDispString()")));
@end

__attribute__((swift_name("Kodein_diDIBuilderArgSetBinder")))
@protocol ClientPluginKodein_diDIBuilderArgSetBinder
@required
- (void)addCreateBinding:(id<ClientPluginKodein_diDIBinding> (^)(void))createBinding __attribute__((swift_name("add(createBinding:)")));
- (void)bindTag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides createBinding:(id<ClientPluginKodein_diDIBinding> (^)(void))createBinding __attribute__((swift_name("bind(tag:overrides:createBinding:)")));
@end

__attribute__((swift_name("Kodein_diDIBuilderSetBinder")))
@protocol ClientPluginKodein_diDIBuilderSetBinder
@required
- (void)addCreateBinding_:(id<ClientPluginKodein_diDIBinding> (^)(void))createBinding __attribute__((swift_name("add(createBinding_:)")));
- (void)bindTag:(id _Nullable)tag overrides:(ClientPluginBoolean * _Nullable)overrides createBinding_:(id<ClientPluginKodein_diDIBinding> (^)(void))createBinding __attribute__((swift_name("bind(tag:overrides:createBinding_:)")));
@end

__attribute__((swift_name("Kodein_diDIBuilderDelegateBinder")))
@interface ClientPluginKodein_diDIBuilderDelegateBinder<T> : ClientPluginBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)ToType:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag __attribute__((swift_name("To(type:tag:)")));
- (void)toTag:(id _Nullable)tag __attribute__((swift_name("to(tag:)")));
@end

__attribute__((swift_name("Kodein_diContextTranslator")))
@protocol ClientPluginKodein_diContextTranslator
@required
- (id _Nullable)translateDi:(id<ClientPluginKodein_diDirectDI>)di ctx:(id)ctx __attribute__((swift_name("translate(di:ctx:)")));
@property (readonly) id<ClientPluginKaveritTypeToken> contextType __attribute__((swift_name("contextType")));
@property (readonly) id<ClientPluginKaveritTypeToken> scopeType __attribute__((swift_name("scopeType")));
@end

__attribute__((swift_name("Kodein_diDIBuilderConstantBinder")))
@protocol ClientPluginKodein_diDIBuilderConstantBinder
@required
- (void)WithValueType:(id<ClientPluginKaveritTypeToken>)valueType value:(id)value __attribute__((swift_name("With(valueType:value:)")));
@end

__attribute__((swift_name("Kodein_diDirectDIAware")))
@protocol ClientPluginKodein_diDirectDIAware
@required
@property (readonly) id<ClientPluginKodein_diDirectDI> directDI __attribute__((swift_name("directDI")));
@end

__attribute__((swift_name("Kodein_diDirectDIBase")))
@protocol ClientPluginKodein_diDirectDIBase <ClientPluginKodein_diDirectDIAware>
@required
- (id (^)(id _Nullable))FactoryArgType:(id<ClientPluginKaveritTypeToken>)argType type:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag __attribute__((swift_name("Factory(argType:type:tag:)")));
- (id (^ _Nullable)(id _Nullable))FactoryOrNullArgType:(id<ClientPluginKaveritTypeToken>)argType type:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag __attribute__((swift_name("FactoryOrNull(argType:type:tag:)")));
- (id)InstanceType:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag __attribute__((swift_name("Instance(type:tag:)")));
- (id)InstanceArgType:(id<ClientPluginKaveritTypeToken>)argType type:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag arg:(id _Nullable)arg __attribute__((swift_name("Instance(argType:type:tag:arg:)")));
- (id _Nullable)InstanceOrNullType:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag __attribute__((swift_name("InstanceOrNull(type:tag:)")));
- (id _Nullable)InstanceOrNullArgType:(id<ClientPluginKaveritTypeToken>)argType type:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag arg:(id _Nullable)arg __attribute__((swift_name("InstanceOrNull(argType:type:tag:arg:)")));
- (id<ClientPluginKodein_diDirectDI>)OnContext:(id<ClientPluginKodein_diDIContext>)context __attribute__((swift_name("On(context:)")));
- (id (^)(void))ProviderType:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag __attribute__((swift_name("Provider(type:tag:)")));
- (id (^)(void))ProviderArgType:(id<ClientPluginKaveritTypeToken>)argType type:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag arg:(id _Nullable (^)(void))arg __attribute__((swift_name("Provider(argType:type:tag:arg:)")));
- (id (^ _Nullable)(void))ProviderOrNullType:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag __attribute__((swift_name("ProviderOrNull(type:tag:)")));
- (id (^ _Nullable)(void))ProviderOrNullArgType:(id<ClientPluginKaveritTypeToken>)argType type:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag arg:(id _Nullable (^)(void))arg __attribute__((swift_name("ProviderOrNull(argType:type:tag:arg:)")));
@property (readonly) id<ClientPluginKodein_diDIContainer> container __attribute__((swift_name("container")));
@property (readonly) id<ClientPluginKodein_diDI> di __attribute__((swift_name("di")));
@property (readonly) id<ClientPluginKodein_diDI> lazy __attribute__((swift_name("lazy")));
@end

__attribute__((swift_name("Kodein_diDirectDI")))
@protocol ClientPluginKodein_diDirectDI <ClientPluginKodein_diDirectDIBase>
@required
@end

__attribute__((swift_name("Kodein_diDIContainerBuilder")))
@protocol ClientPluginKodein_diDIContainerBuilder
@required
- (void)bindKey:(ClientPluginKodein_diDIKey<id, id, id> *)key binding:(id<ClientPluginKodein_diDIBinding>)binding fromModule:(NSString * _Nullable)fromModule overrides:(ClientPluginBoolean * _Nullable)overrides __attribute__((swift_name("bind(key:binding:fromModule:overrides:)")));
- (void)extendContainer:(id<ClientPluginKodein_diDIContainer>)container allowOverride:(BOOL)allowOverride copy:(NSSet<ClientPluginKodein_diDIKey<id, id, id> *> *)copy __attribute__((swift_name("extend(container:allowOverride:copy:)")));
- (void)onReadyCb:(void (^)(id<ClientPluginKodein_diDirectDI>))cb __attribute__((swift_name("onReady(cb:)")));
- (void)registerContextTranslatorTranslator:(id<ClientPluginKodein_diContextTranslator>)translator __attribute__((swift_name("registerContextTranslator(translator:)")));
- (id<ClientPluginKodein_diDIContainerBuilder>)subBuilderAllowOverride:(BOOL)allowOverride silentOverride:(BOOL)silentOverride __attribute__((swift_name("subBuilder(allowOverride:silentOverride:)")));
@end

__attribute__((swift_name("Kodein_diScope")))
@protocol ClientPluginKodein_diScope
@required
- (ClientPluginKodein_diScopeRegistry *)getRegistryContext:(id _Nullable)context __attribute__((swift_name("getRegistry(context:)")));
@end

__attribute__((swift_name("KotlinKType")))
@protocol ClientPluginKotlinKType
@required

/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
@property (readonly) NSArray<ClientPluginKotlinKTypeProjection *> *arguments __attribute__((swift_name("arguments")));

/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
@property (readonly) id<ClientPluginKotlinKClassifier> _Nullable classifier __attribute__((swift_name("classifier")));
@property (readonly) BOOL isMarkedNullable __attribute__((swift_name("isMarkedNullable")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kodein_diDIKey")))
@interface ClientPluginKodein_diDIKey<__contravariant C, __contravariant A, __covariant T> : ClientPluginBase
- (instancetype)initWithContextType:(id<ClientPluginKaveritTypeToken>)contextType argType:(id<ClientPluginKaveritTypeToken>)argType type:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag __attribute__((swift_name("init(contextType:argType:type:tag:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginKodein_diDIKey<C, A, T> *)doCopyContextType:(id<ClientPluginKaveritTypeToken>)contextType argType:(id<ClientPluginKaveritTypeToken>)argType type:(id<ClientPluginKaveritTypeToken>)type tag:(id _Nullable)tag __attribute__((swift_name("doCopy(contextType:argType:type:tag:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<ClientPluginKaveritTypeToken> argType __attribute__((swift_name("argType")));
@property (readonly) NSString *bindDescription __attribute__((swift_name("bindDescription")));
@property (readonly) NSString *bindFullDescription __attribute__((swift_name("bindFullDescription")));
@property (readonly) id<ClientPluginKaveritTypeToken> contextType __attribute__((swift_name("contextType")));
@property (readonly) NSString *description_ __attribute__((swift_name("description_")));
@property (readonly) NSString *fullDescription __attribute__((swift_name("fullDescription")));
@property (readonly) NSString *internalDescription __attribute__((swift_name("internalDescription")));
@property (readonly) id _Nullable tag __attribute__((swift_name("tag")));
@property (readonly) id<ClientPluginKaveritTypeToken> type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinUnit")))
@interface ClientPluginKotlinUnit : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)unit __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginKotlinUnit *shared __attribute__((swift_name("shared")));
- (NSString *)description __attribute__((swift_name("description()")));
@end

__attribute__((swift_name("Kodein_diDITree")))
@protocol ClientPluginKodein_diDITree
@required
- (NSArray<ClientPluginKotlinTriple<ClientPluginKodein_diDIKey<id, id, id> *, NSArray<ClientPluginKodein_diDIDefinition<id, id, id> *> *, id<ClientPluginKodein_diContextTranslator>> *> *)findSearch:(ClientPluginKodein_diSearchSpecs *)search __attribute__((swift_name("find(search:)")));
- (NSArray<ClientPluginKotlinTriple<ClientPluginKodein_diDIKey<id, id, id> *, ClientPluginKodein_diDIDefinition<id, id, id> *, id<ClientPluginKodein_diContextTranslator>> *> *)findKey:(ClientPluginKodein_diDIKey<id, id, id> *)key overrideLevel:(int32_t)overrideLevel all:(BOOL)all __attribute__((swift_name("find(key:overrideLevel:all:)")));
- (ClientPluginKotlinTriple<ClientPluginKodein_diDIKey<id, id, id> *, NSArray<ClientPluginKodein_diDIDefinition<id, id, id> *> *, id<ClientPluginKodein_diContextTranslator>> * _Nullable)getKey_:(ClientPluginKodein_diDIKey<id, id, id> *)key __attribute__((swift_name("get(key_:)")));
@property (readonly) NSDictionary<ClientPluginKodein_diDIKey<id, id, id> *, NSArray<ClientPluginKodein_diDIDefinition<id, id, id> *> *> *bindings __attribute__((swift_name("bindings")));
@property (readonly) NSArray<id<ClientPluginKodein_diExternalSource>> *externalSources __attribute__((swift_name("externalSources")));
@property (readonly) NSArray<id<ClientPluginKodein_diContextTranslator>> *registeredTranslators __attribute__((swift_name("registeredTranslators")));
@end

__attribute__((swift_name("KotlinLazy")))
@protocol ClientPluginKotlinLazy
@required
- (BOOL)isInitialized __attribute__((swift_name("isInitialized()")));
@property (readonly) id _Nullable value_ __attribute__((swift_name("value_")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_datetimeInstant.Companion")))
@interface ClientPluginKotlinx_datetimeInstantCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginKotlinx_datetimeInstantCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginKotlinx_datetimeInstant *)fromEpochMillisecondsEpochMilliseconds:(int64_t)epochMilliseconds __attribute__((swift_name("fromEpochMilliseconds(epochMilliseconds:)")));
- (ClientPluginKotlinx_datetimeInstant *)fromEpochSecondsEpochSeconds:(int64_t)epochSeconds nanosecondAdjustment:(int32_t)nanosecondAdjustment __attribute__((swift_name("fromEpochSeconds(epochSeconds:nanosecondAdjustment:)")));
- (ClientPluginKotlinx_datetimeInstant *)fromEpochSecondsEpochSeconds:(int64_t)epochSeconds nanosecondAdjustment_:(int64_t)nanosecondAdjustment __attribute__((swift_name("fromEpochSeconds(epochSeconds:nanosecondAdjustment_:)")));
- (ClientPluginKotlinx_datetimeInstant *)now __attribute__((swift_name("now()"))) __attribute__((unavailable("Use Clock.System.now() instead")));
- (ClientPluginKotlinx_datetimeInstant *)parseInput:(id)input format:(id<ClientPluginKotlinx_datetimeDateTimeFormat>)format __attribute__((swift_name("parse(input:format:)")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@property (readonly) ClientPluginKotlinx_datetimeInstant *DISTANT_FUTURE __attribute__((swift_name("DISTANT_FUTURE")));
@property (readonly) ClientPluginKotlinx_datetimeInstant *DISTANT_PAST __attribute__((swift_name("DISTANT_PAST")));
@end

__attribute__((swift_name("EngineRoundHistoryItem")))
@interface ClientPluginEngineRoundHistoryItem : ClientPluginBase
@property (readonly) ClientPluginKotlinx_datetimeInstant *createdAt __attribute__((swift_name("createdAt")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRoundContractState")))
@interface ClientPluginEngineRoundContractState : ClientPluginKotlinEnum<ClientPluginEngineRoundContractState *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginEngineRoundContractStateCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginEngineRoundContractState *failed __attribute__((swift_name("failed")));
@property (class, readonly) ClientPluginEngineRoundContractState *fulfilled __attribute__((swift_name("fulfilled")));
@property (class, readonly) ClientPluginEngineRoundContractState *controversial __attribute__((swift_name("controversial")));
+ (ClientPluginKotlinArray<ClientPluginEngineRoundContractState *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineRoundContractState *> *entries __attribute__((swift_name("entries")));
@property (readonly) BOOL isControversial __attribute__((swift_name("isControversial")));
@property (readonly) BOOL isFailed __attribute__((swift_name("isFailed")));
@property (readonly) BOOL isFulfilled __attribute__((swift_name("isFulfilled")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRoundHistory.Companion")))
@interface ClientPluginEngineRoundHistoryCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineRoundHistoryCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineRoundHistory *)initialRoundNumber:(int32_t)roundNumber __attribute__((swift_name("initial(roundNumber:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRandomData")))
@interface ClientPluginEngineRandomData : ClientPluginBase
- (instancetype)initWithSignature:(NSString * _Nullable)signature id:(NSString *)id indexes:(NSArray<ClientPluginInt *> *)indexes provider:(ClientPluginEngineDealerStrategy *)provider __attribute__((swift_name("init(signature:id:indexes:provider:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineRandomData *)doCopySignature:(NSString * _Nullable)signature id:(NSString *)id indexes:(NSArray<ClientPluginInt *> *)indexes provider:(ClientPluginEngineDealerStrategy *)provider __attribute__((swift_name("doCopy(signature:id:indexes:provider:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSArray<ClientPluginInt *> *indexes __attribute__((swift_name("indexes")));
@property (readonly) ClientPluginEngineDealerStrategy *provider __attribute__((swift_name("provider")));
@property (readonly) NSString * _Nullable signature __attribute__((swift_name("signature")));
@end

__attribute__((swift_name("EnginePlayerHistoryItem")))
@protocol ClientPluginEnginePlayerHistoryItem <ClientPluginCorePlayerIdContract>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayerHistoryAnalytics")))
@interface ClientPluginEnginePlayerHistoryAnalytics : ClientPluginBase <ClientPluginEnginePlayerHistoryItem>
- (instancetype)initWithPlayerId:(NSString *)playerId liveDurationPercentage:(ClientPluginInt * _Nullable)liveDurationPercentage combinations:(NSArray<ClientPluginEngineCombinationAnalytics *> * _Nullable)combinations achievements:(ClientPluginEngineAchievements * _Nullable)achievements __attribute__((swift_name("init(playerId:liveDurationPercentage:combinations:achievements:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEnginePlayerHistoryAnalytics *)doCopyPlayerId:(NSString *)playerId liveDurationPercentage:(ClientPluginInt * _Nullable)liveDurationPercentage combinations:(NSArray<ClientPluginEngineCombinationAnalytics *> * _Nullable)combinations achievements:(ClientPluginEngineAchievements * _Nullable)achievements __attribute__((swift_name("doCopy(playerId:liveDurationPercentage:combinations:achievements:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginEngineAchievements * _Nullable achievements __attribute__((swift_name("achievements")));
@property (readonly) NSArray<ClientPluginEngineCombinationAnalytics *> * _Nullable combinations __attribute__((swift_name("combinations")));
@property (readonly) ClientPluginInt * _Nullable liveDurationPercentage __attribute__((swift_name("liveDurationPercentage")));
@property (readonly) NSString *playerId __attribute__((swift_name("playerId")));
@property (readonly) BOOL wasPlayerOffline __attribute__((swift_name("wasPlayerOffline")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineGameHistoryMetadata.Companion")))
@interface ClientPluginEngineGameHistoryMetadataCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineGameHistoryMetadataCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineGameHistoryMetadata *)initial __attribute__((swift_name("initial()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CorePlayerState")))
@interface ClientPluginCorePlayerState : ClientPluginBase
- (instancetype)initWithStartFromTime:(ClientPluginKotlinx_datetimeInstant * _Nullable)startFromTime playerTurnTimeout:(ClientPluginKotlinx_datetimeInstant * _Nullable)playerTurnTimeout waitPlayerUntilTime:(ClientPluginKotlinx_datetimeInstant * _Nullable)waitPlayerUntilTime state:(ClientPluginCorePlayerStatePlayerWaitingState *)state ready:(BOOL)ready tag:(NSString * _Nullable)tag connection:(ClientPluginCorePlayerConnection * _Nullable)connection __attribute__((swift_name("init(startFromTime:playerTurnTimeout:waitPlayerUntilTime:state:ready:tag:connection:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginCorePlayerStateCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginCorePlayerState *)doCopyStartFromTime:(ClientPluginKotlinx_datetimeInstant * _Nullable)startFromTime playerTurnTimeout:(ClientPluginKotlinx_datetimeInstant * _Nullable)playerTurnTimeout waitPlayerUntilTime:(ClientPluginKotlinx_datetimeInstant * _Nullable)waitPlayerUntilTime state:(ClientPluginCorePlayerStatePlayerWaitingState *)state ready:(BOOL)ready tag:(NSString * _Nullable)tag connection:(ClientPluginCorePlayerConnection * _Nullable)connection __attribute__((swift_name("doCopy(startFromTime:playerTurnTimeout:waitPlayerUntilTime:state:ready:tag:connection:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (int64_t)getProgressLogger:(id<ClientPluginLoggerRaspberryLogger>)logger __attribute__((swift_name("getProgress(logger:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)provideTag __attribute__((swift_name("provideTag()")));
- (NSString *)requireTag __attribute__((swift_name("requireTag()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginCorePlayerConnection * _Nullable connection __attribute__((swift_name("connection")));
@property (readonly) BOOL isIdle __attribute__((swift_name("isIdle")));
@property (readonly) BOOL isPlayerTurn __attribute__((swift_name("isPlayerTurn")));
@property (readonly) ClientPluginKotlinx_datetimeInstant * _Nullable playerTurnTimeout __attribute__((swift_name("playerTurnTimeout")));
@property (readonly) BOOL ready __attribute__((swift_name("ready")));
@property (readonly) ClientPluginCorePlayerConnection *requiredConnection __attribute__((swift_name("requiredConnection")));
@property (readonly) ClientPluginKotlinx_datetimeInstant * _Nullable startFromTime __attribute__((swift_name("startFromTime")));
@property (readonly) ClientPluginCorePlayerStatePlayerWaitingState *state __attribute__((swift_name("state")));
@property (readonly) NSString * _Nullable tag __attribute__((swift_name("tag")));
@property (readonly) ClientPluginKotlinx_datetimeInstant * _Nullable waitPlayerUntilTime __attribute__((swift_name("waitPlayerUntilTime")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayerOptions")))
@interface ClientPluginEnginePlayerOptions : ClientPluginBase
- (instancetype)initWithIsGameCreator:(BOOL)isGameCreator playerNumber:(int32_t)playerNumber isGameWinner:(BOOL)isGameWinner isShuffleCards:(BOOL)isShuffleCards isRoundWinner:(BOOL)isRoundWinner isWonCardLot:(BOOL)isWonCardLot isEarnedBiggestPointInLastGame:(BOOL)isEarnedBiggestPointInLastGame failedContractsCount:(int32_t)failedContractsCount sentVoicesCount:(int32_t)sentVoicesCount isGotLastBribe:(BOOL)isGotLastBribe numberOfCircle:(ClientPluginEnginePlayerOptionsRoundNumber *)numberOfCircle isChoseSuit:(BOOL)isChoseSuit isChoseSuitInLastGame:(BOOL)isChoseSuitInLastGame __attribute__((swift_name("init(isGameCreator:playerNumber:isGameWinner:isShuffleCards:isRoundWinner:isWonCardLot:isEarnedBiggestPointInLastGame:failedContractsCount:sentVoicesCount:isGotLastBribe:numberOfCircle:isChoseSuit:isChoseSuitInLastGame:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEnginePlayerOptionsCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEnginePlayerOptions *)doCopyIsGameCreator:(BOOL)isGameCreator playerNumber:(int32_t)playerNumber isGameWinner:(BOOL)isGameWinner isShuffleCards:(BOOL)isShuffleCards isRoundWinner:(BOOL)isRoundWinner isWonCardLot:(BOOL)isWonCardLot isEarnedBiggestPointInLastGame:(BOOL)isEarnedBiggestPointInLastGame failedContractsCount:(int32_t)failedContractsCount sentVoicesCount:(int32_t)sentVoicesCount isGotLastBribe:(BOOL)isGotLastBribe numberOfCircle:(ClientPluginEnginePlayerOptionsRoundNumber *)numberOfCircle isChoseSuit:(BOOL)isChoseSuit isChoseSuitInLastGame:(BOOL)isChoseSuitInLastGame __attribute__((swift_name("doCopy(isGameCreator:playerNumber:isGameWinner:isShuffleCards:isRoundWinner:isWonCardLot:isEarnedBiggestPointInLastGame:failedContractsCount:sentVoicesCount:isGotLastBribe:numberOfCircle:isChoseSuit:isChoseSuitInLastGame:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (ClientPluginEnginePlayerOptionsRoundNumber *)incrementNumberOfCircle __attribute__((swift_name("incrementNumberOfCircle()")));
- (ClientPluginEnginePlayerOptionsRoundNumber * _Nullable)incrementNumberOfCircleSafe __attribute__((swift_name("incrementNumberOfCircleSafe()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t failedContractsCount __attribute__((swift_name("failedContractsCount")));
@property (readonly) BOOL isChoseSuit __attribute__((swift_name("isChoseSuit")));
@property (readonly) BOOL isChoseSuitInLastGame __attribute__((swift_name("isChoseSuitInLastGame")));
@property (readonly) BOOL isCircleNotStarted __attribute__((swift_name("isCircleNotStarted")));
@property (readonly) BOOL isEarnedBiggestPointInLastGame __attribute__((swift_name("isEarnedBiggestPointInLastGame")));
@property (readonly) BOOL isFirstSuitCirclePassed __attribute__((swift_name("isFirstSuitCirclePassed")));
@property (readonly) BOOL isGameCreator __attribute__((swift_name("isGameCreator")));
@property (readonly) BOOL isGameWinner __attribute__((swift_name("isGameWinner")));
@property (readonly) BOOL isGotLastBribe __attribute__((swift_name("isGotLastBribe")));
@property (readonly) BOOL isRoundWinner __attribute__((swift_name("isRoundWinner")));
@property (readonly) BOOL isSecondSuitCirclePassed __attribute__((swift_name("isSecondSuitCirclePassed")));
@property (readonly) BOOL isShuffleCards __attribute__((swift_name("isShuffleCards")));
@property (readonly) BOOL isWonCardLot __attribute__((swift_name("isWonCardLot")));
@property (readonly) ClientPluginEnginePlayerOptionsRoundNumber *numberOfCircle __attribute__((swift_name("numberOfCircle")));
@property (readonly) int32_t playerNumber __attribute__((swift_name("playerNumber")));
@property (readonly) int32_t sentVoicesCount __attribute__((swift_name("sentVoicesCount")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayerHand")))
@interface ClientPluginEnginePlayerHand : ClientPluginBase
- (instancetype)initWithAverageLuckyFactor:(float)averageLuckyFactor luckyFactors:(NSArray<ClientPluginEngineLuckyFactorRecord *> *)luckyFactors gameCombinationRecords:(NSArray<ClientPluginEngineCombinationRecord *> *)gameCombinationRecords sessionCombinationRecords:(NSArray<ClientPluginEngineCombinationRecord *> *)sessionCombinationRecords hasNoTrump:(BOOL)hasNoTrump hasNoDiamond:(BOOL)hasNoDiamond hasNoSpade:(BOOL)hasNoSpade hasNoHeart:(BOOL)hasNoHeart hasNoClub:(BOOL)hasNoClub __attribute__((swift_name("init(averageLuckyFactor:luckyFactors:gameCombinationRecords:sessionCombinationRecords:hasNoTrump:hasNoDiamond:hasNoSpade:hasNoHeart:hasNoClub:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEnginePlayerHandCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEnginePlayerHand *)doCopyAverageLuckyFactor:(float)averageLuckyFactor luckyFactors:(NSArray<ClientPluginEngineLuckyFactorRecord *> *)luckyFactors gameCombinationRecords:(NSArray<ClientPluginEngineCombinationRecord *> *)gameCombinationRecords sessionCombinationRecords:(NSArray<ClientPluginEngineCombinationRecord *> *)sessionCombinationRecords hasNoTrump:(BOOL)hasNoTrump hasNoDiamond:(BOOL)hasNoDiamond hasNoSpade:(BOOL)hasNoSpade hasNoHeart:(BOOL)hasNoHeart hasNoClub:(BOOL)hasNoClub __attribute__((swift_name("doCopy(averageLuckyFactor:luckyFactors:gameCombinationRecords:sessionCombinationRecords:hasNoTrump:hasNoDiamond:hasNoSpade:hasNoHeart:hasNoClub:)")));
- (ClientPluginEngineLuckyFactorRecord *)currentRoundFactor __attribute__((swift_name("currentRoundFactor()")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (float)getAverageLuckyFactorInGame __attribute__((swift_name("getAverageLuckyFactorInGame()")));
- (float)getAverageLuckyFactorInSession __attribute__((swift_name("getAverageLuckyFactorInSession()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t acceptedCombinationsCountInGame __attribute__((swift_name("acceptedCombinationsCountInGame")));
@property (readonly) float averageLuckyFactor __attribute__((swift_name("averageLuckyFactor")));
@property (readonly) int32_t combinationsCountInGame __attribute__((swift_name("combinationsCountInGame")));
@property (readonly) int32_t combinationsCountInSession __attribute__((swift_name("combinationsCountInSession")));
@property (readonly) NSArray<ClientPluginEngineCombinationRecord *> *gameCombinationRecords __attribute__((swift_name("gameCombinationRecords")));
@property (readonly) BOOL hasNoClub __attribute__((swift_name("hasNoClub")));
@property (readonly) BOOL hasNoDiamond __attribute__((swift_name("hasNoDiamond")));
@property (readonly) BOOL hasNoHeart __attribute__((swift_name("hasNoHeart")));
@property (readonly) BOOL hasNoSpade __attribute__((swift_name("hasNoSpade")));
@property (readonly) BOOL hasNoTrump __attribute__((swift_name("hasNoTrump")));
@property (readonly) NSArray<ClientPluginEngineLuckyFactorRecord *> *luckyFactors __attribute__((swift_name("luckyFactors")));
@property (readonly) NSArray<ClientPluginEngineCombinationRecord *> *sessionCombinationRecords __attribute__((swift_name("sessionCombinationRecords")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayerPoints")))
@interface ClientPluginEnginePlayerPoints : ClientPluginBase
- (instancetype)initWithEarnedPoints:(int32_t)earnedPoints combinations:(NSArray<ClientPluginEngineCombinationDetails *> *)combinations earnedCards:(NSArray<ClientPluginCoreGameCard *> *)earnedCards __attribute__((swift_name("init(earnedPoints:combinations:earnedCards:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEnginePlayerPoints *)doCopyEarnedPoints:(int32_t)earnedPoints combinations:(NSArray<ClientPluginEngineCombinationDetails *> *)combinations earnedCards:(NSArray<ClientPluginCoreGameCard *> *)earnedCards __attribute__((swift_name("doCopy(earnedPoints:combinations:earnedCards:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginEngineCombinationDetails *> *combinations __attribute__((swift_name("combinations")));
@property (readonly) NSArray<ClientPluginCoreGameCard *> *earnedCards __attribute__((swift_name("earnedCards")));
@property (readonly) int32_t earnedPoints __attribute__((swift_name("earnedPoints")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineJassPlayer.Companion")))
@interface ClientPluginEngineJassPlayerCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineJassPlayerCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineJassPlayer *)createUserInfo:(ClientPluginCoreGameUserInfo *)userInfo playerNumber:(int32_t)playerNumber isGameCreator:(BOOL)isGameCreator isShuffleCards:(BOOL)isShuffleCards state:(ClientPluginCorePlayerState *)state hand:(ClientPluginEnginePlayerHand *)hand __attribute__((swift_name("create(userInfo:playerNumber:isGameCreator:isShuffleCards:state:hand:)")));
@end

__attribute__((swift_name("EngineSceneData")))
@protocol ClientPluginEngineSceneData
@required
@property (readonly) NSString *sceneId __attribute__((swift_name("sceneId")));
@end

__attribute__((swift_name("EngineActData")))
@protocol ClientPluginEngineActData
@required
@property (readonly) NSString *actId __attribute__((swift_name("actId")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCardOnTable.Companion")))
@interface ClientPluginEngineCardOnTableCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineCardOnTableCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineCardOnTable * _Nullable)getFirstCard:(NSArray<ClientPluginEngineCardOnTable *> *)receiver __attribute__((swift_name("getFirstCard(_:)")));
- (ClientPluginEngineCardOnTable * _Nullable)getLastCard:(NSArray<ClientPluginEngineCardOnTable *> *)receiver playersCount:(int32_t)playersCount __attribute__((swift_name("getLastCard(_:playersCount:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCardDeck.Companion")))
@interface ClientPluginEngineCardDeckCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineCardDeckCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineCardDeck *)fromCardsCards:(NSArray<ClientPluginCoreGameCard *> *)cards __attribute__((swift_name("fromCards(cards:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRound")))
@interface ClientPluginEngineRound : ClientPluginBase
- (instancetype)initWithRoundNumber:(int32_t)roundNumber numberOfBribe:(ClientPluginInt * _Nullable)numberOfBribe trump:(ClientPluginCoreSuit * _Nullable)trump __attribute__((swift_name("init(roundNumber:numberOfBribe:trump:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineRoundCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineRound *)doCopyRoundNumber:(int32_t)roundNumber numberOfBribe:(ClientPluginInt * _Nullable)numberOfBribe trump:(ClientPluginCoreSuit * _Nullable)trump __attribute__((swift_name("doCopy(roundNumber:numberOfBribe:trump:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t nextBribeNumber __attribute__((swift_name("nextBribeNumber")));
@property (readonly) ClientPluginInt * _Nullable numberOfBribe __attribute__((swift_name("numberOfBribe")));
@property (readonly) int32_t numberOfBribeOrZero __attribute__((swift_name("numberOfBribeOrZero")));
@property (readonly) int32_t roundNumber __attribute__((swift_name("roundNumber")));
@property (readonly) ClientPluginCoreSuit *selectedTrump __attribute__((swift_name("selectedTrump")));
@property (readonly) ClientPluginCoreSuit * _Nullable trump __attribute__((swift_name("trump")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePreviousRound")))
@interface ClientPluginEnginePreviousRound : ClientPluginBase
- (instancetype)initWithBribes:(NSArray<ClientPluginEngineBribe *> *)bribes cardDeck:(ClientPluginEngineCardDeck * _Nullable)cardDeck points:(NSArray<ClientPluginEnginePointItem *> *)points __attribute__((swift_name("init(bribes:cardDeck:points:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEnginePreviousRound *)doCopyBribes:(NSArray<ClientPluginEngineBribe *> *)bribes cardDeck:(ClientPluginEngineCardDeck * _Nullable)cardDeck points:(NSArray<ClientPluginEnginePointItem *> *)points __attribute__((swift_name("doCopy(bribes:cardDeck:points:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginEngineBribe *> *bribes __attribute__((swift_name("bribes")));
@property (readonly) ClientPluginEngineCardDeck * _Nullable cardDeck __attribute__((swift_name("cardDeck")));
@property (readonly) NSArray<ClientPluginEnginePointItem *> *points __attribute__((swift_name("points")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineGameInfo.Companion")))
@interface ClientPluginEngineGameInfoCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineGameInfoCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineGameInfo *)initial __attribute__((swift_name("initial()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineJassTableLite")))
@interface ClientPluginEngineJassTableLite : ClientPluginBase
- (instancetype)initWithId:(NSString *)id config:(ClientPluginEngineConfigLite *)config players:(NSArray<ClientPluginEngineJassPlayerLite *> *)players gameInfo:(ClientPluginEngineGameInfo *)gameInfo __attribute__((swift_name("init(id:config:players:gameInfo:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineJassTableLite *)doCopyId:(NSString *)id config:(ClientPluginEngineConfigLite *)config players:(NSArray<ClientPluginEngineJassPlayerLite *> *)players gameInfo:(ClientPluginEngineGameInfo *)gameInfo __attribute__((swift_name("doCopy(id:config:players:gameInfo:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginEngineConfigLite *config __attribute__((swift_name("config")));
@property (readonly) ClientPluginEngineGameInfo *gameInfo __attribute__((swift_name("gameInfo")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSArray<ClientPluginEngineJassPlayerLite *> *players __attribute__((swift_name("players")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CorePlayerConnectionState.Companion")))
@interface ClientPluginCorePlayerConnectionStateCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginCorePlayerConnectionStateCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineMessageMessageLifecycle")))
@interface ClientPluginEngineMessageMessageLifecycle : ClientPluginKotlinEnum<ClientPluginEngineMessageMessageLifecycle *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginEngineMessageMessageLifecycle *round __attribute__((swift_name("round")));
@property (class, readonly) ClientPluginEngineMessageMessageLifecycle *game __attribute__((swift_name("game")));
@property (class, readonly) ClientPluginEngineMessageMessageLifecycle *room __attribute__((swift_name("room")));
+ (ClientPluginKotlinArray<ClientPluginEngineMessageMessageLifecycle *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineMessageMessageLifecycle *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((swift_name("EngineCombinationContract")))
@protocol ClientPluginEngineCombinationContract
@required
- (NSArray<ClientPluginCoreGameCard *> *)requireCards __attribute__((swift_name("requireCards()")));
@property (readonly) NSArray<ClientPluginCoreGameCard *> * _Nullable cards __attribute__((swift_name("cards")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSSet<ClientPluginEngineCombinationReason *> *reasons __attribute__((swift_name("reasons")));
@property (readonly) ClientPluginEngineCombinationType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCombination")))
@interface ClientPluginEngineCombination : ClientPluginBase <ClientPluginEngineCombinationContract>
- (instancetype)initWithId:(NSString *)id type:(ClientPluginEngineCombinationType *)type cards:(NSArray<ClientPluginCoreGameCard *> * _Nullable)cards reasons:(NSSet<ClientPluginEngineCombinationReason *> *)reasons __attribute__((swift_name("init(id:type:cards:reasons:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineCombination *)doCopyId:(NSString *)id type:(ClientPluginEngineCombinationType *)type cards:(NSArray<ClientPluginCoreGameCard *> * _Nullable)cards reasons:(NSSet<ClientPluginEngineCombinationReason *> *)reasons __attribute__((swift_name("doCopy(id:type:cards:reasons:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginCoreGameCard *> * _Nullable cards __attribute__((swift_name("cards")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSSet<ClientPluginEngineCombinationReason *> *reasons __attribute__((swift_name("reasons")));
@property (readonly) ClientPluginEngineCombinationType *type __attribute__((swift_name("type")));
@end

__attribute__((swift_name("CoreTerminationGameReason")))
@interface ClientPluginCoreTerminationGameReason : ClientPluginBase
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineLeagueInfo")))
@interface ClientPluginEngineLeagueInfo : ClientPluginBase
- (instancetype)initWithLeague:(ClientPluginEngineLeague *)league minRating:(int32_t)minRating maxRating:(int32_t)maxRating enabled:(BOOL)enabled __attribute__((swift_name("init(league:minRating:maxRating:enabled:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineLeagueInfo *)doCopyLeague:(ClientPluginEngineLeague *)league minRating:(int32_t)minRating maxRating:(int32_t)maxRating enabled:(BOOL)enabled __attribute__((swift_name("doCopy(league:minRating:maxRating:enabled:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL enabled __attribute__((swift_name("enabled")));
@property (readonly) ClientPluginEngineLeague *league __attribute__((swift_name("league")));
@property (readonly) int32_t maxRating __attribute__((swift_name("maxRating")));
@property (readonly) int32_t minRating __attribute__((swift_name("minRating")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineLeaguesConfig.Companion")))
@interface ClientPluginEngineLeaguesConfigCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineLeaguesConfigCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineLeaguesConfig *)defaultConfig __attribute__((swift_name("defaultConfig()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineGameLifecycleState.Companion")))
@interface ClientPluginEngineGameLifecycleStateCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineGameLifecycleStateCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((swift_name("Game_clientPointClause")))
@protocol ClientPluginGame_clientPointClause
@required
- (id<ClientPluginGame_clientPointClause>)plusModel:(id<ClientPluginGame_clientPointClause> _Nullable)model __attribute__((swift_name("plus(model:)")));
@property (readonly) int32_t value__ __attribute__((swift_name("value__")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientPointsCellViewModel")))
@interface ClientPluginGame_clientPointsCellViewModel : ClientPluginBase
- (instancetype)initWithPoints:(NSArray<ClientPluginGame_clientPlayerPointsViewModel *> *)points badges:(NSArray<ClientPluginGame_clientBadge *> *)badges beforeEarnedPoints:(int32_t)beforeEarnedPoints finalRoundPoints:(int32_t)finalRoundPoints finalGamePoints:(int32_t)finalGamePoints __attribute__((swift_name("init(points:badges:beforeEarnedPoints:finalRoundPoints:finalGamePoints:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientPointsCellViewModel *)doCopyPoints:(NSArray<ClientPluginGame_clientPlayerPointsViewModel *> *)points badges:(NSArray<ClientPluginGame_clientBadge *> *)badges beforeEarnedPoints:(int32_t)beforeEarnedPoints finalRoundPoints:(int32_t)finalRoundPoints finalGamePoints:(int32_t)finalGamePoints __attribute__((swift_name("doCopy(points:badges:beforeEarnedPoints:finalRoundPoints:finalGamePoints:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginGame_clientBadge *> *badges __attribute__((swift_name("badges")));
@property (readonly) int32_t beforeEarnedPoints __attribute__((swift_name("beforeEarnedPoints")));
@property (readonly) int32_t finalGamePoints __attribute__((swift_name("finalGamePoints")));
@property (readonly) int32_t finalRoundPoints __attribute__((swift_name("finalRoundPoints")));
@property (readonly) NSArray<ClientPluginGame_clientPlayerPointsViewModel *> *points __attribute__((swift_name("points")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreGameCard.Companion")))
@interface ClientPluginCoreGameCardCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginCoreGameCardCompanion *shared __attribute__((swift_name("shared")));
- (int32_t)getStubPositionFromIndex:(int32_t)receiver playerIndex:(int32_t)playerIndex reverse:(BOOL)reverse __attribute__((swift_name("getStubPositionFromIndex(_:playerIndex:reverse:)")));
@property (readonly) int32_t CARD_DECK_SIZE __attribute__((swift_name("CARD_DECK_SIZE")));
@property (readonly) int32_t STUB_CONST __attribute__((swift_name("STUB_CONST")));
@property (readonly) int32_t SUIT_CARD_LINE_CARDS_SIZE __attribute__((swift_name("SUIT_CARD_LINE_CARDS_SIZE")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CoreGameCard.CardName")))
@interface ClientPluginCoreGameCardCardName : ClientPluginKotlinEnum<ClientPluginCoreGameCardCardName *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginCoreGameCardCardName *ace __attribute__((swift_name("ace")));
@property (class, readonly) ClientPluginCoreGameCardCardName *king __attribute__((swift_name("king")));
@property (class, readonly) ClientPluginCoreGameCardCardName *dame __attribute__((swift_name("dame")));
@property (class, readonly) ClientPluginCoreGameCardCardName *jack __attribute__((swift_name("jack")));
@property (class, readonly) ClientPluginCoreGameCardCardName *ten __attribute__((swift_name("ten")));
@property (class, readonly) ClientPluginCoreGameCardCardName *nine __attribute__((swift_name("nine")));
@property (class, readonly) ClientPluginCoreGameCardCardName *eight __attribute__((swift_name("eight")));
@property (class, readonly) ClientPluginCoreGameCardCardName *seven __attribute__((swift_name("seven")));
@property (class, readonly) ClientPluginCoreGameCardCardName *six __attribute__((swift_name("six")));
@property (class, readonly) ClientPluginCoreGameCardCardName *five __attribute__((swift_name("five")));
@property (class, readonly) ClientPluginCoreGameCardCardName *four __attribute__((swift_name("four")));
@property (class, readonly) ClientPluginCoreGameCardCardName *tree __attribute__((swift_name("tree")));
@property (class, readonly) ClientPluginCoreGameCardCardName *two __attribute__((swift_name("two")));
@property (class, readonly) ClientPluginCoreGameCardCardName *stub __attribute__((swift_name("stub")));
+ (ClientPluginKotlinArray<ClientPluginCoreGameCardCardName *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginCoreGameCardCardName *> *entries __attribute__((swift_name("entries")));
- (NSString *)description __attribute__((swift_name("description()")));
@end


/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
__attribute__((swift_name("Kotlinx_coroutines_coreParentJob")))
@protocol ClientPluginKotlinx_coroutines_coreParentJob <ClientPluginKotlinx_coroutines_coreJob>
@required

/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
- (ClientPluginKotlinCancellationException *)getChildJobCancellationCause __attribute__((swift_name("getChildJobCancellationCause()")));
@end


/**
 * @note annotations
 *   kotlinx.coroutines.InternalCoroutinesApi
*/
__attribute__((swift_name("Kotlinx_coroutines_coreSelectInstance")))
@protocol ClientPluginKotlinx_coroutines_coreSelectInstance
@required
- (void)disposeOnCompletionDisposableHandle:(id<ClientPluginKotlinx_coroutines_coreDisposableHandle>)disposableHandle __attribute__((swift_name("disposeOnCompletion(disposableHandle:)")));
- (void)selectInRegistrationPhaseInternalResult:(id _Nullable)internalResult __attribute__((swift_name("selectInRegistrationPhase(internalResult:)")));
- (BOOL)trySelectClauseObject:(id)clauseObject result:(id _Nullable)result __attribute__((swift_name("trySelect(clauseObject:result:)")));
@property (readonly) id<ClientPluginKotlinCoroutineContext> context __attribute__((swift_name("context")));
@end

__attribute__((swift_name("CoreReducer")))
@protocol ClientPluginCoreReducer
@required
- (BOOL)canHandleAction:(id<ClientPluginCoreAction>)action __attribute__((swift_name("canHandle(action:)")));
- (id _Nullable)reduceState:(id _Nullable)state action:(id<ClientPluginCoreAction>)action __attribute__((swift_name("reduce(state:action:)")));
@end

__attribute__((swift_name("CoreMiddleware")))
@protocol ClientPluginCoreMiddleware
@required
- (BOOL)canHandleAction:(id<ClientPluginCoreAction>)action __attribute__((swift_name("canHandle(action:)")));
- (void)handleStore:(id<ClientPluginCoreStore>)store next:(void (^)(id<ClientPluginCoreAction>))next action:(id<ClientPluginCoreAction>)action __attribute__((swift_name("handle(store:next:action:)")));
@end

__attribute__((swift_name("CoreStore")))
@protocol ClientPluginCoreStore
@required
@property void (^dispatch)(id<ClientPluginCoreAction>) __attribute__((swift_name("dispatch")));
@property (readonly) id<ClientPluginKotlinx_coroutines_coreFlow> observe __attribute__((swift_name("observe")));
@property (readonly) void (^replaceReducer)(id<ClientPluginCoreReducer>) __attribute__((swift_name("replaceReducer")));
@property (readonly) id _Nullable state __attribute__((swift_name("state")));
@property (readonly) id<ClientPluginCoreStore> store __attribute__((swift_name("store")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineTerminationState")))
@interface ClientPluginEngineTerminationState : ClientPluginBase
- (instancetype)initWithReason:(ClientPluginCoreTerminationGameReason *)reason transition:(id<ClientPluginEngineGameTransition> _Nullable)transition __attribute__((swift_name("init(reason:transition:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineTerminationState *)doCopyReason:(ClientPluginCoreTerminationGameReason *)reason transition:(id<ClientPluginEngineGameTransition> _Nullable)transition __attribute__((swift_name("doCopy(reason:transition:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginCoreTerminationGameReason *reason __attribute__((swift_name("reason")));
@property (readonly) id<ClientPluginEngineGameTransition> _Nullable transition __attribute__((swift_name("transition")));
@end

__attribute__((swift_name("CoreNotValidateIfGameFinished")))
@protocol ClientPluginCoreNotValidateIfGameFinished
@required
- (BOOL)notValidateWhenFinished __attribute__((swift_name("notValidateWhenFinished()")));
@end

__attribute__((swift_name("CoreTimerTag")))
@protocol ClientPluginCoreTimerTag <ClientPluginCoreNotValidateIfGameFinished>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRoundContractType.Companion")))
@interface ClientPluginEngineRoundContractTypeCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineRoundContractTypeCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@property (readonly) NSArray<ClientPluginEngineRoundContractType *> *debertzContractTypes __attribute__((swift_name("debertzContractTypes")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineBidType.Companion")))
@interface ClientPluginEngineBidTypeCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineBidTypeCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@property (readonly) ClientPluginKotlinArray<ClientPluginEngineBidType *> *belotBalgarian __attribute__((swift_name("belotBalgarian")));
@property (readonly) ClientPluginKotlinArray<ClientPluginEngineBidType *> *coinche __attribute__((swift_name("coinche")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCombinationType.Companion")))
@interface ClientPluginEngineCombinationTypeCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineCombinationTypeCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@property (readonly) NSArray<ClientPluginEngineCombinationType *> *belotWithFirstCardCombination __attribute__((swift_name("belotWithFirstCardCombination")));
@property (readonly) NSArray<ClientPluginEngineCombinationType *> *debertzWithFirstCardCombinations __attribute__((swift_name("debertzWithFirstCardCombinations")));
@property (readonly) NSArray<ClientPluginEngineCombinationType *> *fourCardsCombinations __attribute__((swift_name("fourCardsCombinations")));
@property (readonly) NSArray<ClientPluginEngineCombinationType *> *inRowCombinations __attribute__((swift_name("inRowCombinations")));
@property (readonly) NSArray<ClientPluginEngineCombinationType *> *withFirstCardCombination __attribute__((swift_name("withFirstCardCombination")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineBotIntelligenceLevel.Companion")))
@interface ClientPluginEngineBotIntelligenceLevelCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineBotIntelligenceLevelCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((swift_name("Kodein_diDIBindingCopier")))
@protocol ClientPluginKodein_diDIBindingCopier
@required
- (id<ClientPluginKodein_diDIBinding>)doCopyBuilder:(id<ClientPluginKodein_diDIContainerBuilder>)builder __attribute__((swift_name("doCopy(builder:)")));
@end

__attribute__((swift_name("Kodein_diWithContext")))
@protocol ClientPluginKodein_diWithContext
@required
@property (readonly) id context __attribute__((swift_name("context")));
@end

__attribute__((swift_name("Kodein_diBindingDI")))
@protocol ClientPluginKodein_diBindingDI <ClientPluginKodein_diDirectDI, ClientPluginKodein_diWithContext>
@required
- (id<ClientPluginKodein_diBindingDI>)onErasedContext __attribute__((swift_name("onErasedContext()")));
- (id (^)(id _Nullable))overriddenFactory __attribute__((swift_name("overriddenFactory()")));
- (id (^ _Nullable)(id _Nullable))overriddenFactoryOrNull __attribute__((swift_name("overriddenFactoryOrNull()")));
@end

__attribute__((swift_name("Kodein_diScopeCloseable")))
@protocol ClientPluginKodein_diScopeCloseable
@required
- (void)close __attribute__((swift_name("close()")));
@end

__attribute__((swift_name("Kodein_diScopeRegistry")))
@interface ClientPluginKodein_diScopeRegistry : ClientPluginBase <ClientPluginKodein_diScopeCloseable>
- (void)clear __attribute__((swift_name("clear()")));
- (void)close __attribute__((swift_name("close()")));
- (id)getOrCreateKey:(id)key sync:(BOOL)sync creator:(ClientPluginKodein_diReference<id> *(^)(void))creator __attribute__((swift_name("getOrCreate(key:sync:creator:)")));
- (id _Nullable (^ _Nullable)(void))getOrNullKey:(id)key __attribute__((swift_name("getOrNull(key:)")));
- (void)removeKey:(id)key __attribute__((swift_name("remove(key:)")));
- (id)values __attribute__((swift_name("values()")));
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKTypeProjection")))
@interface ClientPluginKotlinKTypeProjection : ClientPluginBase
- (instancetype)initWithVariance:(ClientPluginKotlinKVariance * _Nullable)variance type:(id<ClientPluginKotlinKType> _Nullable)type __attribute__((swift_name("init(variance:type:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginKotlinKTypeProjectionCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginKotlinKTypeProjection *)doCopyVariance:(ClientPluginKotlinKVariance * _Nullable)variance type:(id<ClientPluginKotlinKType> _Nullable)type __attribute__((swift_name("doCopy(variance:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<ClientPluginKotlinKType> _Nullable type __attribute__((swift_name("type")));
@property (readonly) ClientPluginKotlinKVariance * _Nullable variance __attribute__((swift_name("variance")));
@end

__attribute__((swift_name("Kodein_diDIDefining")))
@interface ClientPluginKodein_diDIDefining<C, A, T> : ClientPluginBase
- (instancetype)initWithBinding:(id<ClientPluginKodein_diDIBinding>)binding fromModule:(NSString * _Nullable)fromModule __attribute__((swift_name("init(binding:fromModule:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<ClientPluginKodein_diDIBinding> binding __attribute__((swift_name("binding")));
@property (readonly) NSString * _Nullable fromModule __attribute__((swift_name("fromModule")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kodein_diDIDefinition")))
@interface ClientPluginKodein_diDIDefinition<C, A, T> : ClientPluginKodein_diDIDefining<C, A, T>
- (instancetype)initWithBinding:(id<ClientPluginKodein_diDIBinding>)binding fromModule:(NSString * _Nullable)fromModule tree:(id<ClientPluginKodein_diDITree>)tree __attribute__((swift_name("init(binding:fromModule:tree:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithBinding:(id<ClientPluginKodein_diDIBinding>)binding fromModule:(NSString * _Nullable)fromModule __attribute__((swift_name("init(binding:fromModule:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) id<ClientPluginKodein_diDITree> tree __attribute__((swift_name("tree")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinTriple")))
@interface ClientPluginKotlinTriple<__covariant A, __covariant B, __covariant C> : ClientPluginBase
- (instancetype)initWithFirst:(A _Nullable)first second:(B _Nullable)second third:(C _Nullable)third __attribute__((swift_name("init(first:second:third:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginKotlinTriple<A, B, C> *)doCopyFirst:(A _Nullable)first second:(B _Nullable)second third:(C _Nullable)third __attribute__((swift_name("doCopy(first:second:third:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) A _Nullable first __attribute__((swift_name("first")));
@property (readonly) B _Nullable second __attribute__((swift_name("second")));
@property (readonly) C _Nullable third __attribute__((swift_name("third")));
@end

__attribute__((swift_name("Kodein_diSearchSpecs")))
@interface ClientPluginKodein_diSearchSpecs : ClientPluginBase
- (instancetype)initWithContextType:(id<ClientPluginKaveritTypeToken> _Nullable)contextType argType:(id<ClientPluginKaveritTypeToken> _Nullable)argType type:(id<ClientPluginKaveritTypeToken> _Nullable)type tag:(id _Nullable)tag __attribute__((swift_name("init(contextType:argType:type:tag:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property id<ClientPluginKaveritTypeToken> _Nullable argType __attribute__((swift_name("argType")));
@property id<ClientPluginKaveritTypeToken> _Nullable contextType __attribute__((swift_name("contextType")));
@property id _Nullable tag __attribute__((swift_name("tag")));
@property id<ClientPluginKaveritTypeToken> _Nullable type __attribute__((swift_name("type")));
@end

__attribute__((swift_name("Kodein_diExternalSource")))
@protocol ClientPluginKodein_diExternalSource
@required
- (id (^ _Nullable)(id _Nullable))getFactoryDi:(id<ClientPluginKodein_diBindingDI>)di key:(ClientPluginKodein_diDIKey<id, id, id> *)key __attribute__((swift_name("getFactory(di:key:)")));
@end

__attribute__((swift_name("Kotlinx_datetimeDateTimeFormat")))
@protocol ClientPluginKotlinx_datetimeDateTimeFormat
@required
- (NSString *)formatValue:(id _Nullable)value __attribute__((swift_name("format(value:)")));
- (id<ClientPluginKotlinAppendable>)formatToAppendable:(id<ClientPluginKotlinAppendable>)appendable value:(id _Nullable)value __attribute__((swift_name("formatTo(appendable:value:)")));
- (id _Nullable)parseInput:(id)input __attribute__((swift_name("parse(input:)")));
- (id _Nullable)parseOrNullInput:(id)input __attribute__((swift_name("parseOrNull(input:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRoundContractState.Companion")))
@interface ClientPluginEngineRoundContractStateCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineRoundContractStateCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCombinationAnalytics")))
@interface ClientPluginEngineCombinationAnalytics : ClientPluginBase
- (instancetype)initWithType:(ClientPluginEngineCombinationType *)type count:(int32_t)count acceptedCounts:(int32_t)acceptedCounts __attribute__((swift_name("init(type:count:acceptedCounts:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineCombinationAnalytics *)doCopyType:(ClientPluginEngineCombinationType *)type count:(int32_t)count acceptedCounts:(int32_t)acceptedCounts __attribute__((swift_name("doCopy(type:count:acceptedCounts:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t acceptedCounts __attribute__((swift_name("acceptedCounts")));
@property (readonly) int32_t count __attribute__((swift_name("count")));
@property (readonly) ClientPluginEngineCombinationType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineAchievements")))
@interface ClientPluginEngineAchievements : ClientPluginBase
- (instancetype)initWithOldRating:(double)oldRating newRating:(double)newRating __attribute__((swift_name("init(oldRating:newRating:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineAchievements *)doCopyOldRating:(double)oldRating newRating:(double)newRating __attribute__((swift_name("doCopy(oldRating:newRating:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isRatingUp __attribute__((swift_name("isRatingUp")));
@property (readonly, getter=doNewRating) double newRating __attribute__((swift_name("newRating")));
@property (readonly) double oldRating __attribute__((swift_name("oldRating")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CorePlayerState.PlayerWaitingState")))
@interface ClientPluginCorePlayerStatePlayerWaitingState : ClientPluginKotlinEnum<ClientPluginCorePlayerStatePlayerWaitingState *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginCorePlayerStatePlayerWaitingStateCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginCorePlayerStatePlayerWaitingState *idle __attribute__((swift_name("idle")));
@property (class, readonly) ClientPluginCorePlayerStatePlayerWaitingState *inProgress __attribute__((swift_name("inProgress")));
@property (class, readonly) ClientPluginCorePlayerStatePlayerWaitingState *inProgressAttentionMode __attribute__((swift_name("inProgressAttentionMode")));
+ (ClientPluginKotlinArray<ClientPluginCorePlayerStatePlayerWaitingState *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginCorePlayerStatePlayerWaitingState *> *entries __attribute__((swift_name("entries")));
@property (readonly) BOOL isIdle __attribute__((swift_name("isIdle")));
@property (readonly) BOOL isPlayerTurn __attribute__((swift_name("isPlayerTurn")));
@property (readonly) BOOL isProgress __attribute__((swift_name("isProgress")));
@property (readonly) BOOL isProgressAttentionMode __attribute__((swift_name("isProgressAttentionMode")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CorePlayerConnection")))
@interface ClientPluginCorePlayerConnection : ClientPluginBase
- (instancetype)initWithState:(ClientPluginCorePlayerConnectionState *)state connectionChangedTime:(ClientPluginKotlinx_datetimeInstant *)connectionChangedTime notLiveDurationRecord:(int64_t)notLiveDurationRecord __attribute__((swift_name("init(state:connectionChangedTime:notLiveDurationRecord:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginCorePlayerConnectionCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)connectionChangedMoreThenDuration:(int64_t)duration __attribute__((swift_name("connectionChangedMoreThen(duration:)")));
- (ClientPluginCorePlayerConnection *)doCopyState:(ClientPluginCorePlayerConnectionState *)state connectionChangedTime:(ClientPluginKotlinx_datetimeInstant *)connectionChangedTime notLiveDurationRecord:(int64_t)notLiveDurationRecord __attribute__((swift_name("doCopy(state:connectionChangedTime:notLiveDurationRecord:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (int32_t)getLiveDurationInPercentageGameDuration:(int64_t)gameDuration __attribute__((swift_name("getLiveDurationInPercentage(gameDuration:)")));
- (int32_t)getNotLiveDurationInPercentageGameDuration:(int64_t)gameDuration __attribute__((swift_name("getNotLiveDurationInPercentage(gameDuration:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
- (BOOL)wasLiveDuringGameGameDuration:(int64_t)gameDuration __attribute__((swift_name("wasLiveDuringGame(gameDuration:)")));
@property (readonly) ClientPluginKotlinx_datetimeInstant *connectionChangedTime __attribute__((swift_name("connectionChangedTime")));
@property (readonly) int64_t lastConnectionStateDuration __attribute__((swift_name("lastConnectionStateDuration")));
@property (readonly) int64_t notLiveDuration __attribute__((swift_name("notLiveDuration")));
@property (readonly) int64_t notLiveDurationRecord __attribute__((swift_name("notLiveDurationRecord")));
@property (readonly) ClientPluginCorePlayerConnectionState *state __attribute__((swift_name("state")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CorePlayerState.Companion")))
@interface ClientPluginCorePlayerStateCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginCorePlayerStateCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginCorePlayerState *)askQuestion:(ClientPluginCorePlayerState *)receiver playerTurnTimeout:(ClientPluginKotlinx_datetimeInstant *)playerTurnTimeout tag:(NSString *)tag __attribute__((swift_name("askQuestion(_:playerTurnTimeout:tag:)")));
- (ClientPluginCorePlayerState *)idleState:(ClientPluginCorePlayerState *)receiver __attribute__((swift_name("idleState(_:)")));
- (ClientPluginCorePlayerState *)initialConnection:(ClientPluginCorePlayerConnection * _Nullable)connection __attribute__((swift_name("initial(connection:)")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayerOptions.RoundNumber")))
@interface ClientPluginEnginePlayerOptionsRoundNumber : ClientPluginKotlinEnum<ClientPluginEnginePlayerOptionsRoundNumber *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginEnginePlayerOptionsRoundNumberCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginEnginePlayerOptionsRoundNumber *circleNotStarted __attribute__((swift_name("circleNotStarted")));
@property (class, readonly) ClientPluginEnginePlayerOptionsRoundNumber *firstCirclePassed __attribute__((swift_name("firstCirclePassed")));
@property (class, readonly) ClientPluginEnginePlayerOptionsRoundNumber *secondCirclePassed __attribute__((swift_name("secondCirclePassed")));
+ (ClientPluginKotlinArray<ClientPluginEnginePlayerOptionsRoundNumber *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEnginePlayerOptionsRoundNumber *> *entries __attribute__((swift_name("entries")));
@property (readonly) int32_t number __attribute__((swift_name("number")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayerOptions.Companion")))
@interface ClientPluginEnginePlayerOptionsCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEnginePlayerOptionsCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEnginePlayerOptions *)fromHistoryIndex:(int32_t)index playerId:(NSString *)playerId gameCreatorId:(NSString *)gameCreatorId lastRound:(ClientPluginEngineRoundHistory * _Nullable)lastRound allRounds:(NSArray<ClientPluginEngineRoundHistory *> *)allRounds __attribute__((swift_name("fromHistory(index:playerId:gameCreatorId:lastRound:allRounds:)")));
- (ClientPluginEnginePlayerOptions *)initialIsGameCreator:(BOOL)isGameCreator playerNumber:(int32_t)playerNumber isShuffleCards:(BOOL)isShuffleCards isRoundWinner:(BOOL)isRoundWinner __attribute__((swift_name("initial(isGameCreator:playerNumber:isShuffleCards:isRoundWinner:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineLuckyFactorRecord")))
@interface ClientPluginEngineLuckyFactorRecord : ClientPluginBase
- (instancetype)initWithFactor:(float)factor value:(float)value __attribute__((swift_name("init(factor:value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineLuckyFactorRecordCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineLuckyFactorRecord *)doCopyFactor:(float)factor value:(float)value __attribute__((swift_name("doCopy(factor:value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) float factor __attribute__((swift_name("factor")));
@property (readonly) float value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCombinationRecord")))
@interface ClientPluginEngineCombinationRecord : ClientPluginBase
- (instancetype)initWithId:(NSString *)id type:(ClientPluginEngineCombinationType *)type state:(ClientPluginEngineCombinationState *)state value:(float)value __attribute__((swift_name("init(id:type:state:value:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineCombinationRecord *)doCopyId:(NSString *)id type:(ClientPluginEngineCombinationType *)type state:(ClientPluginEngineCombinationState *)state value:(float)value __attribute__((swift_name("doCopy(id:type:state:value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) BOOL isAccepted __attribute__((swift_name("isAccepted")));
@property (readonly) ClientPluginEngineCombinationState *state __attribute__((swift_name("state")));
@property (readonly) ClientPluginEngineCombinationType *type __attribute__((swift_name("type")));
@property (readonly) float value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayerHand.Companion")))
@interface ClientPluginEnginePlayerHandCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEnginePlayerHandCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEnginePlayerHand *)resetGame:(ClientPluginEnginePlayerHand *)receiver __attribute__((swift_name("resetGame(_:)")));
- (ClientPluginEnginePlayerHand *)resetRound:(ClientPluginEnginePlayerHand *)receiver __attribute__((swift_name("resetRound(_:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCombinationDetails")))
@interface ClientPluginEngineCombinationDetails : ClientPluginBase <ClientPluginEngineCombinationContract>
- (instancetype)initWithId:(NSString *)id type:(ClientPluginEngineCombinationType *)type state:(ClientPluginEngineCombinationState *)state reasons:(NSSet<ClientPluginEngineCombinationReason *> *)reasons cards:(NSArray<ClientPluginCoreGameCard *> * _Nullable)cards points:(ClientPluginInt * _Nullable)points numberOfPlayer:(ClientPluginInt * _Nullable)numberOfPlayer reason:(NSString * _Nullable)reason __attribute__((swift_name("init(id:type:state:reasons:cards:points:numberOfPlayer:reason:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) ClientPluginEngineCombinationDetailsCompanion *companion __attribute__((swift_name("companion")));
- (ClientPluginEngineCombinationDetails *)doCopyId:(NSString *)id type:(ClientPluginEngineCombinationType *)type state:(ClientPluginEngineCombinationState *)state reasons:(NSSet<ClientPluginEngineCombinationReason *> *)reasons cards:(NSArray<ClientPluginCoreGameCard *> * _Nullable)cards points:(ClientPluginInt * _Nullable)points numberOfPlayer:(ClientPluginInt * _Nullable)numberOfPlayer reason:(NSString * _Nullable)reason __attribute__((swift_name("doCopy(id:type:state:reasons:cards:points:numberOfPlayer:reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (int32_t)requireNumberOfPlayerLogger:(id<ClientPluginLoggerRaspberryLogger>)logger __attribute__((swift_name("requireNumberOfPlayer(logger:)")));
- (int32_t)requirePoints __attribute__((swift_name("requirePoints()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<ClientPluginCoreGameCard *> * _Nullable cards __attribute__((swift_name("cards")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) ClientPluginInt * _Nullable numberOfPlayer __attribute__((swift_name("numberOfPlayer")));
@property (readonly) ClientPluginInt * _Nullable points __attribute__((swift_name("points")));
@property (readonly) NSString * _Nullable reason __attribute__((swift_name("reason"))) __attribute__((deprecated("use CombinationReason instead")));
@property (readonly) NSSet<ClientPluginEngineCombinationReason *> *reasons __attribute__((swift_name("reasons")));
@property (readonly) ClientPluginEngineCombinationState *state __attribute__((swift_name("state")));
@property (readonly) ClientPluginEngineCombinationType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineRound.Companion")))
@interface ClientPluginEngineRoundCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineRoundCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineRound *)initial __attribute__((swift_name("initial()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePointItem")))
@interface ClientPluginEnginePointItem : ClientPluginBase
- (instancetype)initWithPlayers:(NSArray<NSString *> *)players points:(int32_t)points __attribute__((swift_name("init(players:points:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEnginePointItem *)doCopyPlayers:(NSArray<NSString *> *)players points:(int32_t)points __attribute__((swift_name("doCopy(players:points:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> *players __attribute__((swift_name("players")));
@property (readonly) int32_t points __attribute__((swift_name("points")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineConfigLite")))
@interface ClientPluginEngineConfigLite : ClientPluginBase
- (instancetype)initWithMode:(ClientPluginEngineRoomMode *)mode players:(ClientPluginEnginePlayersMode *)players points:(ClientPluginEnginePointsMode *)points timeoutTimeMillis:(int32_t)timeoutTimeMillis rulesPreset:(ClientPluginEngineRulesSetType *)rulesPreset isPrivate:(BOOL)isPrivate isChatEnabled:(BOOL)isChatEnabled isBotsEnabled:(BOOL)isBotsEnabled rules:(ClientPluginEngineRules * _Nullable)rules __attribute__((swift_name("init(mode:players:points:timeoutTimeMillis:rulesPreset:isPrivate:isChatEnabled:isBotsEnabled:rules:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineConfigLite *)doCopyMode:(ClientPluginEngineRoomMode *)mode players:(ClientPluginEnginePlayersMode *)players points:(ClientPluginEnginePointsMode *)points timeoutTimeMillis:(int32_t)timeoutTimeMillis rulesPreset:(ClientPluginEngineRulesSetType *)rulesPreset isPrivate:(BOOL)isPrivate isChatEnabled:(BOOL)isChatEnabled isBotsEnabled:(BOOL)isBotsEnabled rules:(ClientPluginEngineRules * _Nullable)rules __attribute__((swift_name("doCopy(mode:players:points:timeoutTimeMillis:rulesPreset:isPrivate:isChatEnabled:isBotsEnabled:rules:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isBotsEnabled __attribute__((swift_name("isBotsEnabled")));
@property (readonly) BOOL isChatEnabled __attribute__((swift_name("isChatEnabled")));
@property (readonly) BOOL isPrivate __attribute__((swift_name("isPrivate")));
@property (readonly) ClientPluginEngineRoomMode *mode __attribute__((swift_name("mode")));
@property (readonly) ClientPluginEnginePlayersMode *players __attribute__((swift_name("players")));
@property (readonly) ClientPluginEnginePointsMode *points __attribute__((swift_name("points")));
@property (readonly) ClientPluginEngineRules * _Nullable rules __attribute__((swift_name("rules")));
@property (readonly) ClientPluginEngineRulesSetType *rulesPreset __attribute__((swift_name("rulesPreset")));
@property (readonly) int32_t timeoutTimeMillis __attribute__((swift_name("timeoutTimeMillis")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineJassPlayerLite")))
@interface ClientPluginEngineJassPlayerLite : ClientPluginBase
- (instancetype)initWithUserInfo:(ClientPluginCoreGameUserInfo *)userInfo options:(ClientPluginEnginePlayerOptions *)options points:(ClientPluginEnginePlayerPoints *)points __attribute__((swift_name("init(userInfo:options:points:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginEngineJassPlayerLite *)doCopyUserInfo:(ClientPluginCoreGameUserInfo *)userInfo options:(ClientPluginEnginePlayerOptions *)options points:(ClientPluginEnginePlayerPoints *)points __attribute__((swift_name("doCopy(userInfo:options:points:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginEnginePlayerOptions *options __attribute__((swift_name("options")));
@property (readonly) ClientPluginEnginePlayerPoints *points __attribute__((swift_name("points")));
@property (readonly) ClientPluginCoreGameUserInfo *userInfo __attribute__((swift_name("userInfo")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCombinationReason")))
@interface ClientPluginEngineCombinationReason : ClientPluginKotlinEnum<ClientPluginEngineCombinationReason *> <ClientPluginKotlinComparable>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginEngineCombinationReason *winSingleCombination __attribute__((swift_name("winSingleCombination")));
@property (class, readonly) ClientPluginEngineCombinationReason *winHigherCombination __attribute__((swift_name("winHigherCombination")));
@property (class, readonly) ClientPluginEngineCombinationReason *winHigherCardCombination __attribute__((swift_name("winHigherCardCombination")));
@property (class, readonly) ClientPluginEngineCombinationReason *winTrumpCombination __attribute__((swift_name("winTrumpCombination")));
@property (class, readonly) ClientPluginEngineCombinationReason *winHigherPlayerNumberCombination __attribute__((swift_name("winHigherPlayerNumberCombination")));
@property (class, readonly) ClientPluginEngineCombinationReason *winPartnerCombination __attribute__((swift_name("winPartnerCombination")));
@property (class, readonly) ClientPluginEngineCombinationReason *rejectedNotTrumpSevenInRowCombination __attribute__((swift_name("rejectedNotTrumpSevenInRowCombination")));
+ (ClientPluginKotlinArray<ClientPluginEngineCombinationReason *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineCombinationReason *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientPlayerPointsViewModel")))
@interface ClientPluginGame_clientPlayerPointsViewModel : ClientPluginBase
- (instancetype)initWithUserInfo:(ClientPluginCoreGameUserInfo *)userInfo roundEarnedPoints:(id<ClientPluginGame_clientPointClause>)roundEarnedPoints roundBonusesOrFinesPoints:(id<ClientPluginGame_clientPointClause> _Nullable)roundBonusesOrFinesPoints rejectedPoints:(id<ClientPluginGame_clientPointClause> _Nullable)rejectedPoints __attribute__((swift_name("init(userInfo:roundEarnedPoints:roundBonusesOrFinesPoints:rejectedPoints:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginGame_clientPlayerPointsViewModel *)doCopyUserInfo:(ClientPluginCoreGameUserInfo *)userInfo roundEarnedPoints:(id<ClientPluginGame_clientPointClause>)roundEarnedPoints roundBonusesOrFinesPoints:(id<ClientPluginGame_clientPointClause> _Nullable)roundBonusesOrFinesPoints rejectedPoints:(id<ClientPluginGame_clientPointClause> _Nullable)rejectedPoints __attribute__((swift_name("doCopy(userInfo:roundEarnedPoints:roundBonusesOrFinesPoints:rejectedPoints:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<ClientPluginGame_clientPointClause> _Nullable rejectedPoints __attribute__((swift_name("rejectedPoints")));
@property (readonly) id<ClientPluginGame_clientPointClause> _Nullable roundBonusesOrFinesPoints __attribute__((swift_name("roundBonusesOrFinesPoints")));
@property (readonly) id<ClientPluginGame_clientPointClause> roundEarnedPoints __attribute__((swift_name("roundEarnedPoints")));
@property (readonly) ClientPluginCoreGameUserInfo *userInfo __attribute__((swift_name("userInfo")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientBadge")))
@interface ClientPluginGame_clientBadge : ClientPluginBase <ClientPluginKotlinComparable>
- (instancetype)initWithType:(ClientPluginGame_clientBadgeType *)type countOfBytes:(ClientPluginInt * _Nullable)countOfBytes __attribute__((swift_name("init(type:countOfBytes:)"))) __attribute__((objc_designated_initializer));
- (int32_t)compareToOther:(ClientPluginGame_clientBadge *)other __attribute__((swift_name("compareTo(other:)")));
- (ClientPluginGame_clientBadge *)doCopyType:(ClientPluginGame_clientBadgeType *)type countOfBytes:(ClientPluginInt * _Nullable)countOfBytes __attribute__((swift_name("doCopy(type:countOfBytes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) ClientPluginInt * _Nullable countOfBytes __attribute__((swift_name("countOfBytes")));
@property (readonly) ClientPluginGame_clientBadgeType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kodein_diReference")))
@interface ClientPluginKodein_diReference<__covariant T> : ClientPluginBase
- (instancetype)initWithCurrent:(T)current next:(T _Nullable (^)(void))next __attribute__((swift_name("init(current:next:)"))) __attribute__((objc_designated_initializer));
- (ClientPluginKodein_diReference<T> *)doCopyCurrent:(T)current next:(T _Nullable (^)(void))next __attribute__((swift_name("doCopy(current:next:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) T current __attribute__((swift_name("current")));
@property (readonly) T _Nullable (^next)(void) __attribute__((swift_name("next")));
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKVariance")))
@interface ClientPluginKotlinKVariance : ClientPluginKotlinEnum<ClientPluginKotlinKVariance *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) ClientPluginKotlinKVariance *invariant __attribute__((swift_name("invariant")));
@property (class, readonly) ClientPluginKotlinKVariance *in __attribute__((swift_name("in")));
@property (class, readonly) ClientPluginKotlinKVariance *out __attribute__((swift_name("out")));
+ (ClientPluginKotlinArray<ClientPluginKotlinKVariance *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginKotlinKVariance *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKTypeProjection.Companion")))
@interface ClientPluginKotlinKTypeProjectionCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginKotlinKTypeProjectionCompanion *shared __attribute__((swift_name("shared")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (ClientPluginKotlinKTypeProjection *)contravariantType:(id<ClientPluginKotlinKType>)type __attribute__((swift_name("contravariant(type:)")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (ClientPluginKotlinKTypeProjection *)covariantType:(id<ClientPluginKotlinKType>)type __attribute__((swift_name("covariant(type:)")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (ClientPluginKotlinKTypeProjection *)invariantType:(id<ClientPluginKotlinKType>)type __attribute__((swift_name("invariant(type:)")));
@property (readonly) ClientPluginKotlinKTypeProjection *STAR __attribute__((swift_name("STAR")));
@end

__attribute__((swift_name("KotlinAppendable")))
@protocol ClientPluginKotlinAppendable
@required
- (id<ClientPluginKotlinAppendable>)appendValue:(unichar)value __attribute__((swift_name("append(value:)")));
- (id<ClientPluginKotlinAppendable>)appendValue_:(id _Nullable)value __attribute__((swift_name("append(value_:)")));
- (id<ClientPluginKotlinAppendable>)appendValue:(id _Nullable)value startIndex:(int32_t)startIndex endIndex:(int32_t)endIndex __attribute__((swift_name("append(value:startIndex:endIndex:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CorePlayerState.PlayerWaitingStateCompanion")))
@interface ClientPluginCorePlayerStatePlayerWaitingStateCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginCorePlayerStatePlayerWaitingStateCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CorePlayerConnection.Companion")))
@interface ClientPluginCorePlayerConnectionCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginCorePlayerConnectionCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginCorePlayerConnection *)initialState:(ClientPluginCorePlayerConnectionState *)state timeout:(int64_t)timeout __attribute__((swift_name("initial(state:timeout:)")));
- (ClientPluginCorePlayerConnection *)onLive:(ClientPluginCorePlayerConnection * _Nullable)receiver __attribute__((swift_name("onLive(_:)")));
- (ClientPluginCorePlayerConnection *)onStateChanged:(ClientPluginCorePlayerConnection * _Nullable)receiver state:(ClientPluginCorePlayerConnectionState *)state __attribute__((swift_name("onStateChanged(_:state:)")));
@property (readonly) int32_t WAS_LIVE_MIN_PERCENTAGE __attribute__((swift_name("WAS_LIVE_MIN_PERCENTAGE")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnginePlayerOptions.RoundNumberCompanion")))
@interface ClientPluginEnginePlayerOptionsRoundNumberCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEnginePlayerOptionsRoundNumberCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEnginePlayerOptionsRoundNumber *)fromNumberNumber:(int32_t)number __attribute__((swift_name("fromNumber(number:)")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineLuckyFactorRecord.Companion")))
@interface ClientPluginEngineLuckyFactorRecordCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineLuckyFactorRecordCompanion *shared __attribute__((swift_name("shared")));
- (ClientPluginEngineLuckyFactorRecord *)average __attribute__((swift_name("average()")));
- (ClientPluginEngineLuckyFactorRecord *)initialAverageLuckyFactor:(float)averageLuckyFactor __attribute__((swift_name("initial(averageLuckyFactor:)")));
- (ClientPluginEngineLuckyFactorRecord *)roundIfMaxLimitFactor:(float)factor value:(float)value __attribute__((swift_name("roundIfMaxLimit(factor:value:)")));
@property (readonly) float MAX_LUCKY_FACTOR __attribute__((swift_name("MAX_LUCKY_FACTOR")));
@property (readonly) float ZERO_VALUE __attribute__((swift_name("ZERO_VALUE")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCombinationState")))
@interface ClientPluginEngineCombinationState : ClientPluginKotlinEnum<ClientPluginEngineCombinationState *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginEngineCombinationStateCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginEngineCombinationState *notVerified __attribute__((swift_name("notVerified")));
@property (class, readonly) ClientPluginEngineCombinationState *verifying __attribute__((swift_name("verifying")));
@property (class, readonly) ClientPluginEngineCombinationState *accepted __attribute__((swift_name("accepted")));
@property (class, readonly) ClientPluginEngineCombinationState *notAccepted __attribute__((swift_name("notAccepted")));
@property (class, readonly) ClientPluginEngineCombinationState *rejected __attribute__((swift_name("rejected")));
@property (class, readonly) ClientPluginEngineCombinationState *canceled __attribute__((swift_name("canceled")));
+ (ClientPluginKotlinArray<ClientPluginEngineCombinationState *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginEngineCombinationState *> *entries __attribute__((swift_name("entries")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCombinationDetails.Companion")))
@interface ClientPluginEngineCombinationDetailsCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineCombinationDetailsCompanion *shared __attribute__((swift_name("shared")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (int32_t)getPlayerNumberPlayerId:(NSString *)playerId players:(NSArray<id<ClientPluginEngineShuffleCardsContract>> *)players __attribute__((swift_name("getPlayerNumber(playerId:players:)")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (int32_t)getPlayerNumberPlayerId:(NSString *)playerId firstPlayerId:(NSString *)firstPlayerId playerIds:(NSArray<NSString *> *)playerIds __attribute__((swift_name("getPlayerNumber(playerId:firstPlayerId:playerIds:)")));
@end


/**
 * @note annotations
 *   kotlinx.serialization.Serializable
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientBadgeType")))
@interface ClientPluginGame_clientBadgeType : ClientPluginKotlinEnum<ClientPluginGame_clientBadgeType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) ClientPluginGame_clientBadgeTypeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) ClientPluginGame_clientBadgeType *byte __attribute__((swift_name("byte")));
@property (class, readonly) ClientPluginGame_clientBadgeType *hangingByte __attribute__((swift_name("hangingByte")));
@property (class, readonly) ClientPluginGame_clientBadgeType *nByte __attribute__((swift_name("nByte")));
@property (class, readonly) ClientPluginGame_clientBadgeType *rejectedCombinations __attribute__((swift_name("rejectedCombinations")));
@property (class, readonly) ClientPluginGame_clientBadgeType *winner __attribute__((swift_name("winner")));
+ (ClientPluginKotlinArray<ClientPluginGame_clientBadgeType *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<ClientPluginGame_clientBadgeType *> *entries __attribute__((swift_name("entries")));
@property (readonly) int32_t index __attribute__((swift_name("index")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EngineCombinationState.Companion")))
@interface ClientPluginEngineCombinationStateCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginEngineCombinationStateCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Game_clientBadgeType.Companion")))
@interface ClientPluginGame_clientBadgeTypeCompanion : ClientPluginBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) ClientPluginGame_clientBadgeTypeCompanion *shared __attribute__((swift_name("shared")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<ClientPluginKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(ClientPluginKotlinArray<id<ClientPluginKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

#pragma pop_macro("_Nullable_result")
#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
