# game_client_plugin_podspec



## Getting started

## Useful articles:
- [Generate framework](https://kotlinlang.org/docs/multiplatform-build-native-binaries.html#customize-the-info-plist-file)
- [Cocoapod private](https://medium.com/@shahabejaz/create-and-distribute-private-libraries-with-cocoapods-5b6507b57a03)
- [Cocoapod public](https://sanzeevgautam.medium.com/how-to-create-publish-cocoapods-library-5bf305393c86)
- [Cocoapod swift](https://www.kodeco.com/5823-how-to-create-a-cocoapod-in-swift#toc-anchor-005)
- [Consuming KMM Modules in iOS](https://medium.com/wantedly-engineering/different-approaches-in-consuming-kmm-modules-in-ios-7957c722b114)

## How to upload:
- ./gradlew assembleClientPluginXCFramework # assembling IOS (19 min) (PREFERRED)
- copy logic_debertz/client_plugin/build/XCFrameworks/release/ClientPlugin.xcframework to this repo 
 OR
- ./gradlew assembleClientGameEnginePluginXCFramework # kotlinArtifacts
- copy logic_debertz/client_plugin/build/xcframework/release/ClientPlugin.xcframework to this repo
- update version [spec.version = VERSION]
# covered with ./scripts/pod_upload.sh $VERSION
- commit code
- add new tag [git tag "$VERSION"]
- check repo: [pod lib lint ClientPlugin.podspec]
- push tags with files [git push --tags]
- add repo to pod (Optional) [pod repo add game_client_plugin_podspec https://gitlab.com/raspberryapps/game_client_plugin_podspec.git]
- publish cocoapod private [pod repo push game_client_plugin_podspec ClientPlugin.podspec]
- publish cocoapod public [pod trunk push ClientPlugin.podspec]