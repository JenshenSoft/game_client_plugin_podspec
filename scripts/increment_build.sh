#!/bin/bash
# https://gist.github.com/gotev/b4413b64034fac553366ff41b74134c6

TEMPLATE_FILE_NAME="ClientPlugin.podtemplate"
OUT_FILE_NAME="ClientPlugin.podspec"

# Increment the build version and save it into build.properties
. build.properties

BUILD_VERSION=$((BUILD_VERSION+1))

echo "BASE_VERSION=${BASE_VERSION}" > build.properties
echo "BUILD_VERSION=${BUILD_VERSION}" >> build.properties

# Load the template podspec and replace version
TEMPLATE=$(cat "$TEMPLATE_FILE_NAME" | sed "s/\[\[VERSION\]\]/${BASE_VERSION}\.${BUILD_VERSION}/g")

echo "$TEMPLATE" > "$OUT_FILE_NAME"