#!/usr/bin/env bash

# increment version
chmod +x ./scripts/increment_build.sh
./scripts/increment_build.sh

. build.properties
VERSION=${BASE_VERSION}\.${BUILD_VERSION}
echo "VERSION: ${VERSION}"

# check the repo
pod lib lint ClientPlugin.podspec --allow-warnings

echo "Commit changes..."
git commit -a -m "New release $VERSION"
git tag $VERSION

echo "Push tags..."
git push --tags

# publish privately
echo "Publish privately..."
# if error: "Unable to find the `game_client_plugin_podspec` repo
# pod repo add game_client_plugin_podspec git@gitlab.com:raspberryapps/game_client_plugin_podspec.git
# can take a minutes...
pod repo push game_client_plugin_podspec ClientPlugin.podspec --allow-warnings

# publish publicly
# echo "Publish publicly..."
# pod trunk push ClientPlugin.podspec --allow-warnings

echo "Pull sources..."
git pull

echo "Push sources..."
git push